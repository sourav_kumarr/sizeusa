<?php
session_start();
$user_id = "";
if(isset($_SESSION['sua_user_id'])){
    $user_id = $_SESSION['sua_user_id'];
}

echo "<input type='hidden' value='".$user_id."' id='user_id' />";
?>
<html>
    <head>
        <title>Size USA</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="css/font-awesome.css" />
        <link rel="stylesheet" href="css/bootstrap.min.css" />
        <link rel="stylesheet" type="text/css" href="css/style.css" />
        <link rel="stylesheet" href="css/custom.css" />
        <link rel="stylesheet" href="css/bootstrap-datetimepicker.css" />
        <link rel="stylesheet" href="css/jquery.dataTables.min.css" />
        <link rel="stylesheet" href="css/styles1.css" />

        <style>
            .userprofileimg {
                height: 40px;
                width: 40px;
                border-radius: 50%;
            }
        </style>
    </head>
    <body>
        <div class="col-md-12" style="padding:15px">
            <div class="col-md-2">
                <img src="images/logo1.png" style="max-height:60px;margin-top:10px" />
            </div>
            <div class="col-md-9">
                <ul class="menu">
                    <a href="index.php"><li id="index" class="menuitems activemenuitem">Home</li></a>
                    <a href="about.php"><li id="about" class="menuitems">About</li></a>
                    <!--<a href="#"><li id="customize" class="menuitems">Customize</li></a>-->
                    <a href="heritage.php"><li id="heritage" class="menuitems">Heritage</li></a>
                    <?php
                    if($user_id != "") {
                        ?>
                        <a href="report_gene.php">
                            <li id="report" class="menuitems">Report Generator</li>
                        </a>
                        <?php
                    }else{
                        ?>
                        <a onclick="signIn()"><li id="report" class="menuitems">Report Generator</li></a>
                        <?php
                    }
                    ?>
                    <li id="sizematching" onclick="window.location='sizematch.php'" class="menuitems">Size Matching Report</li>
                    <li id="3davatar" onclick="window.location='#'" class="menuitems">3D Avatar Generator</li>
                    <a href="contact.php"><li id="contact" class="menuitems">Contact</li></a>
                    <?php
                    if($user_id != "") {
                        ?>
                        <li class="menuitems">
                            <img id='profileimg' src="images/tc2_logo.png" class="userprofileimg" />
                            Hi<span id="userName"></span>
                        </li>
                        <li class="menuitems" onclick="logout()">Logout</li>
                        <?php
                    }else{
                        ?>
                        <li class="menuitems" onclick="signIn()">Login/Register</li>
                        <?php
                    }
                    ?>
                </ul>
            </div>
            <div class="col-md-1">
                <img onclick="window.location='http://tc2.com'" src="images/tc2_logo.png" style="margin-top:25px;cursor:pointer;height:32px;" />
            </div>
        </div>
        <div class="clear"></div>
