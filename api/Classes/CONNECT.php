<?php
/**
 * Created by PhpStorm.
 * User: Sourav
 * Date: 06-Apr-17
 * Time: 12:56 PM
 */

namespace Modals;
class CONNECT
{
    public $link = null;
    public $link2 = null;
    public $response = array();
    public function Connect(){
        $this->link = mysqli_connect(host,dbUser,dbPass,dbName);
        return $this->link;
    }
    public function Connect2(){
        $this->link2 = mysqli_connect(host,dbUser2,dbPass2,dbName2);
        return $this->link2;
    }
    public function lastId(){
        return mysqli_insert_id($this->link);
    }
    public function sqlError(){
        return mysqli_error($this->link);
    }
    public function lastId2(){
        return mysqli_insert_id($this->link2);
    }
    public function sqlError2(){
        return mysqli_error($this->link2);
    }

    public function insertQuery($table_name,$data){
      $keys = array();
      $values = array();

        foreach ($data as $key => $value) {
           array_push($keys,$key);
           array_push($values,"'$value'");

        }
        $keys = implode(',',$keys);
        $values = implode(',',$values);
//         print_r($keys);
        $query = "insert into $table_name($keys)values($values)";
        return $query;
    }

    public function updateQuery($table_name,$data,$id) {

    }

    public function apiResponse($response){
        header("Content-Type:application/json");
        echo json_encode($response);
    }
}