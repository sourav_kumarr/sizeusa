<?php
include ('header.php');
//Set useful variables for paypal form
$paypal_url = 'https://www.paypal.com/cgi-bin/webscr'; //Test PayPal API URL
$paypal_id = 'kapillikes@gmail.com'; //Business Email
?>

<form action="<?php echo $paypal_url; ?>" method="post">
    <!-- Identify your business so that you can collect the payments. -->
    <input type="hidden" name="business" value="<?php echo $paypal_id; ?>">
    <!-- Specify a Buy Now button. -->
    <input type="hidden" name="cmd" value="_xclick">

    <!-- Specify details about the item that buyers will purchase. -->
    <input type="hidden" name="item_name" value="<?php echo 'Basic Plan'; ?>">
    <input type="hidden" name="item_number" value="<?php echo '1'; ?>">
    <input type="hidden" name="amount" value="<?php echo '1000'; ?>">
    <input type="hidden" name="currency_code" value="USD">

    <!-- Specify URLs -->
    <input type='hidden' name='cancel_return' value='http://scan2fit.com/sizeusa/cancel.php'>
    <input type='hidden' name='return' value='http://scan2fit.com/sizeusa/success.php'>
    <input type='hidden' name='notify_url' value='http://scan2fit.com/sizeusa/payment.php'>


    <!-- Display the payment button. -->
    <input type="image" name="submit" border="0"
           src="https://www.paypalobjects.com/en_US/i/btn/btn_buynow_LG.gif" alt="PayPal - The safer, easier way to pay online" style="display: none" id="pay">
    <img alt="" border="0" width="1" height="1" src="https://www.paypalobjects.com/en_US/i/scr/pixel.gif" >

</form>

<?php
include ('footer.php');

?>
<script>
    $('#pay').click();
</script>
