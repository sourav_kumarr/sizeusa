<?php
include_once 'header.php';
?>
<hr>
<style>
    .heritage1 {
        height: 500px;
        margin: -22px 39px;
    }
    label {
        display: inherit;
    }
    .form-group {
        /*margin-bottom: 10px;*/
    }
    .child-lable {
        font-weight: 500;
        margin-bottom: 9px;
        margin-left: 17px;
        margin-top: 9px;
    }
    .form-group li {
        list-style: outside none none;
        display: flex;
    }
    #overlay{
        display: none;
    }
</style>
<!--<div class="col-md-12 heritageheader">
    <video autoplay loop style="width: 100%">
        <Source src="images/tailor.mp4" type="video/mp4" />
    </video>
    <div class="videotopdiv">
        <p class="videotopdivtext">Heritage is Diversity</p>
        <div class="line"></div>
    </div>
</div>-->
<div id="overlay">
    <div id="progstat">....Please Wait....<br>While generating report</div>
    <div id="progress"></div>
</div>
<div class="centercontent">
    <p class="heritage">Report Generation</p>
    <div class="line"></div>
    <div class="clear"></div>
    <div class="col-md-12">
        <label id="message" style="text-align: center"></label>
    </div>
    <div class="col-md-12" style="padding: 10px 142px;">
        <div class="col-md-4 zig">
            <div class="form-group">
                <ul>
                    <li ><input type="checkbox" onchange="onChangeSelectAll(1)" id="select_all_1" value="Gender" />&nbsp;&nbsp;Gender</li>
                    <li class="child-lable"><input class="checkbox_1" type="checkbox" name="gender" value="Male" onchange="onChangeSelectItem(1)">&nbsp;&nbsp;Male</li>
                    <li class="child-lable"><input class="checkbox_1" type="checkbox" name="gender" value="Female" onchange="onChangeSelectItem(1)">&nbsp;&nbsp;Female</li>
                </ul>
            </div>

            <div class="form-group">
                <ul>
                    <li><input type="checkbox" onchange="onChangeSelectAll(2)" id="select_all_2"/>&nbsp;&nbsp;Age</li>
                    <li class="child-lable"><input class="checkbox_2" type="checkbox" name="age" value="18 - 25" onchange="onChangeSelectItem(2)">&nbsp;&nbsp; 18 - 25</li>
                    <li class="child-lable"><input class="checkbox_2" type="checkbox" name="age" value="26 - 35" onchange="onChangeSelectItem(2)">&nbsp;&nbsp; 26 - 35</li>
                    <li class="child-lable"><input class="checkbox_2" type="checkbox" name="age" value="36 - 45" onchange="onChangeSelectItem(2)">&nbsp;&nbsp; 36 - 45</li>
                    <li class="child-lable"><input class="checkbox_2" type="checkbox" name="age" value="46 - 55" onchange="onChangeSelectItem(2)">&nbsp;&nbsp; 46 - 55</li>
                    <li class="child-lable"><input class="checkbox_2" type="checkbox" name="age" value="56 - 65" onchange="onChangeSelectItem(2)">&nbsp;&nbsp; 56 - 65</li>
                    <li class="child-lable"><input class="checkbox_2" type="checkbox" name="age" value="66 +" onchange="onChangeSelectItem(2)">&nbsp;&nbsp;  66+</li>
                </ul>
            </div>

            <div class="form-group">
                <ul>
                    <li><input type="checkbox" onchange="onChangeSelectAll(3)" id="select_all_3"/>&nbsp;&nbsp; Ethnicity</li>
                    <li class="child-lable"><input class="checkbox_3" type="checkbox" name="ethnicity" value="Non-Hispanic Black" onchange="onChangeSelectItem(3)">&nbsp;&nbsp; African-America</li>
                    <li class="child-lable"><input class="checkbox_3" type="checkbox" name="ethnicity" value="Non-Hispanic White" onchange="onChangeSelectItem(3)">&nbsp;&nbsp; Non-Hispanic White</li>
                    <li class="child-lable"><input class="checkbox_3" type="checkbox" name="ethnicity" value="Asian" onchange="onChangeSelectItem(3)">&nbsp;&nbsp; Asian</li>
                    <li class="child-lable"><input class="checkbox_3" type="checkbox" name="ethnicity" value="Mexican Hispanic" onchange="onChangeSelectItem(3)">&nbsp;&nbsp; Mexican Hispanic</li>
                    <li class="child-lable"><input class="checkbox_3" type="checkbox" name="ethnicity" value="Non-Mexican Hispanic" onchange="onChangeSelectItem(3)">&nbsp;&nbsp; Non-Mexican Hispanic</li>
                    <li class="child-lable"><input class="checkbox_3" type="checkbox" name="ethnicity" value="Other" onchange="onChangeSelectItem(3)">&nbsp;Other</li>

                </ul>
            </div>
        </div>

        <div class="col-md-4 zig">
            <div class="form-group">
                <ul>
                    <li><input type="checkbox" onchange="onChangeSelectAll(4)" id="select_all_4"/>&nbsp;&nbsp; Income </li>
                    <li class="child-lable"><input class="checkbox_4" type="checkbox" name="income" value="Under 25K" onchange="onChangeSelectItem(4)">&nbsp;&nbsp; Under 25K</li>
                    <li class="child-lable"><input class="checkbox_4" type="checkbox" name="income" value="25 - 50K" onchange="onChangeSelectItem(4)">&nbsp;&nbsp; 25-50k</li>
                    <li class="child-lable"><input class="checkbox_4" type="checkbox" name="income" value="50 - 75K" onchange="onChangeSelectItem(4)">&nbsp;&nbsp; 50-75k</li>
                    <li class="child-lable"><input class="checkbox_4" type="checkbox" name="income" value="75 - 100K" onchange="onChangeSelectItem(4)">&nbsp;&nbsp; 75-100k</li>
                    <li class="child-lable"><input class="checkbox_4" type="checkbox" name="income" value="100K+" onchange="onChangeSelectItem(4)">&nbsp;&nbsp; 100k+</li>
                </ul>
            </div>

            <div class="form-group">
                <ul>
                    <li><input type="checkbox" onchange="onChangeSelectAll(5)" id="select_all_5"/>&nbsp;&nbsp; Education</li>
                    <li class="child-lable"><input class="checkbox_5" type="checkbox" name="education" value="Less High School" onchange="onChangeSelectItem(5)"/>&nbsp;&nbsp; Less High School</li>
                    <li class="child-lable"><input class="checkbox_5" type="checkbox" name="education" value="High School" onchange="onChangeSelectItem(5)"/>&nbsp;&nbsp; High School</li>
                    <li class="child-lable"><input class="checkbox_5" type="checkbox" name="education" value="Some College" onchange="onChangeSelectItem(5)"/>&nbsp;&nbsp; Some College</li>
                    <li class="child-lable"><input class="checkbox_5" type="checkbox" name="education" value="College" onchange="onChangeSelectItem(5)"/>&nbsp;&nbsp; College</li>
                    <li class="child-lable"><input class="checkbox_5" type="checkbox" name="education" value="Post-Graduate" onchange="onChangeSelectItem(5)"/>&nbsp;&nbsp; Post-Graduate</li>
                </ul>
            </div>
        </div>

        <div class="col-md-4 zig">
            <div class="form-group">
                <ul>
                    <li><input type="checkbox" onchange="onChangeSelectAll(6)" id="select_all_6"/>&nbsp;&nbsp; Marital Status</li>
                    <li class="child-lable"><input class="checkbox_6" type="checkbox" name="MaritalStatus" value="Single" onchange="onChangeSelectItem(6)">&nbsp;&nbsp; Single</li>
                    <li class="child-lable"><input class="checkbox_6" type="checkbox" name="MaritalStatus" value="Married" onchange="onChangeSelectItem(6)">&nbsp;&nbsp; Married</li>
                    <li class="child-lable"><input class="checkbox_6" type="checkbox" name="MaritalStatus" value="Divorced/Separated" onchange="onChangeSelectItem(6)">&nbsp;&nbsp; Divorced/Separated</li>
                    <li class="child-lable"><input class="checkbox_6" type="checkbox" name="MaritalStatus" value="Widowed" onchange="onChangeSelectItem(6)">&nbsp;&nbsp; Widowed</li>
                </ul>
            </div>

            <div class="form-group">
                <ul>
                    <li><input type="checkbox" onchange="onChangeSelectAll(7)" id="select_all_7"/>&nbsp;&nbsp; Activity level</li>
                    <li class="child-lable"><input class="checkbox_7" type="checkbox" name="Activitylevel" value="Blank" onchange="onChangeSelectItem(7)">&nbsp;&nbsp;Blank</li>
                    <li class="child-lable"><input class="checkbox_7" type="checkbox" name="Activitylevel" value="Little Less" onchange="onChangeSelectItem(7)">&nbsp;&nbsp; Little Less</li>
                    <li class="child-lable"><input class="checkbox_7" type="checkbox" name="Activitylevel" value="Average" onchange="onChangeSelectItem(7)">&nbsp;&nbsp; Average</li>
                    <li class="child-lable"><input class="checkbox_7" type="checkbox" name="Activitylevel" value="Very" onchange="onChangeSelectItem(7)">&nbsp;&nbsp; Very</li>
                </ul>
            </div>
        </div>
        <input type="button" value="Create Report" style="float: right; padding: 5px 16px; background: #428bca none repeat scroll 0% 0%;
         border: 1px solid seagreen; color: white; font-weight: 700; margin-top: 71px;" id="creatReport" onclick="onReportSubmit()">
    </div>

</div>
<div class="clear"></div>
<hr>
<?php
include_once 'footer.php';
?>

<script>

    function selected(selectId) {
        var selectAll = document.getElementById('select_all_'+selectId);
        var selectItem = document.getElementsByClassName('checkbox_'+selectId);
//        alert(selectAll);
        selectAll.addEventListener('change',function () {
            console.log('onChange called---');
            $(".checkbox_" + selectId).prop('checked', $(this).prop("checked"));
        });
        for (var i = 0; i < selectItem.length; i++) {
            selectItem[i].addEventListener('change',function () {
                if (false == $(this).prop("checked")) {
                    $("#select_all_" + selectId).prop('checked', false);
                }
                if ($('.checkbox_' + selectId + ':checked').length == $('.checkbox_' + selectId).length) {
                    $("#select_all_" + selectId).prop('checked', true);
                }
            });
        }

        $('#message').html('');
      }

      function onChangeSelectAll(selectId) {
               var isChecked = $("#select_all_"+selectId).prop("checked");
//               console.log('isChecked -- '+isChecked);
              $(".checkbox_" + selectId).prop('checked', isChecked);
               var selectItem = document.getElementsByClassName('checkbox_'+selectId);
              for (var i = 0; i < selectItem.length; i++) {
                selectItem[i].addEventListener('change',function () {
                  if (false == $(this).prop("checked")) {
                      $("#select_all_" + selectId).prop('checked', false);
                  }
                  if ($('.checkbox_' + selectId + ':checked').length == $('.checkbox_' + selectId).length) {
                      $("#select_all_" + selectId).prop('checked', true);
                  }
              });
          }

      }
      function onChangeSelectItem(selectId) {
          var isChecked = $(".checkbox_"+selectId).prop("checked");
          var selectItem = document.getElementsByClassName('checkbox_'+selectId);
          for (var i = 0; i < selectItem.length; i++) {
              selectItem[i].addEventListener('change',function () {
                  if (false == isChecked) {
                      $("#select_all_" + selectId).prop('checked', false);
                  }
                  if ($('.checkbox_' + selectId + ':checked').length == $('.checkbox_' + selectId).length) {
                      $("#select_all_" + selectId).prop('checked', true);
                  }
              });
          }
      }

          function onReportSubmit() {
              var gender = [0];
              var age = [0];
              var ethnicity = [0];
              var income = [0];
              var education = [0];
              var MaritalStatus = [0];
              var Activitylevel = [0];


              var user_id = $("#user_id").val();
              if(user_id == '') {
                  signIn();
                  return false;
              }
              $.each($("input[name='gender']:checked"), function(){
                  gender.push($(this).val());

//                    alert(gender);
              });

              $.each($("input[name='age']:checked"), function(){
                  age.push($(this).val());
//                    alert(age);
              });

              $.each($("input[name='ethnicity']:checked"), function(){
                  ethnicity.push($(this).val());
//                    alert(ethnicity);
              });

              $.each($("input[name='income']:checked"), function(){
                  income.push($(this).val());
//                    alert(income);
              });

              $.each($("input[name='education']:checked"), function(){
                  education.push($(this).val());
//                    alert(education);
              });

              $.each($("input[name='MaritalStatus']:checked"), function(){
                  MaritalStatus.push($(this).val());
//                    alert(MaritalStatus);
              });

              $.each($("input[name='Activitylevel']:checked"), function(){
                  Activitylevel.push($(this).val());
//                    alert(Activitylevel);
              });
              gender = gender.join(',');
              age = age.join(',');
              ethnicity = ethnicity.join(',');
              income = income.join(',');
              education = education.join(',');
              MaritalStatus = MaritalStatus.join(',');
              Activitylevel = Activitylevel.join(',');

              var url = 'api/reportProcess.php';
              if(gender.length>1)
                  gender = gender.substr(2,gender.length);
              if(age.length>1)
                  age = age.substr(2,age.length);
              if(ethnicity.length>1)
                  ethnicity = ethnicity.substr(2,ethnicity.length);
              if(income.length>1)
                  income = income.substr(2,income.length);
              if(education.length>1)
                  education = education.substr(2,education.length);
              if(MaritalStatus.length>1)
                  MaritalStatus = MaritalStatus.substr(2,MaritalStatus.length);
              if(Activitylevel.length>1)
                  Activitylevel = Activitylevel.substr(2,Activitylevel.length);

              console.log('gender -- '+gender);
              console.log('age -- '+age);
              console.log('ethnicity -- '+ethnicity);
              console.log('education -- '+education);
              console.log('MaritalStatus -- '+MaritalStatus);
              console.log('Activitylevel -- '+Activitylevel);

              if(gender.length == 1 && age.length ==1 && ethnicity.length == 1 && income.length ==1 && education.length==1 &&
                  MaritalStatus.length == 1 && Activitylevel.length ==1) {
//                  alert('Nothing is selected');
                  $("#message").html('Nothing is selected');
                  $("#message").css('color','red');
                  return false;
              }
              $('#overlay').css('display','block');
              $.post(url,{'gender':gender,'age':age,'ethnicity':ethnicity,'income':income,
                  'education':education,'activity':Activitylevel,'martial':MaritalStatus,'type':'addReport','user_id':user_id},
                  function(data){
                  var status = data.Status;
                  var message = data.Message;
                  var code = data.Code;
                  if(status == 'Failure' && code == 501) {
                     $("#message").html(message);
                     $("#message").css('color','red');
                      $('#overlay').css('display','none');
                  }
                  else if(status == 'Failure' && code == 500) {
                      $('#overlay').css('display','none');
                      /*$(".modal-title").html("<label style='color: red'>Error !</label>");
                      $(".modal-body").html('<div class="col-md-12"><label>You have not subscribed for our plan to get subscription please purchase plan </label></div>'+
                       '<div class="col-md-12">Charges $1000 per year</div>');
                      $(".modal-body").css('height','100px');
                      $(".modal-footer").html('<button type="button" class="btn btn-primary" onclick="onBuyClicked()"><i class="fa fa-credit-card" aria-hidden="true"></i> Buy</button><button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-close"></i> Close</button>');
                      $("#myModal").modal("show");*/

                      var modal = document.getElementById('customModal');
                      var span = document.getElementById("close");

                      modal.style.display = "block";

                      span.onclick = function() {
                          modal.style.display = "none";
                      }

                      window.onclick = function(event) {
                          if (event.target == modal) {
                              modal.style.display = "none";
                          }
                      }

                  }
                  else if(status == 'Success' && code == 200) {
//                      $("#message").html(message);
//                      $("#message").css('color','green');

                      setTimeout(function () {
                          window.location = 'measurements.php';
                          $('#overlay').css('display','none');
                      },2000);

                  }
                  console.log('data --- '+JSON.stringify(data));
              }).fail(function () {
                  alert("unable to locate request....");
                  $('#overlay').css('display','none');

              });
          }

          function onBuyClicked() {
              var modal = document.getElementById('customModal');

              modal.style.display = "none";
            setTimeout(function () {
                window.location ='payment.php';
            },1000);

          }
          function onPopUpClosed() {
              var modal = document.getElementById('customModal');

              modal.style.display = "none";
          }

</script>