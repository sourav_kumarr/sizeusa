<?php
    include_once 'header.php';
?>
<div class="col-md-12 aboutheader"></div>
<div class="centercontent">
    <p class="about">About SizeUSA</p>
    <div class="line"></div>
    <div class="clear"></div>
    <div class="col-md-1"></div>
    <div class="col-md-10 zig">
        <div class="col-md-6 abouttext" >
            <!--<div class="col-md-12 aboutdivs">
                <div class="col-md-2" style="padding: 0">
                    <label class="numbering">01</label>
                </div>
                <div class="col-md-10" style="padding: 0">
                    <label class="headingafternumber">Praesent vestibulum molestie</label><br>
                    <p class="aboutcontent">
                        Fusce suscipit varius mi. Cum sociis nato que penatibus et magnis dis parturient montes,
                        nascetur ridiculus mus. Nulla dui. Fusce feugiat malesuada odio. Morbi nunc odio, gravida at,
                        cursus nec.
                    </p>
                </div>
            </div>
            <div class="col-md-12 aboutdivs">
                <div class="col-md-2" style="padding: 0">
                    <label class="numbering">02</label>
                </div>
                <div class="col-md-10" style="padding: 0">
                    <label class="headingafternumber">Praesent vestibulum molestie</label><br>
                    <p class="aboutcontent">
                        Fusce suscipit varius mi. Cum sociis nato que penatibus et magnis dis parturient montes,
                        nascetur ridiculus mus. Nulla dui. Fusce feugiat malesuada odio. Morbi nunc odio, gravida at,
                        cursus nec.
                    </p>
                </div>
            </div>
            <div class="col-md-12 aboutdivs">
                <div class="col-md-2" style="padding: 0">
                    <label class="numbering">03</label>
                </div>
                <div class="col-md-10" style="padding: 0">
                    <label class="headingafternumber">Praesent vestibulum molestie</label><br>
                    <p class="aboutcontent">
                        Fusce suscipit varius mi. Cum sociis nato que penatibus et magnis dis parturient montes,
                        nascetur ridiculus mus. Nulla dui. Fusce feugiat malesuada odio. Morbi nunc odio, gravida at,
                        cursus nec.
                    </p>
                </div>
            </div>
            <div class="col-md-12 aboutdivs">
                <div class="col-md-2" style="padding: 0">
                    <label class="numbering">04</label>
                </div>
                <div class="col-md-10" style="padding: 0">
                    <label class="headingafternumber">Praesent vestibulum molestie</label><br>
                    <p class="aboutcontent">
                        Fusce suscipit varius mi. Cum sociis nato que penatibus et magnis dis parturient montes,
                        nascetur ridiculus mus. Nulla dui. Fusce feugiat malesuada odio. Morbi nunc odio, gravida at,
                        cursus nec.
                    </p>
                </div>
            </div>-->
            <p>Leading apparel brands with an interest in establishing accurate measurements for the current U.S.
            population sponsored SizeUSA.</p>
            <p>In the first and only U.S. apparel focused survey done in the last 40 years, subjects were scanned
            in 12 cities across the nation resulting in a database of over 10,000 scanned subjects.
            This data can be purchased along with the measurement extraction software and includes the following
            components:</p>
            <li>Demographic information of each subject</li>
            <li>Over 300 standard measurements (thousands of measurements possible)</li>
            <li>Ability to extract custom measurements 3D scan file of each subject</li>
            <li>Ability to create 3D avatars</li><br>
            <p>We offer consulting services to optimize fit and better determine target customer size and shape. 
                Using the SizeUSA data and our proprietary methodology, we can assist your brand in maximizing
                your fit across size ranges and product categories.  This innovation brings better fitting garment,
                what improves the quality and  image of the brand. This will help you to grow sales, customer
                loyalty and improve profitability.  If you are interested in doing research of your own,
                producing made-to-measure garments, or another scanning solution, please visit our website
                <a href="http://www.tc2.com">www.tc2.com</a> for more details.</p>
            <p>Here you can see, using your current size table, what percentage of
                your target group fits it well and much more.  Better to know then to guess</p>
        </div>
        <div class="col-md-6">
            <img class="img-responsive img-thumbnail" src="images/shutterstock_534761053.jpg" />
        </div>
    </div>
</div>
<div class="clear"></div>
<hr>
<?php
    include_once 'footer.php';
?>
<script>
    $(".menuitems").removeClass("activemenuitem");
    $("#about").addClass("activemenuitem");
</script>