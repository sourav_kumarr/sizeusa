<?php

session_start();
include("Constants/configuration.php");
include("Constants/dbConfig.php");
include("Constants/functions.php");
require_once "Classes/USERS.php";
require_once "Classes/REPORT.php";

//print_r($_SESSION);
$connect = new \Modals\CONNECT();
$reportClass = new \Modals\REPORT();
$SQL = $_SESSION['sql'];

$header = '';
$result ='';
$link = $connect->Connect();
$exportData = mysqli_query ($link,$SQL ) or die ( "Sql error : " . $connect->sqlError() );
 
//$fields = $reportClass->getReportFields($_SESSION['tableName']);
 $fields = mysqli_num_fields($exportData);

for ( $i = 0; $i < $fields; $i++ )
{
    $header .=  mysqli_fetch_field_direct($exportData,$i)->name. "\t";
}
 
while( $row = mysqli_fetch_row( $exportData ) )
{
    $line = '';
    foreach( $row as $value )
    {                                            
        if ( ( !isset( $value ) ) || ( $value == "" ) )
        {
            $value = "\t";
        }
        else
        {
            $value = str_replace( '"' , '""' , $value );
            $value = '"' . $value . '"' . "\t";
        }
        $line .= $value;
    }
    $result .= trim( $line ) . "\n";
}
$result = str_replace( "\r" , "" , $result );
 
if ( $result == "" )
{
    $result = "\nNo Record(s) Found!\n";                        
}

//header('Content-Type: text/csv; charset=utf-8');
//header("Content-type: application/vnd.ms-excel");
//header("Content-type: application/x-msexcel");
//header("Content-Disposition: attachment; filename=export.xls");
//header("Pragma: public");
$filename ="excelreport.csv";

header("Pragma: public");
header("Expires: 0");
header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
header("Content-Type: application/force-download");
header("Content-Type: application/octet-stream");
header("Content-Type: application/download");
header("Content-type: application/ms-excel");
header("Content-Disposition: attachment; filename={$filename} ");
header("Content-Transfer-Encoding: binary ");

header("Pragma: no-cache");
header("Expires: 0");
print "$header\n$result";
 
?>