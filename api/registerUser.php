<?php
include("Constants/configuration.php");
include("Constants/dbConfig.php");
include("Constants/functions.php");
require_once "Classes/USERS.php";
$userClass = new Modals\USERS();
if(isset($_REQUEST["_"])){
    session_start();
    $_SESSION['capd'] = $_REQUEST['_'];
    header("Location:../index.php?_=cpd");
}
else {
    $requiredFeilds = array("type");
    $response = RequiredFields($_REQUEST, $requiredFeilds);
    if ($response[Status] != Success) {
        $userClass->apiResponse($response);
        return false;
    }
    $type = $_REQUEST['type'];
    if ($type == "Register") {
        $requiredFeilds = array('fname', 'lname', 'email', 'password','country','state','city','gender','dob',
        'stylist','height','weight','language');
        $response = RequiredFields($_REQUEST, $requiredFeilds);
        if ($response[Status] != Success) {
            $userClass->apiResponse($response);
            return false;
        }
        $fname = trim($_REQUEST['fname']);
        $lname = trim($_REQUEST['lname']);
        $email = trim($_REQUEST['email']);
        $password = trim($_REQUEST['password']);
        $country = trim($_REQUEST['country']);
        $state = trim($_REQUEST['state']);
        $city = trim($_REQUEST['city']);
        $gender = trim($_REQUEST['gender']);
        $dob = trim($_REQUEST['dob']);
        $stylist = trim($_REQUEST['stylist']);
        $height = trim($_REQUEST['height']);
        $weight = trim($_REQUEST['weight']);
        $language = trim($_REQUEST['language']);
        $emailResponse = $userClass->checkEmailExistance($email);
        if ($emailResponse[Status] == Error) {
            $userClass->apiResponse($emailResponse);
            return false;
        }
        ($response = $userClass->RegisterUser($fname,$lname,$email,$password,$country,$state,$city,$gender,$dob,$stylist,$height,
        $weight,$language));
        if ($response[Status] != Success) {
            $userClass->apiResponse($response);
            return false;
        }
        $userId = $response['lastId'];
        $temp = $userClass->getParticularUserData($userId);
        $userData = $temp['userData'];
        $response['userData'] = $userData;
        unset($response['lastId']);
        unset($response['file_name']);
        $userClass->apiResponse($response);
    }else if ($type == "updateProfile") {
        $requiredFeilds = array('user_id','fname', 'lname', 'contact', 'gender', 'dob', 'height', 'weight');
        $response = RequiredFields($_REQUEST, $requiredFeilds);
        if ($response[Status] != Success) {
            $userClass->apiResponse($response);
            return false;
        }
        $fname = trim($_REQUEST['fname']);
        $lname = trim($_REQUEST['lname']);
        $contact = trim($_REQUEST['contact']);
        $gender = trim($_REQUEST['gender']);
        $dob = trim($_REQUEST['dob']);
        $height = trim($_REQUEST['height']);
        $weight = trim($_REQUEST['weight']);
        $user_id = trim($_REQUEST['user_id']);
        $response = $userClass->updateProfile($user_id, $fname, $lname, $contact, $gender, $dob, $height, $weight);
        if ($response[Status] != Success) {
            $userClass->apiResponse($response);
            return false;
        }
        $temp = $userClass->getParticularUserData($user_id);
        ($userData = $temp['userData']);
        $response['userData'] = $userData;
        unset($response['file_name']);
        $userClass->apiResponse($response);
    } else if ($type == "login") {
        $requiredFeilds = array('username', 'password');
        $response = RequiredFields($_REQUEST, $requiredFeilds);
        if ($response[Status] != Success) {
            $userClass->apiResponse($response);
            return false;
        }
        $username = trim($_REQUEST['username']);
        $password = trim($_REQUEST['password']);
        $response = $userClass->userLogin($username, $password);
        if ($response[Status] != Success) {
            $userClass->apiResponse($response);
            return false;
        }
        session_start();
        $_SESSION['sua_user_id'] = $response['userData']['user_id'];
        $userClass->apiResponse($response);
    } else if ($type == "Logout") {
        session_start();
        unset($_SESSION['sua_user_id']);
        $response[Status] = Success;
        $response[Message] = "User Logged out Successfully";
        $userClass->apiResponse($response);
    } else if ($type == "uploadPicture") {
        $requiredFeilds = array('imagetype', 'imageParam', 'user_id');
        $response = RequiredFields($_REQUEST, $requiredFeilds);
        if ($response[Status] != Success) {
            $userClass->apiResponse($response);
            return false;
        }
        $imageParam = $_REQUEST['imageParam'];
        $imageType = $_REQUEST['imagetype'];
        $userId = $_REQUEST['user_id'];
        $response = $userClass->uploadPicture("img", $imageParam);
        if ($response[Status] == Error) {
            $userClass->apiResponse($response);
            return false;
        }
        $file_name = $response['file_name'];
        $response = $userClass->updateUserImage($imageType, $file_name, $userId);
        unset($response['file_name']);
        $userClass->apiResponse($response);
    } else if ($type == "forgotPassword") {
        $requiredFeilds = array('email');
        $response = RequiredFields($_REQUEST, $requiredFeilds);
        if ($response[Status] != Success) {
            $userClass->apiResponse($response);
            return false;
        }
        $email = $_REQUEST['email'];
        $response = $userClass->checkEmailExistance($email);
        if ($response[STATUS] == Success) {
            $response[Status] = Error;
            $response[Message] = "E-Mail is not Registered with us...";
            $userClass->apiResponse($response);
            return false;
        }
        $response = $userClass->sendMaill($email);
        $userClass->apiResponse($response);
    }else if ($type == "changeForgotPassword") {
        session_start();
        $requiredfields = array('new_password');
        $response = RequiredFields($_REQUEST, $requiredfields);
        if ($response['Status'] == 'Failure') {
            $userClass->apiResponse($response);
            return false;
        }
        $email = trim($_SESSION['capd']);
        $new_password = trim($_REQUEST['new_password']);
        $response = $userClass->changeForgotPassword($new_password, $email);
        if ($response[STATUS] == Error) {
            $userClass->apiResponse($response);
            return false;
        }
        $userClass->apiResponse($response);
    }else if ($type == "getSize") {
        $response = $userClass->getSize();
        if ($response[STATUS] == Error) {
            $userClass->apiResponse($response);
            return false;
        }
        $userClass->apiResponse($response);
    }else if($type == "getParticularUserData"){
        $requiredfields = array('user_id');
        $response = RequiredFields($_REQUEST, $requiredfields);
        if ($response['Status'] == 'Failure') {
            $userClass->apiResponse($response);
            return false;
        }
        $user_id = trim($_REQUEST['user_id']);
        $response = $userClass->getParticularUserData($user_id);
        if($response[Status] == Error){
            $userClass->apiResponse($response);
            return false;
        }
        $userClass->apiResponse($response);
    }else if($type == "resetValues"){
        session_start();
        $user_id = trim($_SESSION['sf_user_id']);
        $response = $userClass->resetData($user_id);
        if($response[Status] == Error){
            $userClass->apiResponse($response);
            return false;
        }
        $userClass->apiResponse($response);
    }
    else if($type == "updateDimensions"){
        $requiredfields = array('user_id','height','weight','age','gender','race','neck','brand','size','waist');
        $response = RequiredFields($_REQUEST, $requiredfields);
        if ($response['Status'] == 'Failure') {
            $userClass->apiResponse($response);
            return false;
        }
        $user_id = trim($_REQUEST['user_id']);
        $height = trim($_REQUEST['height']);
        $weight = trim($_REQUEST['weight']);
        $age = trim($_REQUEST['age']);
        $gender = trim($_REQUEST['gender']);
        $race = trim($_REQUEST['race']);
        $neck = trim($_REQUEST['neck']);
        $brand = trim($_REQUEST['brand']);
        $size = trim($_REQUEST['size']);
        $waist = trim($_REQUEST['waist']);
        $response = $userClass->updateDimensions($user_id,$height,$weight,$age,$gender,$race,$neck,$brand,$size,$waist);
        if($response[Status] == Error){
            $userClass->apiResponse($response);
            return false;
        }
        $userClass->apiResponse($response);
    } else {
        $response[Status] = Error;
        $response[Message] = "502 UnAuthorized Access";
        $userClass->apiResponse($response);
    }
}
?>