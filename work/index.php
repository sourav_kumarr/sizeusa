<html>
    <head>
        <title>SizeUSA</title>
        <link rel='stylesheet' id='main-css'  href='css/main.css' type='text/css' media='all' />
        <link rel='stylesheet' href='css/custom.css' type='text/css' media='all' />
        <link rel='stylesheet' href='css/font-awesome.css' type='text/css' media='all' />
        <link rel='stylesheet' href='css/bootstrap.min.css' type='text/css' media='all' />
        <link rel='stylesheet' href='css/bootstrap-datetimepicker.css' type='text/css' media='all' />
    </head>
    <style>
        .col-md-1{
            width:8.33%;
            float:left;
        }
        .col-md-2{
            width:16.66%;
            float:left;
        }
        .col-md-3{
            width:25%;
            float:left;
        }
        .col-md-4{
            width:33.33%;
            float:left;
        }
        .clear{
            clear:both;
        }
        .socialicons{
            font-size: 16px;
        }
        .custombtn{
            -moz-user-select: none;
            background-image: none;
            border: 1px solid transparent;
            border-radius: 4px;
            cursor: pointer;
            display: inline-block;
            font-size: 14px;
            font-weight: 400;
            line-height: 1.42857;
            margin-bottom: 0;
            padding: 6px 12px;
            text-align: center;
            touch-action: manipulation;
            vertical-align: middle;
            white-space: nowrap;
            color:white;
        }
        .tc2logo{
            cursor: pointer;
            height: 32px;
            position: absolute;
            right: 80px;
            top: 30px;
        }
    </style>
    <body>
        <div class="site">
            <header class="site-header header" role="banner">
                <!--<div style="width:100%;padding: 15px 0 35px 0;background:#fff">
                    <div style="width:94%;margin: 0 auto">
                        <div class="col-md-4">
                            <i class="fa fa-map-marker"><span class="calibri"> TC2 Labs LLC 2500 Reliance Avenue
                    Apex NC 27539 USA</span></i>
                        </div>
                        <div class="col-md-2">
                            <i class="fa fa-envelope"><span class="calibri"> info@tc2.com </span></i>
                        </div>
                        <div class="col-md-2">
                            <i class="fa fa-phone"><span class="calibri"> +1 919 380.21.71</span></i>
                        </div>
                        <div class="col-md-2"></div>
                        <div class="col-md-1">
                            <img onclick="window.location='http://tc2.com'" src="images/tc2_logo.png" style="cursor:pointer" />
                        </div>
                        <div class="col-md-1"></div>
                    </div>
                </div>
                <div class="clear"></div>-->
                <div class="header__container">
                    <a href="index.php" class="header__logo">
                        <img src="images/logo1.png" style="max-height:60px;margin-top:10px" />
                    </a>
                    <div class="header__toggler">
                        <a href="#">
                            <svg viewBox="0 0 16 13" xmlns="http://www.w3.org/2000/svg">
                                <g>
                                    <rect id="top-line" x="0" y="0" width="16" height="1"></rect>
                                    <rect id="middle-line" x="0" y="6" width="16" height="1"></rect>
                                    <rect id="bottom-line" x="0" y="12" width="16" height="1"></rect>
                                </g>
                            </svg>
                        </a>
                    </div>
                    <div class="header__list">
                        <nav class="header__menu header__menu--main">
                            <ul class="menu" style="margin-top:0">
                                <li id="index" onclick="window.location='index.php'" class="menuitems activemenuitem">Home</li>
                                <li id="about" onclick="window.location='about.php'" class="menuitems">About</li>
                                <!--<a href="#"><li id="customize" class="menuitems">Customize</li></a>-->
                                <li id="heritage" onclick="window.location='heritage.php'" class="menuitems">Heritage</li>
                                <li id="report" onclick="window.location='#'" class="menuitems">Report Generator</li>
                                <li id="3davatar" onclick="window.location='#'" class="menuitems">3D Avatar Generator</li>
                                <li id="sizematching" onclick="window.location='#'" class="menuitems">Size Matching Report</li>
                                <li id="contact" onclick="window.location='contact.php'" class="menuitems">Contact</li>
                                <li class="menuitems" onclick="signIn()">Login/Register</li>
                            </ul>
                        </nav>
                    </div>
                    <img onclick="window.location='http://tc2.com'" src="images/tc2_logo.png" class="tc2logo" />
                </div>
            </header>
            <main id="wrapper">
                <div data-namespace="front-page" class="home page page-id-9 page-template page-template-front-page page-template-front-page-php container"           data-translations='{"en":{"id":6,"order":0,"slug":"en","locale":"en-US","name":"English","url":"http:\/\/www.scabal.com\/en\/","flag":"http:\/\/www.scabal.com\/wp-content\/plugins\/polylang-pro\/flags\/us.png","current_lang":true,"no_translation":false,"classes":["lang-item","lang-item-6","lang-item-en","lang-item-first","current-lang"]},"zh":{"id":9,"order":0,"slug":"zh","locale":"zh-CN","name":"\u4e2d\u6587 (\u4e2d\u56fd)","url":"http:\/\/www.scabal.com\/zh\/","flag":"http:\/\/www.scabal.com\/wp-content\/plugins\/polylang-pro\/flags\/cn.png","current_lang":false,"no_translation":false,"classes":["lang-item","lang-item-9","lang-item-zh"]},"fr":{"id":439,"order":0,"slug":"fr","locale":"fr-FR","name":"Fran\u00e7ais","url":"http:\/\/www.scabal.com\/fr\/","flag":"http:\/\/www.scabal.com\/wp-content\/plugins\/polylang-pro\/flags\/fr.png","current_lang":false,"no_translation":false,"classes":["lang-item","lang-item-439","lang-item-fr"]},"es":{"id":443,"order":0,"slug":"es","locale":"es-ES","name":"Espa\u00f1ol","url":"http:\/\/www.scabal.com\/es\/","flag":"http:\/\/www.scabal.com\/wp-content\/plugins\/polylang-pro\/flags\/es.png","current_lang":false,"no_translation":false,"classes":["lang-item","lang-item-443","lang-item-es"]},"it":{"id":447,"order":0,"slug":"it","locale":"it-IT","name":"Italiano","url":"http:\/\/www.scabal.com\/it\/","flag":"http:\/\/www.scabal.com\/wp-content\/plugins\/polylang-pro\/flags\/it.png","current_lang":false,"no_translation":false,"classes":["lang-item","lang-item-447","lang-item-it"]},"nl":{"id":451,"order":0,"slug":"nl","locale":"nl-NL","name":"Nederlands","url":"http:\/\/www.scabal.com\/nl\/","flag":"http:\/\/www.scabal.com\/wp-content\/plugins\/polylang-pro\/flags\/nl.png","current_lang":false,"no_translation":false,"classes":["lang-item","lang-item-451","lang-item-nl"]},"de":{"id":492,"order":0,"slug":"de","locale":"de-DE","name":"Deutsch","url":"http:\/\/www.scabal.com\/de\/","flag":"http:\/\/www.scabal.com\/wp-content\/plugins\/polylang-pro\/flags\/de.png","current_lang":false,"no_translation":false,"classes":["lang-item","lang-item-492","lang-item-de"]},"ru":{"id":496,"order":0,"slug":"ru","locale":"ru-RU","name":"\u0420\u0443\u0441\u0441\u043a\u0438\u0439","url":"http:\/\/www.scabal.com\/ru\/","flag":"http:\/\/www.scabal.com\/wp-content\/plugins\/polylang-pro\/flags\/ru.png","current_lang":false,"no_translation":false,"classes":["lang-item","lang-item-496","lang-item-ru"]},"ja":{"id":500,"order":0,"slug":"ja","locale":"ja","name":"\u65e5\u672c\u8a9e","url":"http:\/\/www.scabal.com\/ja\/","flag":"http:\/\/www.scabal.com\/wp-content\/plugins\/polylang-pro\/flags\/jp.png","current_lang":false,"no_translation":false,"classes":["lang-item","lang-item-500","lang-item-ja"]},"ko":{"id":504,"order":0,"slug":"ko","locale":"ko-KR","name":"\ud55c\uad6d\uc5b4","url":"http:\/\/www.scabal.com\/ko\/","flag":"http:\/\/www.scabal.com\/wp-content\/plugins\/polylang-pro\/flags\/kr.png","current_lang":false,"no_translation":false,"classes":["lang-item","lang-item-504","lang-item-ko"]}}'>
                    <div class="lateral-nav-wrapper">
                        <nav class="lateral-nav">
                            <ul>

                            </ul>
                        </nav>
                    </div>
                    <section class="movie">
                        <div class="block block--video">
                            <div class="block__overlay block__video">
                                <video autoplay loop muted data-width="1080" data-height="600">
                                    <source src="images/tailor.mp4" type="video/mp4">
                                </video>
                                <img class="block__video__fallback" src="images/scabal-luxury-tailored-suit-1024x435.jpg" srcset="images/scabal-luxury-tailored-suit-300x127.jpg 300w, images/scabal-luxury-tailored-suit-768x326.jpg 768w, images/scabal-luxury-tailored-suit-1024x435.jpg 1024w, images/scabal-luxury-tailored-suit-450x191.jpg 450w, images/scabal-luxury-tailored-suit-600x255.jpg 600w, images/scabal-luxury-tailored-suit-800x340.jpg 800w, images/scabal-luxury-tailored-suit-900x382.jpg 900w, images/scabal-luxury-tailored-suit-1200x510.jpg 1200w, images/scabal-luxury-tailored-suit-1600x680.jpg 1600w" alt="scabal-luxury-tailored-suit" />
                            </div>
                            <div class="block__overlay movie__text">
                                <div class="block__center">
                                    <h1>Cultural Day will make us one and life joined us with Happiness</h1>
                                    <a href="#"></a>
                                </div>
                            </div>
                        </div>
                    </section>
                    <section class="tailoring">
                        <div class="block block--video">
                            <div class="block__overlay block__video">
                                <video autoplay loop muted data-width="1080" data-height="600">
                                    <source src="images/tailor.mp4" type="video/mp4">
                                </video>
                            </div>
                            <div class="block__title">
                                <h2>Report Generation</h2>
                                <h3>Report Generator with the Measurement you want (On screen in pdf and excel)</h3>
                            </div>
                        </div>
                        <div class="block block--text" data-fit-text>
                            <div class="block--text__wrapper">
                                <span class="divider"></span>
                                <p>A survey can only be truly valuable when it's reliable and representative
                                for your business. However, determining the ideal survey sample size and population
                                can prove tricky. In other words, who will you be surveying and how many people?
                                No idea? No worries. We're here to help!
                                </p>
                                <span class="divider"></span>
                                <a>Find the Perfect Fit</a>
                            </div>
                        </div>
                        <div class="block block--image tailoring__curzon">
                            <img class="block--image__background"
                            src="images/scabal-tailored-suits-1024x684.jpg"
                            srcset="images/scabal-tailored-suits-300x200.jpg 300w, images/scabal-tailored-suits-768x513.jpg 768w, images/scabal-tailored-suits-1024x684.jpg 1024w, images/scabal-tailored-suits-600x401.jpg 600w, images/scabal-tailored-suits-800x534.jpg 800w, images/scabal-tailored-suits-900x601.jpg 900w, images/scabal-tailored-suits-1200x801.jpg 1200w, images/scabal-tailored-suits-1600x1068.jpg 1600w"
                            sizes="(min-width: 48.75em) 50vw,100vw" alt="scabal-tailored-suits"/>
                        </div>
                        <div class="block block--image tailoring__soho">
                            <img class="block--image__background"
                            src="images/scabal-tailored-suit-curzon-676x1024.jpg"
                            srcset="images/scabal-tailored-suit-curzon-198x300.jpg 198w, images/scabal-tailored-suit-curzon-768x1163.jpg 768w, images/scabal-tailored-suit-curzon-676x1024.jpg 676w, images/scabal-tailored-suit-curzon-450x682.jpg 450w, images/scabal-tailored-suit-curzon-600x909.jpg 600w, images/scabal-tailored-suit-curzon-800x1212.jpg 800w, images/scabal-tailored-suit-curzon-900x1363.jpg 900w, images/scabal-tailored-suit-curzon-1200x1818.jpg 1200w, images/scabal-tailored-suit-curzon-1600x2424.jpg 1600w"
                            sizes="(min-width: 48.75em) 50vw,100vw" alt="scabal-tailored-suit-curzon"/>
                        </div>
                        <div class="block block--image tailoring__mayfair">
                            <img class="block--image__background"
                            src="images/scabal-tailored-suit-clevedon-676x1024.jpg"
                            srcset="images/scabal-tailored-suit-clevedon-198x300.jpg 198w, images/scabal-tailored-suit-clevedon-768x1163.jpg 768w, images/scabal-tailored-suit-clevedon-676x1024.jpg 676w, images/scabal-tailored-suit-clevedon-450x682.jpg 450w, images/scabal-tailored-suit-clevedon-600x909.jpg 600w, images/scabal-tailored-suit-clevedon-800x1212.jpg 800w, images/scabal-tailored-suit-clevedon-900x1363.jpg 900w, images/scabal-tailored-suit-clevedon-1200x1818.jpg 1200w, images/scabal-tailored-suit-clevedon-1600x2424.jpg 1600w"
                            sizes="(min-width: 48.75em) 50vw,100vw" alt="scabal-tailored-suit-clevedon"/>
                        </div>
                    </section>
                    <section class="fabrics">
                        <div class="block block--video">
                            <div class="block__overlay block__video">
                                <video autoplay loop data-width="710" data-height="680" src="images/shutterstock_v9744443.mov"></video>
                            </div>
                            <div class="block__title">
                                <h2>Heritage is Diversity</h2>
                                <h3>All Social Demographic</h3>
                            </div>
                        </div>
                        <div class="block block--text" data-fit-text>
                            <div class="block--text__wrapper">
                                <span class="divider"></span>
                                <p>One of the many beauties and strength of the USA is his huge diversity on Social
                                    demographics field. An great example: The Multiracial Americans are at the cutting edge of
                                    social and demographic change in the U.S.
                                </p>
                                <span class="divider"></span>
                                <a href="heritage.php">Heritage</a>
                            </div>
                        </div>
                        <div class="block block--image fabrics__collection">
                            <img class="block--image__background"
                            src="images/scabal-luxury-fabrics-huddersfield-1024x768.jpg"
                            srcset="images/scabal-luxury-fabrics-huddersfield-300x225.jpg 300w, images/scabal-luxury-fabrics-huddersfield-768x576.jpg 768w, images/scabal-luxury-fabrics-huddersfield-1024x768.jpg 1024w, images/scabal-luxury-fabrics-huddersfield-450x337.jpg 450w, images/scabal-luxury-fabrics-huddersfield-600x450.jpg 600w, images/scabal-luxury-fabrics-huddersfield-800x600.jpg 800w, images/scabal-luxury-fabrics-huddersfield-900x675.jpg 900w, images/scabal-luxury-fabrics-huddersfield-1200x900.jpg 1200w, images/scabal-luxury-fabrics-huddersfield-1600x1199.jpg 1600w"
                            sizes="(min-width: 48.75em) 50vw,100vw" alt="scabal-luxury-fabrics-huddersfield" />
                        </div>
                    </section>
                    <section class="club">
                        <div class="block block--video">
                            <div class="block__overlay block__video">
                                <video autoplay loop muted data-width="1080" data-height="600" data-heightm="2">
                                    <source src="images/shutterstock_v13319186.mp4" type="video/mp4">
                                </video>
                            </div>
                            <div class="block__title">
                                <h2>3D Avatar Generation</h2>
                                <h3>Selection Presented 3D Avatar (Based on average of OBJ files from SizeUSA)</h3>
                            </div>
                        </div>
                        <div class="block block--text block--small" data-fit-text>
                            <div class="block--text__wrapper">
                                <span class="divider"></span>
                                <p>With the formula is by Cochran.<br>With our 10,000 scans we are with a 1% error representative
                                for USA population with 95% confidence .Its inheritance is our strength Our whereabouts and
                                our which is which Our breakthrough and our future Our undivided tension and our nation
                                in colors
                                </p>
                                <span class="divider"></span>
                                <a>Request for your 3D Model</a>
                            </div>
                        </div>
                        <div class="block block--image block--small">
                            <img class="block--image__background"
                            src="images/scabal-savile-row-tailor.jpg"
                            srcset="images/scabal-savile-row-tailor-300x282.jpg 300w, images/scabal-savile-row-tailor-768x721.jpg 768w, images/scabal-savile-row-tailor-450x423.jpg 450w, images/scabal-savile-row-tailor-600x563.jpg 600w, images/scabal-savile-row-tailor-800x751.jpg 800w, images/scabal-savile-row-tailor.jpg 868w"
                            sizes="(min-width: 48.75em) 25vw,100vw" alt="scabal-savile-row-tailor" />
                        </div>
                        <div class="block block--image block--small">
                            <img class="block--image__background"
                            src="images/scabal-tailor-london.jpg"
                            srcset="images/scabal-tailor-london-300x241.jpg 300w, images/scabal-tailor-london-768x618.jpg 768w, images/scabal-tailor-london-450x362.jpg 450w, images/scabal-tailor-london-600x483.jpg 600w, images/scabal-tailor-london-800x644.jpg 800w, images/scabal-tailor-london-900x724.jpg 900w, images/scabal-tailor-london.jpg 1014w"
                            sizes="(min-width: 48.75em) 25vw, 100vw" alt="scabal-tailor-london"/>
                        </div>
                    </section>
                    <section class="heritage">
                        <div class="block block--video">
                            <div class="block__overlay block__video">
                                <video autoplay loop muted data-width="840" data-height="690">
                                    <source src="images/shutterstock_v12205250.mov">
                                </video>
                            </div>
                            <div class="block__title">
                                <h2>Size Matching </h2>
                                <h3>Have the Perfect Fit</h3>
                            </div>
                        </div>
                        <div class="block block--text" data-fit-text>
                            <div class="block--text__wrapper">
                                <span class="divider"></span>
                                <p>
                                    <li>Demographic information of each subject</li>
                                    <li>Over 300 standard measurements (thousands of measurements possible)</li>
                                    <li>Ability to extract custom measurements 3D scan file of each subject</li>
                                    <li>Ability to create 3D avatars</li>
                                </p>
                                <span class="divider"></span>
                                <a>Learn more about Size Matching</a>
                            </div>
                        </div>
                        <div class="block block--text heritage__fact">
                            <div class="block--text__wrapper">
                                <h3>Perfect Size Found</h3>
                                <p>All reports based on spreadsheet SizeUSA</p>
                            </div>
                        </div>
                        <div class="block block--image heritage__mobile-picture"></div>
                    </section>
                    <div class="scroll-down">
                        <div class="scroll-down__content">
                            <span>Scroll</span>
                            <svg width="16px" height="24px" viewBox="0 0 16 24" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                                <g id="scroll-down" transform="translate(0, -65.000000)" fill="#C0AB89">
                                    <g transform="translate(-89.000000, -16.000000)" id="scroll-icon">
                                        <g transform="translate(90.000000, 81.000000)">
                                            <path d="M13.0909091,7.54925455 L13.0909091,16.2765273 C13.0909091,19.2841636 10.644,21.7310727 7.63636364,21.7310727 C4.62872727,21.7310727 2.18181818,19.2841636 2.18181818,16.2765273 L2.18181818,7.54925455 C2.18181818,4.54161818 4.62872727,2.09470909 7.63636364,2.09470909 C10.644,2.09470909 13.0909091,4.54161818 13.0909091,7.54925455 L13.0909091,7.54925455 Z M6.54545455,0.000163636364 C2.85109091,0.531981818 0,3.7098 0,7.54925455 L0,16.2765273 C0,20.4874364 3.42490909,23.9128909 7.63636364,23.9128909 C11.8467273,23.9128909 15.2727273,20.4874364 15.2727273,16.2765273 L15.2727273,7.54925455 C15.2727273,3.7098 12.4216364,0.531981818 8.72727273,0.000163636364 L6.54545455,0.000163636364 Z" id="mouse-outline"></path>
                                            <path d="M6.54545455,6.45834545 L6.54545455,8.64016364 C6.54545455,9.24234545 7.03363636,9.73107273 7.63636364,9.73107273 C8.23854545,9.73107273 8.72727273,9.24234545 8.72727273,8.64016364 L8.72727273,6.45834545 C8.72727273,5.85561818 8.23854545,5.36743636 7.63636364,5.36743636 C7.03363636,5.36743636 6.54545455,5.85561818 6.54545455,6.45834545" id="mouse-button"></path>
                                        </g>
                                    </g>
                                </g>
                            </svg>
                            <span>Down</span>
                            <svg width="10px" height="42px" viewBox="0 0 10 21" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                                <g id="scroll-down-arrow" fill="#C0AB89" transform="translate(-2,-5) rotate(90, 11, 11)" >
                                    <g id="scroll-down-arrow-shape">
                                        <path d="M24.857891,14.8491619 C25.0507278,14.6515042 25.0468196,14.3349458 24.8491619,14.142109 C24.6515042,13.9492722 24.3349458,13.9531804 24.142109,14.1508381 L20.142109,18.2508385 C19.9492722,18.4484962 19.9531804,18.7650546 20.1508381,18.9578914 C20.3484958,19.1507282 20.6650542,19.14682 20.857891,18.9491623 L24.857891,14.8491619 Z" id="oblicBottom"></path>
                                        <path d="M5.5,14 C5.22385763,14 5,14.2238576 5,14.5 C5,14.7761424 5.22385763,15 5.5,15 L24.5,15 C24.7761424,15 25,14.7761424 25,14.5 C25,14.2238576 24.7761424,14 24.5,14 L5.5,14 Z" id="horizontalLine"></path>
                                        <path d="M24.1464466,14.8535534 C24.3417088,15.0488155 24.6582912,15.0488155 24.8535534,14.8535534 C25.0488155,14.6582912 25.0488155,14.3417088 24.8535534,14.1464466 L20.8535534,10.1464466 C20.6582912,9.95118446 20.3417088,9.95118446 20.1464466,10.1464466 C19.9511845,10.3417088 19.9511845,10.6582912 20.1464466,10.8535534 L24.1464466,14.8535534 Z" id="oblicTop"></path>
                                    </g>
                                </g>
                            </svg>
                        </div>
                    </div>
                </div><!-- end .container -->
            </main><!-- end #wrapper -->
            <footer class="site-footer footer" role="contentinfo">
                <div class="col-md-3 footerblock">
                    <p class="blockheadtext">Telephone</p>
                    <p class="blockvalue"><i class="fa fa-phone"></i> +1 919 380.21.71</p>
                    <p class="blockvalue"><i class="fa fa-fax"></i> +1 919 380.21.81</p>
                </div>
                <div class="col-md-3 footerblock">
                    <p class="blockheadtext">E-Mail</p>
                    <p class="blockvalue"><i class="fa fa-envelope"></i> info@tc2.com</p>
                    <p class="blockvalue"><a href="http://tc2.com"><i class="fa fa-globe"></i> www.tc2.com</a></p>
                </div>
                <div class="col-md-3 footerblock">
                    <p class="blockheadtext">Social</p>
                    <p class="blockvalue">
                        <i style="margin-left:0" class="fa fa-facebook socialicons"></i>
                        <i class="fa fa-google-plus socialicons"></i>
                        <i class="fa fa-twitter socialicons"></i>`
                        <i class="fa fa-youtube socialicons"></i>
                        <i class="fa fa-rss socialicons"></i>
                    </p>
                </div>
                <div class="col-md-3 footerblock" style="border:0">
                    <p class="blockheadtext">Address</p>
                    <p class="blockvalue"><i class="fa fa-map-marker"></i> TC2 Labs LLC<br>2500 Reliance Avenue<br>
                        Apex NC 27539<br>USA</p>
                </div>
                <div class="clear"></div>
                <div class="footer__connect" style="margin-top:30px;text-align: center">
                    <div class="footer__newsletter">
                        <p class="subtitle h3">Newsletter</p>

                        <p class="footer__newsletter__feedback"></p>

                        <form action="#" type="POST" method="POST">
                            <input type="hidden" name="action" value="newsletter_form" />
                            <input type="email" name="email" placeholder="Your email address" autocomplete="off" />
                            <button type="submit">Join</button>
                        </form>
                    </div>
                </div>
            </footer>
        </div> <!-- end .site -->
        <section class="wechat" data-wechat-popup data-prevent-scroll="true">
            <div class="wechat__layer"></div>
            <div class="wechat__box">
                <div class="wechat__close"></div>
            </div>
        </section>
        <div class="needle-loading">
            <svg id="loadingScabal" x="0px" y="0px" viewBox="0 0 130 30" width="120px" height="30px"
            style="background-color:transparent;"
            xml:space="preserve">
                <path id="needle" class="lst5" d="M5.08,0C2.28,0,0,.17,0,1.5S2.28,3,5.08,3C7,3,90,1.47,90,1.47S7,0,5.08
                ,0ZM4.55,2.12C3.18,2.12,1.62,2,1.62,1.46S3.22,0.8,4.55.8C7,0.8,10.6.92,10.6,1.46S7,2.12,4.55,2.12Z"
                transform="translate(43,15)"/>
                <polyline id="sinusoide" class="lst6" points="0,0 0,0 0,0 0,0"/>
            </svg>
        </div>
        <div class="loading-text" id="loadingText">Loading</div>
        <script type='text/javascript'>
            var SCABALINFO = {"BASEURL":"http:\/\/www.scabal.com\/wp-content\/themes\/scabal","ADMIN_BAR_VISIBLE":"","FACEBOOK_KEY":"1670372613289998","GOOGLE_MAPS_KEY":"AIzaSyDPH0ECl3UQNoffqB-r1KklDllaTVRTFWI","CURRENT_LANG":"en","CURRENT_LOCATION":"BE"};
        </script>
        <script type='text/javascript' src='js/main.js'></script>
    </body>
</html>
<div id ="myModal" class="modal fade" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="gridSystemModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="gridSystemModalLabel">Modal title</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-4">.col-md-4</div>
                    <div class="col-md-4 col-md-offset-4">.col-md-4 .col-md-offset-4</div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<div id ="loginModal" class="modal fade" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="gridSystemModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content" style="border-radius:0;">
            <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <div class="row">
                    <div class="col-md-12">
                        <p class="loginheader">Login to SizeUSA</p>
                        <div class="line"></div >
                    </div>
                    <div class="col-md-1"></div>
                    <div class="col-md-10">
                        <div class="col-md-12" style="margin-bottom:30px">
                            <label class="label label-danger">Please Fill all the Required Fields</label>
                        </div>
                        <div class="col-md-12 form-group">
                            <label>UserName / E-Mail</label>
                            <input type="text" placeholder="Enter UserName or E-Mail Address" class="form-control" />
                        </div>
                        <div class="col-md-12 form-group">
                            <label>Password</label>
                            <input type="password" placeholder="Enter Password" class="form-control" />
                        </div>
                        <div class="col-md-6 form-group">
                            <label>Forgot Password ?</label>
                        </div>
                        <div class="col-md-6 form-group">
                            <input type="button" class="btn btn-danger pull-right" value="Login Now" />
                        </div>
                    </div>
                    <div class="col-md-1"></div>
                    <div class="clear"></div>
                    <hr>
                </div>
                <p class="signuplink">New User ? Sign Up <a style="cursor:pointer" onclick="switchmodel('login')">Here</a></p>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<div id ="registerModal" class="modal fade" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="gridSystemModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content" style="border-radius:0;">
            <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <div class="row">
                    <div class="col-md-12">
                        <p class="loginheader">Register With SizeUSA</p>
                        <div class="line"></div >
                    </div>
                    <div class="col-md-12" style="margin-bottom:30px">
                        <label class="label label-danger">Please Fill all the Required Fields</label>
                    </div>
                    <div class="col-md-12">
                        <div class="col-md-6 form-group">
                            <input type="text" placeholder="Enter First Name" class="form-control" />
                        </div>
                        <div class="col-md-6 form-group">
                            <input type="text" placeholder="Enter Last Name" class="form-control" />
                        </div>
                        <div class="col-md-6 form-group">
                            <input type="text" placeholder="Enter E-Mail Address" class="form-control" />
                        </div>
                        <div class="col-md-6 form-group">
                            <input type="password" placeholder="Enter Password" class="form-control" />
                        </div>
                        <div class="col-md-6 form-group">
                            <select class="form-control countries" id="countryId" onchange="countrydate()" >
                                <option value="" selected="selected">Select Country</option>
                            </select>
                        </div>
                        <div class="col-md-6 form-group">
                            <select class="form-control states" id="stateId">
                                <option value="" selected="selected">Select State</option>
                            </select>
                        </div>
                        <div class="col-md-6 form-group" id="cityfeild">
                            <select class="form-control cities" id="cityId">
                                <option value="" selected="selected">Select City</option>
                            </select>
                        </div>
                        <div class="col-md-6 form-group">
                            <select class="form-control" >
                                <option value="" selected="selected">Select Gender</option>
                                <option value="Male">Male</option>
                                <option value="Female">Female</option>
                            </select>
                        </div>
                        <div class="col-md-6 form-group">
                            <input type="text" placeholder="Pick Date of Birth" id="inputdob" class="form-control" />
                        </div>
                        <div class="col-md-6 form-group">
                            <input type="text" placeholder="Enter Stylist Name" class="form-control" />
                        </div>
                        <div class="col-md-6 form-group" style="padding: 0">
                            <div class="col-md-4" style="padding-right:0">
                                <select class="form-control" style="padding:0" id="heightparam" onchange="heightFeild()" >
                                    <option value="" selected="selected">Parameter</option>
                                    <option value="Inches" >Inches</option>
                                    <option value="Centimeters" >Centimeters</option>
                                    <option value="Feets" >Feets</option>
                                </select>
                            </div>
                            <div class="col-md-8" id="heightFeilds">
                                <input type="text" id='height' class="form-control" placeholder="Enter Height"/>
                            </div>
                        </div>
                        <div class="col-md-6 form-group" style="padding: 0">
                            <div class="col-md-4" style="padding-right:0">
                                <select class="form-control" style="padding: 0" >
                                    <option value="" selected="selected">Parameter</option>
                                    <option value="Pounds">Pounds</option>
                                    <option value="Kilograms">Kilograms</option>
                                    <option value="Stones">Stones</option>
                                </select>
                            </div>
                            <div class="col-md-8">
                                <input type="text" class="form-control" placeholder="Enter Weight"/>
                            </div>
                        </div>
                        <div class="col-md-6 form-group">
                            <select class="form-control" >
                                <option value="English (US)" selected="selected">English (US)</option>
                                <option value="English (US)">English (UK)</option>
                                <option value="French">French</option>
                            </select>
                        </div>
                        <div class="col-md-6 form-group">
                            <input type="file" class="form-control" />
                        </div>
                        <div class="col-md-6 form-group">
                            <p class="signuplink">Already Register ? Login <a style="cursor:pointer"
                            onclick="switchmodel('register')">Here</a></p>
                        </div>
                        <div class="col-md-6 form-group">
                            <input type="button" class="btn btn-danger pull-right" value="Register Now" />
                        </div>
                    </div>
                    <div class="col-md-1"></div>
                </div>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<script src="js/jquery.min.js" type="text/javascript"></script>
<script src="js/myscript.js" type="text/javascript"></script>
<script src="js/location.js" type="text/javascript"></script>
<script src="js/moment.min.js" type="text/javascript"></script>
<script src="js/bootstrap.min.js" type="text/javascript"></script>
<script src="js/bootstrap-datetimepicker.js" type="text/javascript"></script>
