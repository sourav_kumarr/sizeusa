<?php
namespace Modals;
require_once "CONNECT.php";
require_once('PHPExcel.php');
class USERS
{
    private $link = null;
    private $link2 = null;
    public $CurrentDateTime = null;
    function __construct()
    {
        $this->link = new CONNECT();
        $this->link2 = new CONNECT();
        $this->CurrentDateTime = date("Y-m-d h:i:s");
        $this->objPHPExcel = new \PHPExcel();
        // set default font
        $this->objPHPExcel->getDefaultStyle()->getFont()->setName('Calibri');
        // set default font size
        $this->objPHPExcel->getDefaultStyle()->getFont()->setSize(10);
        // create the writer
        $this->objWriter = \PHPExcel_IOFactory::createWriter($this->objPHPExcel, "Excel2007");
        // currency format, € with < 0 being in red color
        $currencyFormat = '#,#0.## \€;[Red]-#,#0.## \€';
        // number format, with thousands separator and two decimal points.
        $numberFormat = '#,#0.##;[Red]-#,#0.##';
        // writer already created the first sheet for us, let's get it
        $this->objSheet = $this->objPHPExcel->getActiveSheet();
    }
    public function RegisterUser($fname,$lname,$email,$password,$country,$state,$city,$gender,$dob,$stylist,$height,
    $weight,$language){
        if($this->link->Connect()){
            $file_response = $this->uploadPicture();
            if($file_response[Status] == Error){
                $this->link->response[Status] == Error;
                $this->link->response[Message] == $file_response[Message];
            }else {
                $file_name = $file_response['file_name'];
                if($file_name != ""){
                    $file_name = MainServer."/api/Files/images/".$file_name;
                }
                $query = "insert into wp_customers(name,b_country,b_state,b_city,dob,driver_licence,face_login,height,
                profile_pic,email,password,email_receive,stylist,gender,lang,weight,site_name,app_type) values 
                ('$fname $lname','$country','$state','$city','$dob','false','false','$height Inches','$file_name','$email',
                '$password','false','$stylist','$gender','$language','$weight Pounds','SizeUSA','Website')";
                $result = mysqli_query($this->link->Connect(), $query);
                if ($result) {
                    $user_id = $this->link->lastId();
                    $this->link->response[Status] = Success;
                    $this->link->response[Message] = "User Registered Successfully";
                    $this->link->response['lastId'] = $user_id;
                }
                else{
                    $this->link->response[Status] = Error;
                    $this->link->response[Message] = $this->link->sqlError();
                }
            }
        }
        else{
            $this->link->response[Status] = Error;
            $this->link->response[Message] = $this->link->sqlError();
        }
        return $this->link->response;
    }
    public function checkEmailExistance($email)
    {
        if($this->link->Connect()){
            $query = "select * from wp_customers where email = '$email'";
            $result = mysqli_query($this->link->Connect(),$query);
            if($result){
                $num = mysqli_num_rows($result);
                if($num>0){
                    $this->link->response[Status] = Error;
                    $this->link->response[Message] = "Email Already Registered";
                }
                else{
                    $this->link->response[Status] = Success;
                    $this->link->response[Message] = "New User";
                }
            }
            else{
                $this->link->response[Status] = Error;
                $this->link->response[Message] = $this->link->sqlError();
            }
        }
        else{
            $this->link->response[Status] = Error;
            $this->link->response[Message] = $this->link->sqlError();
        }
        return $this->link->response;
    }
    public function getParticularUserData($userId){
        if($this->link->Connect()){
            $query = "select * from wp_customers where user_id = '$userId'";
            $result = mysqli_query($this->link->Connect(),$query);
            if($result){
                $num = mysqli_num_rows($result);
                if($num>0){
                    $userData = mysqli_fetch_assoc($result);
                    $this->link->response[Status] = Success;
                    $this->link->response[Message] = "User Data Found";
                    $this->link->response['userData'] = $userData;
                }
                else{
                    $this->link->response[Status] = Error;
                    $this->link->response[Message] = "UnAuthorized User";
                }
            }
            else{
                $this->link->response[Status] = Error;
                $this->link->response[Message] = $this->link->sqlError();
            }
        }
        else{
            $this->link->response[Status] = Error;
            $this->link->response[Message] = $this->link->sqlError();
        }
        return $this->link->response;
    }
    public function updateProfile($user_id, $fname, $lname, $contact, $gender, $dob, $height, $weight){
        if($this->link->Connect() && $this->link2->Connect2()){
            $file_response = $this->uploadPicture();
            if($file_response[Status] == Error){
                $this->link->response[Status] == Error;
                $this->link->response[Message] == $file_response[Message];
            }else {
                $file_name = $file_response['file_name'];
                if($file_name != "") {
                    $file_name = MainServer."/api/Files/users/".$file_name;
                    $query = "update wp_customers set name='$fname $lname',mobile='$contact',gender='$gender',dob='$dob',
                    height='$height Inches',weight='$weight Kilograms',profile_pic='$file_name' where user_id = '$user_id'";
                }else{
                    $query = "update wp_customers set name='$fname $lname',mobile='$contact',gender='$gender',dob='$dob',
                    height='$height Inches',weight='$weight Kilograms' where user_id = '$user_id'";
                }
                $result = mysqli_query($this->link2->Connect2(), $query);
                if ($result) {
                    $file_name = $file_response['file_name'];
                    if($file_name != "") {
                        $file_name = MainServer."/api/Files/users/".$file_name;
                        $query2 = "update users set fname='$fname',lname='$lname',contact='$contact',gender='$gender',dob='$dob',
                        height_in='$height',weight='$weight',user_profile='$file_name' where user_id = '$user_id'";
                    }else{
                        $query2 = "update users set fname='$fname',lname='$lname',contact='$contact',gender='$gender',dob='$dob',
                        height_in='$height',weight='$weight' where user_id = '$user_id'";
                    }
                    $result2 = mysqli_query($this->link->Connect(), $query2);
                    if ($result2) {
                        $this->link->response[Status] = Success;
                        $this->link->response[Message] = "User Detail Updated Successfully";
                    } else {
                        $this->link->response[Status] = Error;
                        $this->link->response[Message] = $this->link->sqlError();
                    }
                } else {
                    $this->link->response[Status] = Error;
                    $this->link->response[Message] = $this->link2->sqlError2();
                }
            }
        }
        else{
            $this->link->response[Status] = Error;
            $this->link->response[Message] = $this->link->sqlError();
        }
        return $this->link->response;
    }

    public function userLogin($email,$password)
    {
        if($this->link->Connect()){
            $query = "select * from wp_customers where email='$email' and password='$password'";
            $result = mysqli_query($this->link->Connect(),$query);
            if($result){
                $num = mysqli_num_rows($result);
                if($num>0){
                    $userData = mysqli_fetch_assoc($result);
                    $this->link->response[Status] = Success;
                    $this->link->response[Message] = "Login Success";
                    $this->link->response['userData'] = $userData;
                }
                else{
                    $this->link->response[Status] = Error;
                    $this->link->response[Message] = "Invalid User Credential";
                }
            }
            else{
                $this->link->response[Status] = Error;
                $this->link->response[Message] = $this->link->sqlError();
            }
        }
        else{
            $this->link->response[Status] = Error;
            $this->link->response[Message] = $this->link->sqlError();
        }
        return $this->link->response;
    }
    public function uploadPicture(){
        if(isset($_FILES['user_profile'])){
            $file_name = $_FILES['user_profile']['name'];
            $file_tmp = $_FILES['user_profile']['tmp_name'];
            $file_ext=strtolower(end(explode('.',$file_name)));
            $expensions= array("jpeg","jpg","png");
            if(in_array($file_ext,$expensions)=== false){
                $this->link->response[Status] = Error;
                $this->link->response[Message] = "File Should Be JPG,JPEG and PNG Formats Only";
            }
            else{
                $file_name = "File".rand(000000,999999).".".$file_ext;
                if(move_uploaded_file($file_tmp,"Files/images/".$file_name)){
                    $this->link->response[Status] = Success;
                    $this->link->response[Message] = "File Uploaded Successfully";
                    $this->link->response['file_name'] = $file_name;
                }
                else{
                    $this->link->response[Status] = Error;
                    $this->link->response[Message] = "Error While Uploading File";
                }
            }
        }
        else{
            $this->link->response[Status] = Error;
            $this->link->response[Message] = "File Not Selected";
        }
        return $this->link->response;
    }
    public function sendMaill($email)
    {
        require 'SMTP/PHPMailerAutoload.php';
        $mail = new \PHPMailer();
        $mail->isSMTP();
        $mail->Host = 'md-in-68.webhostbox.net';
        $mail->SMTPAuth = true;
        $mail->Username = 'noreply@stsmentor.com';
        $mail->Password = 'noreply@sts';
        $mail->SMTPSecure = 'TLS';
        $mail->Port = 587;
        $mail->setFrom('noreply@stsmentor.com', 'Scan2fit');   // sender
        $mail->addAddress($email);
        //$mail->addCC('sbsunilbhatia9@gmail.com');				//to add cc
        $mail->addAttachment('');         // Add attachments
        $mail->addAttachment('');    // Optional name
        $mail->isHTML(true);                                  // Set email format to HTML
        $mail->Subject = 'noreply@Size Matching';
        $mail->Body='<div style="width:100%;background:#ccc;padding-bottom:36px"><div style="width:80%;margin: 0 10%">
        <div style="background:#eee;border-bottom:1px solid #ccc;height:63px;padding:10px 0 0 10px"><img style="float:left;
        height:50px" src="http://scan2fit.com/size/images/logo.png" alt="Size Matching" />
        <p style="float: left;color:#666;font-size:15px;font-weight:bold;margin-left:10px">Hello ! Guest</p>
        </div><div style="background:#fff;margin-top:-15px;padding-left:69px;padding-bottom:35px">
        <p style="color:green;line-height:40px;font-weight:bold;font-size:15px">Greetings of the Day,</p>
        <p>Welcome To Scan-2-Fit Family</p><p>Here Is The Change Password link . Click here to Change your Account Password.</p>
        <a style="text-decoration:none" href="'.MainServer.'/api/registerUser.php?_='.$email.'" >
        <input type="button" style="background:#d14130;border-radius:3px;color: white;font-weight:600;margin: 10px 0;
        padding: 10px 20px" value="Change Password" /></a>
        <div style="background:#eee;border:1px solid #ccc;height:63px;padding:0 0 10px 10px">
        <p style="font-weight:bold;color:#666">Thanks & Regards<br><br>Scan-2-Fit Team</p>
        </div></div></div>';
        $mail->AltBody = '';
        if(!$mail->send())
        {
            return $mail->ErrorInfo;
        }
        else {
            return "Email has been sent Successfully to your registered email address";
        }
    }
    public function changeForgotPassword($newPassword,$email){
        if($this->link2->connect2()) {
            $query = "select * from wp_customers where email = '$email'";
            $result = mysqli_query($this->link2->connect2(), $query);
            if ($result) {
                $num = mysqli_num_rows($result);
                if ($num > 0) {
                    $update = mysqli_query($this->link2->connect2(), "UPDATE wp_customers SET password='$newPassword' WHERE email='$email'");
                    if ($update) {
                        $this->link->response[STATUS] = Success;
                        $this->link->response[MESSAGE] = "Password Has Been Changed Successfully";
                    } else {
                        $this->link->response[STATUS] = Error;
                        $this->link->response[MESSAGE] = $this->link2->sqlError2();
                    }
                } else {
                    $this->link->response[STATUS] = Error;
                    $this->link->response[MESSAGE] = "Invalid User";
                }
            } else {
                $this->link->response[STATUS] = Error;
                $this->link->response[MESSAGE] = $this->link2->sqlError2();
            }
        }
        else{
            $this->link->response[STATUS] = Error;
            $this->link->response[MESSAGE] = "Connection Error";
        }
        return $this->link->response;
    }
    public function getSize(){
        $link = $this->link->connect();
        if($link) {
            session_start();
            $user_id = $_SESSION['sf_user_id'];
            $query = "select * from users where user_id = '$user_id'";
            $result = mysqli_query($link, $query);
            if ($result) {
                $num = mysqli_num_rows($result);
                if ($num > 0) {
                    $rows = mysqli_fetch_assoc($result);
                    $height = $rows['height_in'];
                    $height = $height*2.54; ///coversion inches to cm
                    $min_height = $height-2;
                    $max_height = $height+2;
                    $weight = $rows['weight'];
                    $weight = $weight*0.453592; ///conversion of lbs to kilogram
                    $min_weight = $weight-2;
                    $max_weight = $weight+2;
                    $gender = $rows['gender'];
                    $race = $rows['race'];
                    $neck = $rows['neck'];
                    $waist = $rows['waist'];
                    $waist = $waist*2.54; ///conversion inches to cm
                    $sizeresponse = $this->getsizeData($min_height,$max_height,$min_weight,$max_weight,$race,$neck,$waist);
                    if($sizeresponse[Status] == Error){
                        $this->link->response[Status]=Error;
                        $this->link->response[Message]="No Size Found";
                    }else{
                        $this->link->response[Status] = Success;
                        $this->link->response[Message] = "Size Found";
                        $this->link->response['sizeData_in'] = $sizeresponse['sizeData_in'];
                        $this->link->response['sizeData_cm'] = $sizeresponse['sizeData_cm'];
                        $this->createOrdFile($sizeresponse['sizeData_in'],$rows,"in");
                        $this->createOrdFile($sizeresponse['sizeData_cm'],$rows,"cm");
                    }
                } else {
                    $this->link->response[STATUS] = Error;
                    $this->link->response[MESSAGE] = "Invalid User";
                }
            } else {
                $this->link->response[STATUS] = Error;
                $this->link->response[MESSAGE] = $this->link->sqlError();
            }
        }
        else{
            $this->link->response[STATUS] = Error;
            $this->link->response[MESSAGE] = "Connection Error";
        }
        return $this->link->response;
    }
    public function createOrdFile($sizeData,$userData,$ut){
        $link = $this->link->connect();
        if($link) {
            if(!file_exists("Files/users/ord/".$ut."ord".$_SESSION['sf_user_id'].".ord")){
                $myfile = fopen("Files/users/ord/".$ut."ord".$_SESSION['sf_user_id'].".ord", "w") or die("Unable to open file!");
                $content = "\n\nUser Detail\n\n";
                foreach ($userData as $key=>$val) {
                    $content = $content.$key." : ".$val."\n";
                }
                $content = $content."\n\nMeasurements are\n\n";
                foreach ($sizeData as $key=>$val) {
                    $content = $content.$key." : ".$val."\n";
                }
                fwrite($myfile, $content);
                fclose($myfile);
            }else{
                unlink("Files/users/ord/".$ut."ord".$_SESSION['sf_user_id'].".ord");
                $myfile = fopen("Files/users/ord/".$ut."ord".$_SESSION['sf_user_id'].".ord", "w") or die("Unable to open file!");
                $content = "\n\nUser Detail\n\n";
                foreach ($userData as $key=>$val) {
                    $content = $content.$key." : ".$val."\n";
                }
                $content = $content."\n\nMeasurements are\n\n";
                foreach ($sizeData as $key=>$val) {
                    $content = $content.$key." : ".$val."\n";
                }
                fwrite($myfile, $content);
                fclose($myfile);
            }
            if(!file_exists("Files/users/ord/".$ut."ord".$_SESSION['sf_user_id'].".xls")){
                $this->objSheet->mergeCells('A1:C1');
                $this->objSheet->getStyle('A1')->getFont()->setSize(12);
                $this->objSheet->getStyle('A1')->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                $this->objSheet->getCell('A1')->setValue('Size Found Data');

                $this->objSheet->getStyle('A3:F3')->getFont()->setBold(true)->setSize(10);
                $this->objSheet->getCell('A3')->setValue('S.No');
                $this->objSheet->getCell('B3')->setValue('Parameters');
                $this->objSheet->getCell('C3')->setValue('Values');
                // autosize the columns
                $this->objSheet->getColumnDimension('A')->setAutoSize(true);
                $this->objSheet->getColumnDimension('B')->setAutoSize(true);
                $this->objSheet->getColumnDimension('C')->setAutoSize(true);

                $this->objSheet->getStyle('A')->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
                $this->objSheet->getStyle('B')->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
                $this->objSheet->getStyle('C')->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
                // we could get this data from database, but here we are writing for simplicity

                /*$myfile = fopen("Files/users/ord/".$ut."ord".$_SESSION['sf_user_id'].".xlsx", "w") or die("Unable to open file!");*/

                $this->objSheet->mergeCells('A5:C5');
                $this->objSheet->getStyle('A5:C5')->getFont()->setBold(true)->setSize(10);
                $this->objSheet->getCell('A5')->setValue('User Detail');

                $i=6;
                $j=0;
                foreach ($userData as $key=>$val) {
                    $j++;
                    $this->objSheet->getCell("A$i")->setValue($j);
                    $this->objSheet->getCell("B$i")->setValue($key);
                    $this->objSheet->getCell("C$i")->setValue($val);
                    $i++;
                }
                $i=$i+2;

                $this->objSheet->mergeCells("A$i:C$i");
                $this->objSheet->getStyle("A$i:C$i")->getFont()->setBold(true)->setSize(10);
                $this->objSheet->getCell("A$i")->setValue('Measurements');

                $i=$i+1;

                foreach ($sizeData as $key=>$val) {
                    $j++;
                    $this->objSheet->getCell("A$i")->setValue($j);
                    $this->objSheet->getCell("B$i")->setValue($key);
                    $this->objSheet->getCell("C$i")->setValue($val);
                    $i++;
                }
                header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
                header('Content-Disposition: attachment;filename=ORD'.date("hmis").'.xls');
                header('Cache-Control: max-age=0');
                $this->objWriter->save("Files/users/ord/".$ut."ord".$_SESSION['sf_user_id'].".xls");
            }else{
                unlink("Files/users/ord/".$ut."ord".$_SESSION['sf_user_id'].".xls");
                $this->objSheet->mergeCells('A1:C1');
                $this->objSheet->getStyle('A1')->getFont()->setSize(12);
                $this->objSheet->getStyle('A1')->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                $this->objSheet->getCell('A1')->setValue('Size Found Data');

                $this->objSheet->getStyle('A3:F3')->getFont()->setBold(true)->setSize(10);
                $this->objSheet->getCell('A3')->setValue('S.No');
                $this->objSheet->getCell('B3')->setValue('Parameters');
                $this->objSheet->getCell('C3')->setValue('Values');
                // autosize the columns
                $this->objSheet->getColumnDimension('A')->setAutoSize(true);
                $this->objSheet->getColumnDimension('B')->setAutoSize(true);
                $this->objSheet->getColumnDimension('C')->setAutoSize(true);

                $this->objSheet->getStyle('A')->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
                $this->objSheet->getStyle('B')->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
                $this->objSheet->getStyle('C')->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
                // we could get this data from database, but here we are writing for simplicity

                /*$myfile = fopen("Files/users/ord/".$ut."ord".$_SESSION['sf_user_id'].".xlsx", "w") or die("Unable to open file!");*/

                $this->objSheet->mergeCells('A5:C5');
                $this->objSheet->getStyle('A5:C5')->getFont()->setBold(true)->setSize(10);
                $this->objSheet->getCell('A5')->setValue('User Detail');

                $i=6;
                $j=0;
                foreach ($userData as $key=>$val) {
                    $j++;
                    $this->objSheet->getCell("A$i")->setValue($j);
                    $this->objSheet->getCell("B$i")->setValue($key);
                    $this->objSheet->getCell("C$i")->setValue($val);
                    $i++;
                }
                $i=$i+2;

                $this->objSheet->mergeCells("A$i:C$i");
                $this->objSheet->getStyle("A$i:C$i")->getFont()->setBold(true)->setSize(10);
                $this->objSheet->getCell("A$i")->setValue('Measurements');

                $i=$i+1;

                foreach ($sizeData as $key=>$val) {
                    $j++;
                    $this->objSheet->getCell("A$i")->setValue($j);
                    $this->objSheet->getCell("B$i")->setValue($key);
                    $this->objSheet->getCell("C$i")->setValue($val);
                    $i++;
                }
                header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
                header('Content-Disposition: attachment;filename=ORD'.date("hmis").'.xls');
                header('Cache-Control: max-age=0');
                $this->objWriter->save("Files/users/ord/".$ut."ord".$_SESSION['sf_user_id'].".xls");
            }
        }
        else{
            $this->link->response[STATUS] = Error;
            $this->link->response[MESSAGE] = $this->link->sqlError();
        }
        return $this->link->response;
    }
    public function getSizeData($min_height,$max_height,$min_weight,$max_weight,$race,$neck,$waist){
        $link = $this->link->connect();
        if($link) {
            $min_neck = $neck-2;
            $max_neck = $neck+2;
            $min_waist = $waist-1;
            $max_waist = $waist+1;
            $query = "select * from size_data where Height>='$min_height' and Height<='$max_height' and 
            Weight_in_kg>='$min_weight' and Weight_in_kg<='$max_weight' and Neck_Width>='$min_neck' and
            Neck_Width<='$max_neck' and Waist >= '$min_waist' and Waist <= '$max_waist'";
            $result = mysqli_query($link, $query);
            if ($result) {
                $num = mysqli_num_rows($result);
                if ($num > 0) {
                    $row = mysqli_fetch_assoc($result);
                    $this->link->response[STATUS] = Success;
                    $this->link->response[MESSAGE] = "Data Found";
                    $userResponse = $this->getParticularUserData($_SESSION['sf_user_id']);
                    $userData = $userResponse['userData'];
                    $sizeData_in = array(
                        "Unit_Type" => "Inches",
                        "Gender" => $userData['gender'],
                        "AgeRange" => ($userData['age'] - 3) . "-" . ($userData['age'] + 3),
                        "_id" =>$row['_id'],
                        "Name" =>$row['Name'],
                        "Size" =>$userData['size'],
                        "Height" =>$row['Height']*0.393701,
                        "Collar" =>$row['Collar']*0.393701,
                        "Bust" =>$row['Bust']*0.393701,
                        "Waist" =>$userData['waist'],//already in inch
                        "Hips" =>$row['Hips']*0.393701,
                        "NecktoHips" =>$row['NecktoHips']*0.393701,
                        "Shoulder2Shoulder" =>$row['Shoulder2Shoulder']*0.393701,
                        "Neck_Width" =>$userData['neck'],//already in inch
                        "Race" =>$userData['race'],
                        "Bust2Bust_Horizontal" =>$row['Bust2Bust_Horizontal'],//already in inch
                        "Waist_Front" =>$row['Waist_Front']*0.393701,
                        "Waist_Back" =>$row['Waist_Back']*0.393701,
                        "Waist_Height_Front" =>$row['Waist_Height_Front']*0.393701,
                        "Waist_Height_Back" =>$row['Waist_Height_Back']*0.393701,
                        "Waist_Height_Left" =>$row['Waist_Height_Left']*0.393701,
                        "Waist_Height_Right" =>$row['Waist_Height_Right']*0.393701,
                        "Hips_Front" =>$row['Hips_Front']*0.393701,
                        "Hips_Back" =>$row['Hips_Back']*0.393701,
                        "Seat" =>$row['Seat']*0.393701,
                        "Outseam_Left" =>$row['Outseam_Left']*0.393701,
                        "Outseam_Right" =>$row['Outseam_Right']*0.393701,
                        "Inseam_Left" =>$row['Inseam_Left']*0.393701,
                        "Inseam_Right" =>$row['Inseam_Right']*0.393701,
                        "Overarm" =>$row['Overarm']*0.393701,
                        "Stomach" =>$row['Stomach']*0.393701,
                        "Shirt_Sleeve_Left" =>$row['Shirt_Sleeve_Left']*0.393701,
                        "Shirt_Sleeve_Right" =>$row['Shirt_Sleeve_Right']*0.393701,
                        "Across_Chest" =>$row['Across_Chest']*0.393701,
                        "Shoulder_Length_Left" =>$row['Shoulder_Length_Left']*0.393701,
                        "Shoulder_Length_Right" =>$row['Shoulder_Length_Right']*0.393701,
                        "Underbust" =>$row['Underbust']*0.393701,
                        "SideNeck2Bust_Left" =>$row['SideNeck2Bust_Left']*0.393701,
                        "SideNeck2Bust_Right" =>$row['SideNeck2Bust_Right']*0.393701,
                        "Neck2Waist_Front" =>$row['Neck2Waist_Front']*0.393701,
                        "Neck2Waist_Back" =>$row['Neck2Waist_Back']*0.393701,
                        "Shoulder_Slope_Left" =>$row['Shoulder_Slope_Left']*0.393701,
                        "Shoulder_Slope_Right" =>$row['Shoulder_Slope_Right']*0.393701,
                        "Hips_Height" =>$row['Hips_Height']*0.393701,
                        "Knee_Height" =>$row['Knee_Height']*0.393701,
                        "CrotchLength" =>$row['CrotchLength']*0.393701,
                        "CrotchLength_Front" =>$row['CrotchLength_Front']*0.393701,
                        "CrotchLength_Back" =>$row['CrotchLength_Back']*0.393701,
                        "ArmLength_Left" =>$row['ArmLength_Left']*0.393701,
                        "ArmLength_Right" =>$row['ArmLength_Right']*0.393701,
                        "SlvIn_L" =>$row['SlvIn_L']*0.393701,
                        "SlvIn_R" =>$row['SlvIn_R']*0.393701,
                        "Biceps_Left" =>$row['Biceps_Left']*0.393701,
                        "Biceps_Right" =>$row['Biceps_Right']*0.393701,
                        "Wrist_Left" =>$row['Wrist_Left']*0.393701,
                        "Wrist_Right" =>$row['Wrist_Right']*0.393701,
                        "Neck_Front_Height" =>$row['Neck_Front_Height']*0.393701,
                        "Neck_Back_Height" =>$row['Neck_Back_Height']*0.393701,
                        "Neck_Left_Height" =>$row['Neck_Left_Height']*0.393701,
                        "Neck_Right_Height" =>$row['Neck_Right_Height']*0.393701,
                        "BustToWaist_Left" =>$row['BustToWaist_Left']*0.393701,
                        "BustToWaist_Right" =>$row['BustToWaist_Right']*0.393701,
                        "Chest" =>$row['Chest']*0.393701,
                        "Chest_Front" =>$row['Chest_Front']*0.393701,
                        "Chest_Back" =>$row['Chest_Back']*0.393701,
                        "Chest_Height" =>$row['Chest_Height']*0.393701,
                        "Shoulder2Shoulder_Horiz" =>$row['Shoulder2Shoulder_Horiz']*0.393701,
                        "Shoulder2Shoulder_thru_CBN" =>$row['Shoulder2Shoulder_thru_CBN']*0.393701,
                        "Coat_Outsleeve_Left" =>$row['Coat_Outsleeve_Left']*0.393701,
                        "Coat_Outsleeve_Right" =>$row['Coat_Outsleeve_Right']*0.393701,
                        "Stomach_Height" =>$row['Stomach_Height']*0.393701,
                        "Abdomen" =>$row['Abdomen']*0.393701,
                        "Abdomen_Height" =>$row['Abdomen_Height']*0.393701,
                        "Shoulder_Height_Left" =>$row['Shoulder_Height_Left']*0.393701,
                        "Shoulder_Height_Right" =>$row['Shoulder_Height_Right']*0.393701,
                        "Armpit_Avg_Height" =>$row['Armpit_Avg_Height']*0.393701,
                        "Overarm_50" =>$row['Overarm_50']*0.393701,
                        "Overarm_70" =>$row['Overarm_70']*0.393701,
                        "Thigh_Length_Left" =>$row['Thigh_Length_Left']*0.393701,
                        "Thigh_Length_Right" =>$row['Thigh_Length_Right']*0.393701,
                        "Knee_Left" =>$row['Knee_Left']*0.393701,
                        "Knee_Right" =>$row['Knee_Right']*0.393701,
                        "Ankle_Girth_Left" =>$row['Ankle_Girth_Left']*0.393701,
                        "Ankle_Girth_Right" =>$row['Ankle_Girth_Right']*0.393701,
                        "Foot_Width_Left" =>$row['Foot_Width_Left']*0.393701,
                        "Foot_Width_Right" =>$row['Foot_Width_Right']*0.393701,
                        "Calf_Left" =>$row['Calf_Left']*0.393701,
                        "Calf_Right" =>$row['Calf_Right']*0.393701,
                        "Thigh_Left" =>$row['Thigh_Left']*0.393701,
                        "Thigh_Right" =>$row['Thigh_Right']*0.393701,
                        "Seat_Height" =>$row['Seat_Height']*0.393701,
                        "Seat_Width" =>$row['Seat_Width']*0.393701,
                        "Seat_Back_Angle" =>$row['Seat_Back_Angle']*0.393701,
                        "Waist_to_Seat_Back" =>$row['Waist_to_Seat_Back']*0.393701,
                        "Waist_to_Hips_Back" =>$row['Waist_to_Hips_Back']*0.393701,
                        "Bulk_Volume" =>$row['Bulk_Volume']*0.393701,
                        "Weight_in_kg" =>$userData['weight'],
                        "high_hips_height" =>$row['high_hips_height']*0.393701,
                        "high_hips_Full" =>$row['high_hips_Full']*0.393701,
                        "Upper_Hip_Girth_Height" =>$row['Upper_Hip_Girth_Height']*0.393701,
                        "Upper_Hips_Girth_Full" =>$row['Upper_Hips_Girth_Full']*0.393701,
                        "Neck_to_High_Hips" =>$row['Neck_to_High_Hips']*0.393701,
                        "Armscye_Circumference_Left" =>$row['Armscye_Circumference_Left']*0.393701,
                        "Armscye_Circumference_Right" =>$row['Armscye_Circumference_Right']*0.393701,
                        "Armscye_Width_Left_Tape" =>$row['Armscye_Width_Left_Tape']*0.393701,
                        "Armscye_Width_Right_Tape" =>$row['Armscye_Width_Right_Tape']*0.393701,
                        "Center_Trunk_Circumference_Full" =>$row['Center_Trunk_Circumference_Full']*0.393701,
                        "Thigh_Height" =>$row['Thigh_Height']*0.393701,
                        "Shoulder_Left_X" =>$row['Shoulder_Left_X']*0.393701,
                        "Shoulder_Right_X" =>$row['Shoulder_Right_X']*0.393701,
                        "Shoulder_Left_Y" =>$row['Shoulder_Left_Y']*0.393701,
                        "Shoulder_Right_Y" =>$row['Shoulder_Right_Y']*0.393701,
                        "Shoulder_2_Shoulder_Caliper_straight_line" =>$row['Shoulder_2_Shoulder_Caliper_straight_line']*0.393701
                    );
                    $sizeData_cm = array(
                        "Unit_Type" => "Centimeters",
                        "Gender" => $userData['gender'],
                        "AgeRange" => ($userData['age'] - 3) . "-" . ($userData['age'] + 3),
                        "_id" =>$row['_id'],
                        "Name" =>$row['Name'],
                        "Size" =>$userData['size'],
                        "Height" =>$row['Height'],
                        "Collar" =>$row['Collar'],
                        "Bust" =>$row['Bust'],
                        "Waist" =>$userData['waist']*2.54,
                        "Hips" =>$row['Hips'],
                        "NecktoHips" =>$row['NecktoHips'],
                        "Shoulder2Shoulder" =>$row['Shoulder2Shoulder'],
                        "Neck_Width" =>$userData['neck']*2.54,
                        "Race" =>$userData['race'],
                        "Bust2Bust_Horizontal" =>$row['Bust2Bust_Horizontal'],
                        "Waist_Front" =>$row['Waist_Front'],
                        "Waist_Back" =>$row['Waist_Back'],
                        "Waist_Height_Front" =>$row['Waist_Height_Front'],
                        "Waist_Height_Back" =>$row['Waist_Height_Back'],
                        "Waist_Height_Left" =>$row['Waist_Height_Left'],
                        "Waist_Height_Right" =>$row['Waist_Height_Right'],
                        "Hips_Front" =>$row['Hips_Front'],
                        "Hips_Back" =>$row['Hips_Back'],
                        "Seat" =>$row['Seat'],
                        "Outseam_Left" =>$row['Outseam_Left'],
                        "Outseam_Right" =>$row['Outseam_Right'],
                        "Inseam_Left" =>$row['Inseam_Left'],
                        "Inseam_Right" =>$row['Inseam_Right'],
                        "Overarm" =>$row['Overarm'],
                        "Stomach" =>$row['Stomach'],
                        "Shirt_Sleeve_Left" =>$row['Shirt_Sleeve_Left'],
                        "Shirt_Sleeve_Right" =>$row['Shirt_Sleeve_Right'],
                        "Across_Chest" =>$row['Across_Chest'],
                        "Shoulder_Length_Left" =>$row['Shoulder_Length_Left'],
                        "Shoulder_Length_Right" =>$row['Shoulder_Length_Right'],
                        "Underbust" =>$row['Underbust'],
                        "SideNeck2Bust_Left" =>$row['SideNeck2Bust_Left'],
                        "SideNeck2Bust_Right" =>$row['SideNeck2Bust_Right'],
                        "Neck2Waist_Front" =>$row['Neck2Waist_Front'],
                        "Neck2Waist_Back" =>$row['Neck2Waist_Back'],
                        "Shoulder_Slope_Left" =>$row['Shoulder_Slope_Left'],
                        "Shoulder_Slope_Right" =>$row['Shoulder_Slope_Right'],
                        "Hips_Height" =>$row['Hips_Height'],
                        "Knee_Height" =>$row['Knee_Height'],
                        "CrotchLength" =>$row['CrotchLength'],
                        "CrotchLength_Front" =>$row['CrotchLength_Front'],
                        "CrotchLength_Back" =>$row['CrotchLength_Back'],
                        "ArmLength_Left" =>$row['ArmLength_Left'],
                        "ArmLength_Right" =>$row['ArmLength_Right'],
                        "SlvIn_L" =>$row['SlvIn_L'],
                        "SlvIn_R" =>$row['SlvIn_R'],
                        "Biceps_Left" =>$row['Biceps_Left'],
                        "Biceps_Right" =>$row['Biceps_Right'],
                        "Wrist_Left" =>$row['Wrist_Left'],
                        "Wrist_Right" =>$row['Wrist_Right'],
                        "Neck_Front_Height" =>$row['Neck_Front_Height'],
                        "Neck_Back_Height" =>$row['Neck_Back_Height'],
                        "Neck_Left_Height" =>$row['Neck_Left_Height'],
                        "Neck_Right_Height" =>$row['Neck_Right_Height'],
                        "BustToWaist_Left" =>$row['BustToWaist_Left'],
                        "BustToWaist_Right" =>$row['BustToWaist_Right'],
                        "Chest" =>$row['Chest'],
                        "Chest_Front" =>$row['Chest_Front'],
                        "Chest_Back" =>$row['Chest_Back'],
                        "Chest_Height" =>$row['Chest_Height'],
                        "Shoulder2Shoulder_Horiz" =>$row['Shoulder2Shoulder_Horiz'],
                        "Shoulder2Shoulder_thru_CBN" =>$row['Shoulder2Shoulder_thru_CBN'],
                        "Coat_Outsleeve_Left" =>$row['Coat_Outsleeve_Left'],
                        "Coat_Outsleeve_Right" =>$row['Coat_Outsleeve_Right'],
                        "Stomach_Height" =>$row['Stomach_Height'],
                        "Abdomen" =>$row['Abdomen'],
                        "Abdomen_Height" =>$row['Abdomen_Height'],
                        "Shoulder_Height_Left" =>$row['Shoulder_Height_Left'],
                        "Shoulder_Height_Right" =>$row['Shoulder_Height_Right'],
                        "Armpit_Avg_Height" =>$row['Armpit_Avg_Height'],
                        "Overarm_50" =>$row['Overarm_50'],
                        "Overarm_70" =>$row['Overarm_70'],
                        "Thigh_Length_Left" =>$row['Thigh_Length_Left'],
                        "Thigh_Length_Right" =>$row['Thigh_Length_Right'],
                        "Knee_Left" =>$row['Knee_Left'],
                        "Knee_Right" =>$row['Knee_Right'],
                        "Ankle_Girth_Left" =>$row['Ankle_Girth_Left'],
                        "Ankle_Girth_Right" =>$row['Ankle_Girth_Right'],
                        "Foot_Width_Left" =>$row['Foot_Width_Left'],
                        "Foot_Width_Right" =>$row['Foot_Width_Right'],
                        "Calf_Left" =>$row['Calf_Left'],
                        "Calf_Right" =>$row['Calf_Right'],
                        "Thigh_Left" =>$row['Thigh_Left'],
                        "Thigh_Right" =>$row['Thigh_Right'],
                        "Seat_Height" =>$row['Seat_Height'],
                        "Seat_Width" =>$row['Seat_Width'],
                        "Seat_Back_Angle" =>$row['Seat_Back_Angle'],
                        "Waist_to_Seat_Back" =>$row['Waist_to_Seat_Back'],
                        "Waist_to_Hips_Back" =>$row['Waist_to_Hips_Back'],
                        "Bulk_Volume" =>$row['Bulk_Volume'],
                        "Weight_in_kg" =>$userData['weight'],
                        "high_hips_height" =>$row['high_hips_height'],
                        "high_hips_Full" =>$row['high_hips_Full'],
                        "Upper_Hip_Girth_Height" =>$row['Upper_Hip_Girth_Height'],
                        "Upper_Hips_Girth_Full" =>$row['Upper_Hips_Girth_Full'],
                        "Neck_to_High_Hips" =>$row['Neck_to_High_Hips'],
                        "Armscye_Circumference_Left" =>$row['Armscye_Circumference_Left'],
                        "Armscye_Circumference_Right" =>$row['Armscye_Circumference_Right'],
                        "Armscye_Width_Left_Tape" =>$row['Armscye_Width_Left_Tape'],
                        "Armscye_Width_Right_Tape" =>$row['Armscye_Width_Right_Tape'],
                        "Center_Trunk_Circumference_Full" =>$row['Center_Trunk_Circumference_Full'],
                        "Thigh_Height" =>$row['Thigh_Height'],
                        "Shoulder_Left_X" =>$row['Shoulder_Left_X'],
                        "Shoulder_Right_X" =>$row['Shoulder_Right_X'],
                        "Shoulder_Left_Y" =>$row['Shoulder_Left_Y'],
                        "Shoulder_Right_Y" =>$row['Shoulder_Right_Y'],
                        "Shoulder_2_Shoulder_Caliper_straight_line" =>$row['Shoulder_2_Shoulder_Caliper_straight_line']
                    );
                    $this->link->response['sizeData_in'] = $sizeData_in;
                    $this->link->response['sizeData_cm'] = $sizeData_cm;
                }
                else
                {
                    $query = "select * from size_data where Height>='$min_height' and Height<='$max_height' and 
                    Weight_in_kg>='$min_weight' and Weight_in_kg<='$max_weight' and Neck_Width>='$min_neck' and 
                    Neck_Width<='$max_neck'";
                    $result = mysqli_query($link, $query);
                    if ($result) {
                        $num = mysqli_num_rows($result);
                        if ($num > 0) {
                            $row = mysqli_fetch_assoc($result);
                            $this->link->response[STATUS] = Success;
                            $this->link->response[MESSAGE] = "Data Found";
                            $userResponse = $this->getParticularUserData($_SESSION['sf_user_id']);
                            $userData = $userResponse['userData'];
                            $sizeData_in = array(
                                "Unit_Type" => "Inches",
                                "Gender" => $userData['gender'],
                                "AgeRange" => ($userData['age'] - 3) . "-" . ($userData['age'] + 3),
                                "_id" =>$row['_id'],
                                "Name" =>$row['Name'],
                                "Size" =>$userData['size'],
                                "Height" =>$row['Height']*0.393701,
                                "Collar" =>$row['Collar']*0.393701,
                                "Bust" =>$row['Bust']*0.393701,
                                "Waist" =>$userData['waist'],//already in inch
                                "Hips" =>$row['Hips']*0.393701,
                                "NecktoHips" =>$row['NecktoHips']*0.393701,
                                "Shoulder2Shoulder" =>$row['Shoulder2Shoulder']*0.393701,
                                "Neck_Width" =>$userData['neck'],//already in inch
                                "Race" =>$userData['race'],
                                "Bust2Bust_Horizontal" =>$row['Bust2Bust_Horizontal'],//already in inch
                                "Waist_Front" =>$row['Waist_Front']*0.393701,
                                "Waist_Back" =>$row['Waist_Back']*0.393701,
                                "Waist_Height_Front" =>$row['Waist_Height_Front']*0.393701,
                                "Waist_Height_Back" =>$row['Waist_Height_Back']*0.393701,
                                "Waist_Height_Left" =>$row['Waist_Height_Left']*0.393701,
                                "Waist_Height_Right" =>$row['Waist_Height_Right']*0.393701,
                                "Hips_Front" =>$row['Hips_Front']*0.393701,
                                "Hips_Back" =>$row['Hips_Back']*0.393701,
                                "Seat" =>$row['Seat']*0.393701,
                                "Outseam_Left" =>$row['Outseam_Left']*0.393701,
                                "Outseam_Right" =>$row['Outseam_Right']*0.393701,
                                "Inseam_Left" =>$row['Inseam_Left']*0.393701,
                                "Inseam_Right" =>$row['Inseam_Right']*0.393701,
                                "Overarm" =>$row['Overarm']*0.393701,
                                "Stomach" =>$row['Stomach']*0.393701,
                                "Shirt_Sleeve_Left" =>$row['Shirt_Sleeve_Left']*0.393701,
                                "Shirt_Sleeve_Right" =>$row['Shirt_Sleeve_Right']*0.393701,
                                "Across_Chest" =>$row['Across_Chest']*0.393701,
                                "Shoulder_Length_Left" =>$row['Shoulder_Length_Left']*0.393701,
                                "Shoulder_Length_Right" =>$row['Shoulder_Length_Right']*0.393701,
                                "Underbust" =>$row['Underbust']*0.393701,
                                "SideNeck2Bust_Left" =>$row['SideNeck2Bust_Left']*0.393701,
                                "SideNeck2Bust_Right" =>$row['SideNeck2Bust_Right']*0.393701,
                                "Neck2Waist_Front" =>$row['Neck2Waist_Front']*0.393701,
                                "Neck2Waist_Back" =>$row['Neck2Waist_Back']*0.393701,
                                "Shoulder_Slope_Left" =>$row['Shoulder_Slope_Left']*0.393701,
                                "Shoulder_Slope_Right" =>$row['Shoulder_Slope_Right']*0.393701,
                                "Hips_Height" =>$row['Hips_Height']*0.393701,
                                "Knee_Height" =>$row['Knee_Height']*0.393701,
                                "CrotchLength" =>$row['CrotchLength']*0.393701,
                                "CrotchLength_Front" =>$row['CrotchLength_Front']*0.393701,
                                "CrotchLength_Back" =>$row['CrotchLength_Back']*0.393701,
                                "ArmLength_Left" =>$row['ArmLength_Left']*0.393701,
                                "ArmLength_Right" =>$row['ArmLength_Right']*0.393701,
                                "SlvIn_L" =>$row['SlvIn_L']*0.393701,
                                "SlvIn_R" =>$row['SlvIn_R']*0.393701,
                                "Biceps_Left" =>$row['Biceps_Left']*0.393701,
                                "Biceps_Right" =>$row['Biceps_Right']*0.393701,
                                "Wrist_Left" =>$row['Wrist_Left']*0.393701,
                                "Wrist_Right" =>$row['Wrist_Right']*0.393701,
                                "Neck_Front_Height" =>$row['Neck_Front_Height']*0.393701,
                                "Neck_Back_Height" =>$row['Neck_Back_Height']*0.393701,
                                "Neck_Left_Height" =>$row['Neck_Left_Height']*0.393701,
                                "Neck_Right_Height" =>$row['Neck_Right_Height']*0.393701,
                                "BustToWaist_Left" =>$row['BustToWaist_Left']*0.393701,
                                "BustToWaist_Right" =>$row['BustToWaist_Right']*0.393701,
                                "Chest" =>$row['Chest']*0.393701,
                                "Chest_Front" =>$row['Chest_Front']*0.393701,
                                "Chest_Back" =>$row['Chest_Back']*0.393701,
                                "Chest_Height" =>$row['Chest_Height']*0.393701,
                                "Shoulder2Shoulder_Horiz" =>$row['Shoulder2Shoulder_Horiz']*0.393701,
                                "Shoulder2Shoulder_thru_CBN" =>$row['Shoulder2Shoulder_thru_CBN']*0.393701,
                                "Coat_Outsleeve_Left" =>$row['Coat_Outsleeve_Left']*0.393701,
                                "Coat_Outsleeve_Right" =>$row['Coat_Outsleeve_Right']*0.393701,
                                "Stomach_Height" =>$row['Stomach_Height']*0.393701,
                                "Abdomen" =>$row['Abdomen']*0.393701,
                                "Abdomen_Height" =>$row['Abdomen_Height']*0.393701,
                                "Shoulder_Height_Left" =>$row['Shoulder_Height_Left']*0.393701,
                                "Shoulder_Height_Right" =>$row['Shoulder_Height_Right']*0.393701,
                                "Armpit_Avg_Height" =>$row['Armpit_Avg_Height']*0.393701,
                                "Overarm_50" =>$row['Overarm_50']*0.393701,
                                "Overarm_70" =>$row['Overarm_70']*0.393701,
                                "Thigh_Length_Left" =>$row['Thigh_Length_Left']*0.393701,
                                "Thigh_Length_Right" =>$row['Thigh_Length_Right']*0.393701,
                                "Knee_Left" =>$row['Knee_Left']*0.393701,
                                "Knee_Right" =>$row['Knee_Right']*0.393701,
                                "Ankle_Girth_Left" =>$row['Ankle_Girth_Left']*0.393701,
                                "Ankle_Girth_Right" =>$row['Ankle_Girth_Right']*0.393701,
                                "Foot_Width_Left" =>$row['Foot_Width_Left']*0.393701,
                                "Foot_Width_Right" =>$row['Foot_Width_Right']*0.393701,
                                "Calf_Left" =>$row['Calf_Left']*0.393701,
                                "Calf_Right" =>$row['Calf_Right']*0.393701,
                                "Thigh_Left" =>$row['Thigh_Left']*0.393701,
                                "Thigh_Right" =>$row['Thigh_Right']*0.393701,
                                "Seat_Height" =>$row['Seat_Height']*0.393701,
                                "Seat_Width" =>$row['Seat_Width']*0.393701,
                                "Seat_Back_Angle" =>$row['Seat_Back_Angle']*0.393701,
                                "Waist_to_Seat_Back" =>$row['Waist_to_Seat_Back']*0.393701,
                                "Waist_to_Hips_Back" =>$row['Waist_to_Hips_Back']*0.393701,
                                "Bulk_Volume" =>$row['Bulk_Volume']*0.393701,
                                "Weight_in_kg" =>$userData['weight'],
                                "high_hips_height" =>$row['high_hips_height']*0.393701,
                                "high_hips_Full" =>$row['high_hips_Full']*0.393701,
                                "Upper_Hip_Girth_Height" =>$row['Upper_Hip_Girth_Height']*0.393701,
                                "Upper_Hips_Girth_Full" =>$row['Upper_Hips_Girth_Full']*0.393701,
                                "Neck_to_High_Hips" =>$row['Neck_to_High_Hips']*0.393701,
                                "Armscye_Circumference_Left" =>$row['Armscye_Circumference_Left']*0.393701,
                                "Armscye_Circumference_Right" =>$row['Armscye_Circumference_Right']*0.393701,
                                "Armscye_Width_Left_Tape" =>$row['Armscye_Width_Left_Tape']*0.393701,
                                "Armscye_Width_Right_Tape" =>$row['Armscye_Width_Right_Tape']*0.393701,
                                "Center_Trunk_Circumference_Full" =>$row['Center_Trunk_Circumference_Full']*0.393701,
                                "Thigh_Height" =>$row['Thigh_Height']*0.393701,
                                "Shoulder_Left_X" =>$row['Shoulder_Left_X']*0.393701,
                                "Shoulder_Right_X" =>$row['Shoulder_Right_X']*0.393701,
                                "Shoulder_Left_Y" =>$row['Shoulder_Left_Y']*0.393701,
                                "Shoulder_Right_Y" =>$row['Shoulder_Right_Y']*0.393701,
                                "Shoulder_2_Shoulder_Caliper_straight_line" =>$row['Shoulder_2_Shoulder_Caliper_straight_line']*0.393701
                            );
                            $sizeData_cm = array(
                                "Unit_Type" => "Centimeters",
                                "Gender" => $userData['gender'],
                                "AgeRange" => ($userData['age'] - 3) . "-" . ($userData['age'] + 3),
                                "_id" =>$row['_id'],
                                "Name" =>$row['Name'],
                                "Size" =>$userData['size'],
                                "Height" =>$row['Height'],
                                "Collar" =>$row['Collar'],
                                "Bust" =>$row['Bust'],
                                "Waist" =>$userData['waist']*2.54,
                                "Hips" =>$row['Hips'],
                                "NecktoHips" =>$row['NecktoHips'],
                                "Shoulder2Shoulder" =>$row['Shoulder2Shoulder'],
                                "Neck_Width" =>$userData['neck']*2.54,
                                "Race" =>$userData['race'],
                                "Bust2Bust_Horizontal" =>$row['Bust2Bust_Horizontal'],
                                "Waist_Front" =>$row['Waist_Front'],
                                "Waist_Back" =>$row['Waist_Back'],
                                "Waist_Height_Front" =>$row['Waist_Height_Front'],
                                "Waist_Height_Back" =>$row['Waist_Height_Back'],
                                "Waist_Height_Left" =>$row['Waist_Height_Left'],
                                "Waist_Height_Right" =>$row['Waist_Height_Right'],
                                "Hips_Front" =>$row['Hips_Front'],
                                "Hips_Back" =>$row['Hips_Back'],
                                "Seat" =>$row['Seat'],
                                "Outseam_Left" =>$row['Outseam_Left'],
                                "Outseam_Right" =>$row['Outseam_Right'],
                                "Inseam_Left" =>$row['Inseam_Left'],
                                "Inseam_Right" =>$row['Inseam_Right'],
                                "Overarm" =>$row['Overarm'],
                                "Stomach" =>$row['Stomach'],
                                "Shirt_Sleeve_Left" =>$row['Shirt_Sleeve_Left'],
                                "Shirt_Sleeve_Right" =>$row['Shirt_Sleeve_Right'],
                                "Across_Chest" =>$row['Across_Chest'],
                                "Shoulder_Length_Left" =>$row['Shoulder_Length_Left'],
                                "Shoulder_Length_Right" =>$row['Shoulder_Length_Right'],
                                "Underbust" =>$row['Underbust'],
                                "SideNeck2Bust_Left" =>$row['SideNeck2Bust_Left'],
                                "SideNeck2Bust_Right" =>$row['SideNeck2Bust_Right'],
                                "Neck2Waist_Front" =>$row['Neck2Waist_Front'],
                                "Neck2Waist_Back" =>$row['Neck2Waist_Back'],
                                "Shoulder_Slope_Left" =>$row['Shoulder_Slope_Left'],
                                "Shoulder_Slope_Right" =>$row['Shoulder_Slope_Right'],
                                "Hips_Height" =>$row['Hips_Height'],
                                "Knee_Height" =>$row['Knee_Height'],
                                "CrotchLength" =>$row['CrotchLength'],
                                "CrotchLength_Front" =>$row['CrotchLength_Front'],
                                "CrotchLength_Back" =>$row['CrotchLength_Back'],
                                "ArmLength_Left" =>$row['ArmLength_Left'],
                                "ArmLength_Right" =>$row['ArmLength_Right'],
                                "SlvIn_L" =>$row['SlvIn_L'],
                                "SlvIn_R" =>$row['SlvIn_R'],
                                "Biceps_Left" =>$row['Biceps_Left'],
                                "Biceps_Right" =>$row['Biceps_Right'],
                                "Wrist_Left" =>$row['Wrist_Left'],
                                "Wrist_Right" =>$row['Wrist_Right'],
                                "Neck_Front_Height" =>$row['Neck_Front_Height'],
                                "Neck_Back_Height" =>$row['Neck_Back_Height'],
                                "Neck_Left_Height" =>$row['Neck_Left_Height'],
                                "Neck_Right_Height" =>$row['Neck_Right_Height'],
                                "BustToWaist_Left" =>$row['BustToWaist_Left'],
                                "BustToWaist_Right" =>$row['BustToWaist_Right'],
                                "Chest" =>$row['Chest'],
                                "Chest_Front" =>$row['Chest_Front'],
                                "Chest_Back" =>$row['Chest_Back'],
                                "Chest_Height" =>$row['Chest_Height'],
                                "Shoulder2Shoulder_Horiz" =>$row['Shoulder2Shoulder_Horiz'],
                                "Shoulder2Shoulder_thru_CBN" =>$row['Shoulder2Shoulder_thru_CBN'],
                                "Coat_Outsleeve_Left" =>$row['Coat_Outsleeve_Left'],
                                "Coat_Outsleeve_Right" =>$row['Coat_Outsleeve_Right'],
                                "Stomach_Height" =>$row['Stomach_Height'],
                                "Abdomen" =>$row['Abdomen'],
                                "Abdomen_Height" =>$row['Abdomen_Height'],
                                "Shoulder_Height_Left" =>$row['Shoulder_Height_Left'],
                                "Shoulder_Height_Right" =>$row['Shoulder_Height_Right'],
                                "Armpit_Avg_Height" =>$row['Armpit_Avg_Height'],
                                "Overarm_50" =>$row['Overarm_50'],
                                "Overarm_70" =>$row['Overarm_70'],
                                "Thigh_Length_Left" =>$row['Thigh_Length_Left'],
                                "Thigh_Length_Right" =>$row['Thigh_Length_Right'],
                                "Knee_Left" =>$row['Knee_Left'],
                                "Knee_Right" =>$row['Knee_Right'],
                                "Ankle_Girth_Left" =>$row['Ankle_Girth_Left'],
                                "Ankle_Girth_Right" =>$row['Ankle_Girth_Right'],
                                "Foot_Width_Left" =>$row['Foot_Width_Left'],
                                "Foot_Width_Right" =>$row['Foot_Width_Right'],
                                "Calf_Left" =>$row['Calf_Left'],
                                "Calf_Right" =>$row['Calf_Right'],
                                "Thigh_Left" =>$row['Thigh_Left'],
                                "Thigh_Right" =>$row['Thigh_Right'],
                                "Seat_Height" =>$row['Seat_Height'],
                                "Seat_Width" =>$row['Seat_Width'],
                                "Seat_Back_Angle" =>$row['Seat_Back_Angle'],
                                "Waist_to_Seat_Back" =>$row['Waist_to_Seat_Back'],
                                "Waist_to_Hips_Back" =>$row['Waist_to_Hips_Back'],
                                "Bulk_Volume" =>$row['Bulk_Volume'],
                                "Weight_in_kg" =>$userData['weight'],
                                "high_hips_height" =>$row['high_hips_height'],
                                "high_hips_Full" =>$row['high_hips_Full'],
                                "Upper_Hip_Girth_Height" =>$row['Upper_Hip_Girth_Height'],
                                "Upper_Hips_Girth_Full" =>$row['Upper_Hips_Girth_Full'],
                                "Neck_to_High_Hips" =>$row['Neck_to_High_Hips'],
                                "Armscye_Circumference_Left" =>$row['Armscye_Circumference_Left'],
                                "Armscye_Circumference_Right" =>$row['Armscye_Circumference_Right'],
                                "Armscye_Width_Left_Tape" =>$row['Armscye_Width_Left_Tape'],
                                "Armscye_Width_Right_Tape" =>$row['Armscye_Width_Right_Tape'],
                                "Center_Trunk_Circumference_Full" =>$row['Center_Trunk_Circumference_Full'],
                                "Thigh_Height" =>$row['Thigh_Height'],
                                "Shoulder_Left_X" =>$row['Shoulder_Left_X'],
                                "Shoulder_Right_X" =>$row['Shoulder_Right_X'],
                                "Shoulder_Left_Y" =>$row['Shoulder_Left_Y'],
                                "Shoulder_Right_Y" =>$row['Shoulder_Right_Y'],
                                "Shoulder_2_Shoulder_Caliper_straight_line" =>$row['Shoulder_2_Shoulder_Caliper_straight_line']
                            );
                            $this->link->response['sizeData_in'] = $sizeData_in;
                            $this->link->response['sizeData_cm'] = $sizeData_cm;
                        } else {
                            $this->link->response[STATUS] = Error;
                            $this->link->response[MESSAGE] = "No Size Found";
                        }
                    }
                    else {
                        $this->link->response[STATUS] = Error;
                        $this->link->response[MESSAGE] = $this->link->sqlError();
                    }
                }
            }
            else {
                $this->link->response[STATUS] = Error;
                $this->link->response[MESSAGE] = $this->link->sqlError();
            }
        }
        else{
            $this->link->response[STATUS] = Error;
            $this->link->response[MESSAGE] = "Connection Error";
        }
        return $this->link->response;
    }
    public function resetData($user_id){
        $link = $this->link->connect();
        if($link) {
            $query = "update users set race = '',neck='',brand='',size='',waist='' where user_id = '$user_id'";
            $result = mysqli_query($link,$query);
            if($result){
                $this->link->response[STATUS] = Success;
                $this->link->response[MESSAGE] = "Data Reset Success";
            }else{
                $this->link->response[STATUS] = Error;
                $this->link->response[MESSAGE] = $this->link->sqlError();
            }
        }
        else{
            $this->link->response[STATUS] = Error;
            $this->link->response[MESSAGE] = "Connection Error";
        }
        return $this->link->response;
    }
    public function storeDatatoTable($userData)
    {
        $link = $this->link->connect();
        if($link) {
            $email = $userData['email'];
            $query = "select * from users where email = '$email'";
            $result = mysqli_query($link, $query);
            if ($result) {
                $num = mysqli_num_rows($result);
                $user_id = $userData['user_id'];
                $name = explode(" ",$userData['name']);
                $fname = $name[0];
                $lname = $name[1];
                $password = $userData['password'];
                $contact = $userData['mobile'];
                $gender = $userData['gender'];
                $dob = $userData['dob'];
                $height_in = explode(" ",$userData['height'])[0];
                $weight = explode(" ",$userData['weight'])[0];
                $user_profile = $userData['profile_pic'];
                $status = '1';
                if ($num > 0) {
                    //update my table data
                    $query = "update users set user_id='$user_id',fname='$fname',lname='$lname',
                    password='$password',contact='$contact',gender='$gender',dob='$dob',height_in='$height_in',
                    weight='$weight',user_profile='$user_profile',status='$status' where email = '$email'";
                } else {
                    //insert new row in my table
                    $query = "insert into users (user_id,fname,lname,password,contact,gender,dob,height_in,weight,
                    user_profile,status,email) values('$user_id','$fname','$lname','$password','$contact','$gender',
                    '$dob','$height_in','$weight','$user_profile','$status','$email')";
                }
                $result = mysqli_query($this->link->Connect(),$query);
                if($result){
                    $this->link->response[STATUS] = Success;
                    $this->link->response[MESSAGE] = "Login Success";
                }
                else{
                    $this->link->response[STATUS] = Error;
                    $this->link->response[MESSAGE] = $this->link->sqlError();
                }
            } else {
                $this->link->response[STATUS] = Error;
                $this->link->response[MESSAGE] = $this->link->sqlError();
            }
        }
        else{
            $this->link->response[STATUS] = Error;
            $this->link->response[MESSAGE] = "Connection Error";
        }
        return $this->link->response;
    }
    public function updateDimensions($user_id, $height, $weight, $age, $gender, $race, $neck, $brand,$size,$waist)
    {
        $link = $this->link->connect();
        if($link) {
            $query = "update users set height_in='$height',weight='$weight',age='$age',
            gender='$gender',race='$race',neck='$neck',brand='$brand',waist = '$waist',size = '$size'
            where user_id = '$user_id'";
            $result = mysqli_query($this->link->Connect(),$query);
            if($result){
                $this->link->response[STATUS] = Success;
                $this->link->response[MESSAGE] = "Data Updated Successfully";
            }
            else{
                $this->link->response[STATUS] = Error;
                $this->link->response[MESSAGE] = $this->link->sqlError();
            }
        }
        else{
            $this->link->response[STATUS] = Error;
            $this->link->response[MESSAGE] = "Connection Error";
        }
        return $this->link->response;
    }
    public function isSubscribed($user_id) {
        $link = $this->link->connect();
        $sql = "select * from wp_customers where subscription='subscribed' and user_id='$user_id'";
//        echo $sql;
        $result = mysqli_query($link,$sql);
        if($result) {
          $rows = mysqli_num_rows($result);
          if($rows>0) {
              return true;
          }
        }
        return false;
    }
    public function apiResponse($response){
        header("Content-Type:application/json");
        echo json_encode($response);
    }
}