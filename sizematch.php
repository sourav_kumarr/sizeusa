<?php
/**
 * Created by PhpStorm.
 * User: Sourav
 * Date: 8/14/2017
 * Time: 12:18 PM
 */
 include('header.php');
?>
<style>
    .size-matching {
        padding: 0;
        display: none;
    }
    .size-matching.active{
        display: block;
    }
</style>

<div class="container">
    <div class="col-md-12 size-matching size-matching-1 active">
        <div class="col-md-12" style="text-align: center">
            <label>Size Matching Requirment</label>
            <hr/>
        </div>
        <div class="col-md-12" style="padding-right: 15%;padding-left: 15%">
            <div class="col-md-12">
                <div class="col-md-4">
                    <div class="col-md-12">
                        <label>Height</label>
                        <input type="text" id="heightn" class="form-control" placeholder="please enter height"/>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="col-md-12">
                        <label>Weight</label>
                        <input type="text" id="weight" class="form-control" placeholder="please enter weight"/>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="col-md-12">
                        <label>Waist</label>
                        <input type="text" id="waist" class="form-control" placeholder="please enter waist"/>
                    </div>
                </div>

                </div>

            <div class="col-md-12" style="margin-top: 1%">
              <div class="col-md-4">
                <div class="col-md-12">
                    <label>Select Race</label>
                    <select id="race" class="form-control">
                        <option value="Non-Hispanic White">Non-Hispanic White</option>
                        <option value="Non-Hispanic Black">Non-Hispanic Black</option>
                        <option value="Non-Mexican Hispanic">Non-Mexican Hispanic</option>
                        <option value="Mexican Hispanic">Mexican Hispanic</option>
                        <option value="Asian">Asian</option>
                        <option value="Other">Other</option>
                    </select>
                </div>
              </div>
                <div class="col-md-4">
                    <div class="col-md-12">
                        <label>Gender</label>
                        <select id="gender" class="form-control">
                            <option value="Male">Male</option>
                            <option value="Female">Female</option>
                        </select>
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="col-md-12">
                        <label>Select Age</label>
                        <select id="age" class="form-control">
                            <option value="18 - 25">18 - 25</option>
                            <option value="26 - 35">26 - 35</option>
                            <option value="36 - 45">36 - 45</option>
                            <option value="46 - 55">46 - 55</option>
                            <option value="56 - 65">56 - 65</option>
                            <option value="66 +">66 +</option>
                        </select>
                    </div>
                 </div>
            </div>

            <div class="col-md-12" style="margin-top: 1%">
                <div class="col-md-8" style="padding-top: 2%;margin-left: 14px;">
                    <ul class="list-inline list-unstyled">
                        <li><button class="btn btn-primary btn-md" onclick="onReportSubmit()">Submit</button></li>
                        <li><label style="color: red" id="message"></label></li>
                    </ul>
                </div>

            </div>


    </div>
  </div>
    <div class="col-md-12 size-matching size-matching-2">
        <div class="col-md-12">
            <ul class="list-inline list-unstyled pull-left" style="margin-top: 2%;width: 100%">
                <li><h4 style="cursor:pointer" onclick="onLevelLoaded(1);"><i class="fa fa-arrow-circle-left"></i> Back</h4> </li>
                <li style="width: 80%;text-align: center"><h4 style="margin-top: 0px;">We Found Your Measurements According to your Given Info</h4></li>
            </ul>

        </div>
        <div class="col-md-12"><hr/></div>
        <div class="col-md-12">
            <table class="table table-bordered table-hover" id="results-dataa">
            </table>
        </div>
    </div>
</div>
<?php
include('footer.php');
?>
<script type="text/javascript" src="js/matching.js"> </script>
