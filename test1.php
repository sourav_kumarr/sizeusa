<?php
/**
 * Created by PhpStorm.
 * User: Sourav
 * Date: 8/18/2017
 * Time: 12:18 PM
 */
?>
<link rel="stylesheet" href="styles1.css"/>
<div id="container" class="chart-container"></div>
<script src="http://code.jquery.com/jquery-1.9.1.min.js"></script>
<script src="http://code.highcharts.com/js/highcharts.js"></script>
<script>
    $(function () {

        var chart = new Highcharts.Chart({
            chart: {
                renderTo: "container",
                type: "column"
            },
            title: {
                useHTML: true,
                x: -10,
                y: 8,
                text: '<span class="chart-title"> Grouped categories <span class="chart-href"> <a href="http://www.blacklabel.pl/highcharts" target="_blank"> Black Label </a> </span> <span class="chart-subtitle">plugin by </span></span>'
            },
            series: [{
                data: [4, 14,14,5]
            }],
            xAxis: {
                labels: {
                    groupedOptions: [{
                        style: {
                            color: 'red' // set red font for labels in 1st-Level
                        }
                    }, {
                        rotation: -45, // rotate labels for a 2nd-level
                        align: 'right'
                    }],
                    rotation: 0 // 0-level options aren't changed, use them as always
                },
                categories: [{
                    categories: [{
                        name: "Male",
                        categories: ["4", "14"]
                    }]
                }, {
                    categories: [{
                        name: "Female",
                        categories: ["14", "5"]
                    }]
                }]
            }
        });
    });
</script>