/**
 * Created by Sourav on 7/13/2017.
 */

var primary = ['id_','Survey_ID','ZipCode','Gender','AgeRange','Weight','ScanTop','ScanBottom','Location','EthnicCategory',
    'EthnicGroup','Income','MaritalStatus','WeightDescription','Activity','School','Employment','ClothingSize','Stores','ClothingTypes'];

var secondry = ['id_','Survey_ID','Gender','AgeRange'];
var image_twin = ['id_','Survey_ID','Gender'];
var items = [];
var total = 0;
var isClicked = true;
window.onload = function(){
    loadData(1);

};

function loadData(num) {
    var userId = $('#user_id').val();
    var table_name = $('#table_name').val();
    var data = $('#data').val();
    var selection = $('#selection').val();

    if(table_name == 'sizeusa_twins') {
        $("#length_type").val("cm");
    }
    var length_type = $("#length_type").val();
    $('#overlay').css('display','block');
    if(userId!='') {
        var url = 'api/reportProcess.php';
        $.post(url,{'type':'filterReportData','userId':userId,'tableName':table_name,'data':data,'selection':selection,'page':num},function(data){
            $('#overlay').css('display','none');
            var status = data.Status;
            items = data.data;
            total = data.total;
            console.log(status);
            if(status == 'Success') {
                var th_ ='<thead><tr>';
                var td_='';
                for(var i=0;i<items.length;i++) {
                    var obj = items[i];

                    if(obj) {
                        td_ = td_ + '<tr>';
                        for (var j in obj) {
                            if (i == 0) {
                                th_ = th_ + '<th>' + j + '</th>';
                            }
                            // console.log('col val -- '+obj[j]+'index -- '+i);
                            var col_value = obj[j];

                            if(table_name == 'sizeusa_primary') { // first table data
                                if(!findItem(primary,j)){
                                    if(length_type == 'cm') {
                                        col_value = parseFloat(col_value).toFixed(2)+' cm';
                                        // col_value.toFixed(2);

                                    }
                                    else{
                                        col_value = parseFloat(col_value).toFixed(2)+' inch';
                                    }

                                }

                            }

                            else if(table_name == 'sizeusa_secondry') { // first table data
                                if(!findItem(secondry,j)){
                                    if(length_type == 'cm') {
                                        // col_value = parseFloat(col_value)*2.54; // inch to cm formula
                                        col_value = parseFloat(col_value).toFixed(2)+' cm';
                                    }
                                    else{
                                        col_value = parseFloat(col_value).toFixed(2)+' inch';
                                    }
                                }

                            }

                            else { // first table data
                                if(!findItem(image_twin,j)) {
                                    if(length_type == 'inch') {
                                        col_value = parseFloat(col_value).toFixed(2)+' inch';
                                    }
                                    else{
                                        col_value = parseFloat(col_value).toFixed(2)+' cm';
                                    }

                                }

                            }

                            td_ = td_ + '<td>' + col_value + '</td>';
                        }

                        td_ = td_ + '</tr>';
                    }

                }


                th_ = th_+'</tr></thead>';
                td_ = td_+'';
                $('#table_data').html(th_+td_);
                // $('#table_data').dataTable({"order": [],"destroy":true});

                $("#total").html('Total : '+(total));
                if(isClicked) {
                    $("#pagination").bootpag({
                        total: Math.ceil(total / 10),
                        page: 1,
                        maxVisible: 10
                    }).on("page", function (e, num) {
                        e.preventDefault();
                        // $("#results").prepend('<div class="loading-indication"><img src="ajax-loader.gif" /> Loading...</div>');
                        loadData(num);
                    });
                    isClicked = false;
                }
                // $('.dataTables_filter').css('display','none');

            }
            else if(status == 'Failure') {
                $('#table_data').html('<label style="color:red">Sorry data not found</label>');
            }
        });
    }
    else{
        $('#table_data').html('<label style="color:red">Sorry parameter not found</label>');
    }

}

function onPrintReport() {
    // var print_data = $('#print_data').val();
    window.location = 'export_csv.php';
}

function filterReport() {
    var table_name = $('#table_name').val();
    var length_type = $("#length_type").val();
    console.log('len type -- '+length_type);
    $("#overlay").css("display","block");
    var th_ ='<thead><tr>';
    var td_='<tbody>';
    for(var i=0;i<items.length;i++) {
        var obj = items[i];
        if(obj) {
            td_ = td_ + '<tr>';
            for (var j in obj) {
                if (i == 0) {
                    th_ = th_ + '<th>' + j + '</th>';
                }

                // console.log('j val -- '+j);
                var col_value = obj[j];

                if(table_name == 'sizeusa_primary') { // first table data
                    if(!findItem(primary,j)){
                        if(length_type == 'cm') {
                            // col_value = parseFloat(col_value);

                            // console.log('col val -- '+col_value+'index -- '+i);

                            col_value = parseInt(col_value)*2.54; // inch to cm formula
                            col_value = col_value.toFixed(2)+' cm';
                            // col_value.toFixed(2);

                        }
                        else{
                            col_value = col_value.toFixed(2)+' inch';
                        }

                    }

                }

                else if(table_name == 'sizeusa_secondry') { // first table data
                    if(!findItem(secondry,j)){
                        if(length_type == 'cm') {
                            col_value = parseFloat(col_value)*2.54; // inch to cm formula
                            col_value = col_value.toFixed(2)+' cm';
                        }
                        else{
                            col_value = parseFloat(col_value).toFixed(2)+' inch';
                        }
                    }

                }

                else { // first table data
                    if(!findItem(image_twin,j)) {
                        if(length_type == 'inch') {
                            col_value = parseFloat(col_value)*0.39; // cm to inch formula
                            col_value = col_value.toFixed(2)+' inch';
                        }
                        else{
                            col_value = parseFloat(col_value).toFixed(2)+' cm';
                        }

                    }

                }

                td_ = td_ + '<td>' + col_value + '</td>';
            }

            td_ = td_ + '</tr>';
        }

    }


    th_ = th_+'</tr></thead>';
    td_ = td_+'</tbody>';
    setTimeout(function () {
        $("#overlay").css("display","none");
        $('#table_data').html(th_+td_);
        $('#table_data').dataTable({"order": [],"destroy":true});
        $("#total").html('Total : '+(total));

    },2000);

}

function findItem(arr,item) {
    var isFound = false;
    for(var i=0;i<arr.length;i++) {
        if(arr[i] == item) {
            isFound = true;
            break;
        }
    }
    return isFound;
}

function onLevelLoaded(step) {
    $(".results").removeClass("active");
    $(".results.results-"+step).addClass("active");
}

function onDataLoaded() {

    // alert('onDataLoaded()');
    var userId = $('#user_id').val();
    var table_name = $('#table_name').val();
    var data = $('#data').val();
    var selection = $('#selection').val();

    var th_ ='<thead><tr ><th>#</th><th>Name</th></tr></thead>';
    var td_='<tbody>';
    $("#total1").html("<label>Total : </label>"+(total));
    var counter = 0;
    var isItemFound = false;
    var url = 'api/reportProcess.php';
    $('#overlay').css('display', 'block');
    $('#result-data').html('');
    $('#chart-container').html('');

    $.post(url,{'type':'filterReportData','userId':userId,'tableName':table_name,'data':data,'selection':selection},function(data) {
        $('#overlay').css('display', 'none');
        var status = data.Status;
        var items = data.data;
        console.log(status);
        if(status == 'Success') {
            for (var i = 0; i < items.length; i++) {
                var obj = items[i];
                if (obj) {

                    counter = 1;
                    for (var j in obj) {
                        if (i == 0) {

                            var col_value = obj[j];
                            if (table_name == 'sizeusa_primary') { // first table data
                                if (!findItem(primary, j)) {

                                    if (counter == 1)
                                        td_ = td_ + '<tr class="active" style="cursor: pointer" onclick=printGraph("' + j + '") id="' + j + '">';
                                    else
                                        td_ = td_ + '<tr style="cursor: pointer" onclick=printGraph("' + j + '") id="' + j + '">';
                                    td_ = td_ + '<td>' + counter + '</td><td><span >' + j + '</span></td>';
                                    if (counter == 1) {
                                        printGraph(j);
                                    }
                                    counter = counter + 1;
                                    isItemFound = true;

                                }
                            }
                            else if (table_name == 'sizeusa_secondry') { // first table data
                                if (!findItem(secondry, j)) {

                                    if (counter == 1)
                                        td_ = td_ + '<tr class="active" style="cursor: pointer" onclick=printGraph("' + j + '") id="' + j + '">';
                                    else
                                        td_ = td_ + '<tr style="cursor: pointer" onclick=printGraph("' + j + '") id="' + j + '">';
                                    td_ = td_ + '<td>' + counter + '</td><td style="cursor: pointer" onclick=printGraph("' + j + '")><span>' + j + '</span></td>';
                                    if (counter == 1) {
                                        printGraph(j);
                                    }
                                    counter = counter + 1;
                                    isItemFound = true;
                                }
                            }
                            else { // first table data
                                if (!findItem(image_twin, j)) {

                                    if (counter == 1)
                                        td_ = td_ + '<tr class="active" style="cursor: pointer" onclick=printGraph("' + j + '") id="' + j + '">';
                                    else
                                        td_ = td_ + '<tr style="cursor: pointer" onclick=printGraph("' + j + '") id="' + j + '">';
                                    td_ = td_ + '<td>' + counter + '</td><td style="cursor: pointer" onclick=printGraph("' + j + '")><span >' + j + '</span></td>';
                                    if (counter == 1) {
                                        printGraph(j);
                                    }
                                    counter = counter + 1;
                                    isItemFound = true;
                                }
                            }

                        }

                        // console.log('j val -- '+j);

                        td_ = td_ + '</tr>';
                    }
                }
            }

            td_ = td_ + '</tbody>';
            $('#result-data').html(th_ + td_);
            if (!isItemFound) {
                alert("Appropirate data is not found graph can not be drawn");
                onLevelLoaded(1);
            }
        }

        // alert(isItemFound);
        // $('#result-data').dataTable({"destroy":true});
    });

}

function printGraph(key) {

    var data = findData(key);
    if(data.series.Male.length == 0 && data.series.Female.length == 0 ) {
        $("#chart-container").html("<label style='color: red'>Sorry appropirate data is not found try again with Male or Female search</label>")
        $("#selected-item").html('');
        return false;
    }
    // data.category.push("");
    // data.series.Male.push(null);
    // data.series.Female.push(null);
    data = filterData(data);
    console.log('data--'+JSON.stringify(data));

    var myConfig = {

        "graphset":[
            {
                "type":"pop-pyramid",
                "legend":{
                    "layout":"1x1", //row x column
                    "x":"25%",
                    "y":"5%"
                },
                "plot":{
                    "stacked":true,
                    "bar-width": "25px",
                    marginBottomOffset: 2,
                    marginLeftOffset: 2,
                    marginRightOffset: 2,
                    marginTopOffset: 2
                },

                "scale-x": {
                    "item":{
                        "font-size":10
                    },
                    "values":data.category,
                    "zooming": true,
                    "zoom-to": [0, 20]
                },
                "scroll-x":{
                    "bar":{
                        "background-color":"#8CF226",
                        "alpha":0.3,
                        "width":10
                    },
                    "handle":{
                        "background-color":"#8CF226"
                    }
                },
                "series":[
                    {
                        "data-side":1,
                        "background-color":"#00A4DC",
                        "values":data.series.Male,
                        "text":"Male",
                        "font-size":17
                    },
                    {
                        "data-side":2,
                        "background-color":"#f47b5d",
                        "values":data.series.Female,
                        "text":"Female"

                    }
                ]
            }
        ]
    };

    zingchart.render({
        id : 'chart-container',
        data : myConfig,
        height: 530,
        width: "100%"
    });
    $('#result-data>tbody>tr').removeClass('active');
    $('#'+key).addClass('active');
}

function findData(key) {
    var table_name = $('#table_name').val();
    // var data = {"dataset":[],"category":[],"x_axis":"Counter","y_axis":""};
    var data = {"series":{"Male":[],"Female":[]},"category":[]};
    var counter = 1;

    if(table_name == 'sizeusa_primary' || table_name =='sizeusa_secondry') {
        data.y_axis = "Units in (Inches)";
    }
    else {
        data.y_axis = "Units in (CM)";
    }
    for(var i=0;i<items.length;i++) {
        var obj = items[i];
        if (obj) {
            for (var j in obj) {


                var col_value = obj[j];
                if(table_name == 'sizeusa_primary') { // first table data
                    var gender = '';
                    var genderObj = {};
                    if(!findItem(primary,j) && j == key) {
                        $("#selected-item").html(key);
                        genderObj = findGender(obj);
                        // console.log('genderObj -- '+JSON.stringify(genderObj));
                        if(genderObj.isGender) {
                            if(genderObj.value == 'Male') {
                                data.series.Male.push(parseInt(col_value));
                            }
                            else if(genderObj.value == 'Female') {
                                data.series.Female.push(parseInt(col_value));
                            }
                            counter = counter+1;
                            // data.category.push(counter);

                        }

                    }

                }
                else if(table_name == 'sizeusa_secondry') { // first table data
                    if(!findItem(secondry,j) && j == key){
                        $("#selected-item").html(key);
                        genderObj = findGender(obj);
                        if(genderObj.isGender) {
                            if(genderObj.value == 'Male') {
                                data.series.Male.push(parseInt(col_value));
                            }
                            else if(genderObj.value == 'Female') {
                                data.series.Female.push(parseInt(col_value));
                            }
                            data.category .push(counter);
                            counter++;
                        }
                    }
                }
                else { // first table data
                    if(!findItem(image_twin,j) && j == key) {
                        genderObj = findGender(obj);
                        $("#selected-item").html(key);
                        if(genderObj.isGender) {
                            if(genderObj.value == 'Male') {
                                data.series.Male.push(parseInt(col_value));
                            }
                            else if(genderObj.value == 'Female') {
                                data.series.Female.push(parseInt(col_value));
                            }
                            data.category .push(counter);
                            counter++;
                        }
                    }
                }
            }
        }

    }

    return data;
}

function findGender(obj) {
    var response = {'isGender':false,value:''};

    for (var j in obj) {
        if(j == 'Gender') {
            response.isGender = true;
            response.value = obj[j];
            break;
        }
    }
    return response;
}

function filterData(data) {
    data.series.Male = data.series.Male.filter(function(e){if(!isNaN(e))return e});
    data.series.Female = data.series.Female.filter(function(e){if(!isNaN(e)) return e});

    if(data.series.Male.every(zeroTest) && data.series.Female.every(zeroTest)) {
        alert('Sorry!!! all data contains 0 so cannot draw chart');

    }
    var maleData = data.series.Male;
    var femaleData = data.series.Female;
    var max = 0;
    var min = 0;
    var interval = 0;
    var len = 0;
    var value = 0;
    var min_data = 0;
    var min_value = 0;

    if(maleData.length>0 ) {
        len = maleData.length;
        if(maleData.length>=10) {
            len = 10;
        }
        max = maleData.max();
        min = maleData.min();
        interval = parseFloat((max-min)/10);
        console.log('max value '+max+' min value '+min);
        console.log('interval '+interval);

        data.series.Male = [];
        value = min;
        for(var i=0;i<len;i++) {
            value = value+interval;
            data.series.Male.push(value);
        }
        min_data = parseInt(min/4);
        min_value = min;
        for(var i=0;i<4;i++) {
            min_value = min_value-min_data;
            // value = value+min_data;
            data.series.Male.push(min_value);
        }

        // console.log('male data -- '+data.series.Male);
    }

    if(femaleData.length>0) {
        max = femaleData.max();
        min = femaleData.min();
        interval = parseFloat((max-min)/10);
        len = femaleData.length;
        if(maleData.length>=10) {
            len = 10;
        }
        console.log('max value '+max+' min value '+min);
        console.log('interval '+interval);
        data.series.Female = [];
        value = min;

        for(var i=0;i<len;i++) {
            value = value+interval;
            data.series.Female.push(value);
        }

        min_data = parseInt(min/4);
        min_value = min;
        for(var i=0;i<4;i++) {
            min_value = min_value-min_data;
            // value = value+min_data;
            data.series.Female.push(min_value);
        }
        // console.log('female data -- '+data.series.Female);

    }

    if(items.length>0) {
        var total_items = items.length/10;
        data.category = [];

        var length = items.length;
        if(items.length>=10) {
            length = 14;
        }
        var tot_value = 0;
        for(var i=0;i<length;i++) {
            tot_value = tot_value+total_items;

            data.category.push(Math.ceil(tot_value));
        }
    }

    data.series.Male.sort(function (a,b) {
        return (b-a);
    });
    data.series.Female.sort(function (a,b) {
        return (b-a);
    });
    console.log('male data -- '+data.series.Male);
    console.log('female data -- '+data.series.Female);

    return data;
}

function zeroTest(element) {
    return element === 0;
}


Array.prototype.max = function() {
    return Math.max.apply(null, this);
};

Array.prototype.min = function() {
    return Math.min.apply(null, this);
};