<?php
/**
 * Created by PhpStorm.
 * User: Sourav
 * Date: 7/12/2017
 * Time: 3:21 PM
 */
 include ('header.php');
if(!isset($_SESSION['sua_user_id'])) {
    ?>
   <script>
       window.location = "report_gene.php";
    </script>
   <?php
}


 if(isset($_SESSION['userId'])) {
     /*$_SESSION['userId'] = '';
     $_SESSION['tableName'] = '';
     $_SESSION['data'] = '';
     $_SESSION['sql'] = '';*/
     unset($_SESSION['userId']);
     unset($_SESSION['tableName']);
     unset($_SESSION['data']);
     unset($_SESSION['sql']);


 }

if(isset($_SESSION['is_subscribed'])) {
    $is_subscribed = $_SESSION['is_subscribed'];
    if(!$is_subscribed) {
        ?>
        <script>
            window.location = "report_gene.php";
        </script>
        <?php
    }
}
else{
    ?>
    <script>
        window.location = "report_gene.php";
    </script>
    <?php
}
 ?>
    <link rel="stylesheet"type="text/css" href="css/mesasurements.css"/>

    <div class="container-fluid">
        <div class="col-md-12">
            <hr/>
        </div>


        <div id="exTab3" class="container" style="padding: 0 1%">
            <ul  class="nav nav-pills">
                <li onclick="loadData('sizeusa_primary','1b')" class="active">
                    <a  href="#1b" data-toggle="tab">Primary SizeUSA</a>
                </li>
                <li onclick="loadData('sizeusa_twins','2b')"><a href="#2b" data-toggle="tab">Imagetwin Data</a>
                </li>
                <li onclick="loadData('sizeusa_secondry','3b')"><a href="#3b" data-toggle="tab">Secondry SizeUSA</a>
                </li>
                <ul class="list-inline list-unstyled pull-right">
                    <li><label id="message"></label></li>

                    <li><button class="btn btn-primary" onclick="onCreateReport()"><i class="fa  fa-file-excel-o"></i> Report</button></li>
                </ul>

            </ul>

            <div class="tab-content clearfix" >
                <div class="tab-pane active" id="1b">

                </div>
                <div class="tab-pane" id="2b">
                </div>
                <div class="tab-pane" id="3b">
                </div>

            </div>
        </div>

    </div>

 <?php
  include ('footer.php');
 ?>

<script src="js/measurement.js" type="application/javascript"></script>
