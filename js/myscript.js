/**
 * Created by Sourav on 3/10/2017.
 */
function signIn(){
    $("#loginModal").modal("show");
}
function loginNow(){
    // alert('hello');
    var username = $("#login_uname").val();
    var password = $("#login_password").val();
    if(username == "" || password == ""){
        $("#loginerror").html("Please Fill UserName and Password First");
        return false;
    }
    var url = "api/registerUser.php";
    $.post(url,{"type":"login","username":username,"password":password},function(data){
        var Status = data.Status;
        if(Status == "Success"){
            location.reload();
        }else{
            $("#loginerror").html(data.Message);
            return false;
        }
    }).fail(function(){
        // $("#loginerror").html("Server Error !!! Please Try After Some Time...");
        return false;
    });
}
function registerNow(){
    var data = new FormData();
    var fname = $("#fname").val();
    var lname = $("#lname").val();
    var email = $("#email").val();
    var password = $("#password").val();
    var countryId = $("#countryId").val();
    var stateId = $("#stateId").val();
    var cityId = $("#cityId").val();
    var gender = $("#gender").val();
    var inputdob = $("#inputdob").val();
    var stylist = $("#stylist").val();
    var heightparam = $("#heightparam").val();
    var weightparam = $("#weightparam").val();
    var weight = $("#weight").val();
    var language = $("#language").val();
    var user_profile = $("#user_profile").val();
    var height = "";
    if(fname == "" || lname == "" || email == "" || password == ""|| countryId == ""|| stateId == ""|| cityId == ""||
    gender == "" || inputdob == "" || stylist == "" || heightparam == "" || weightparam == "" || weight == ""||
    language == "" || user_profile == ""){
        $("#registererror").html("Please Fill all the Fields");
        return false;
    }
    if(heightparam == "Centimeters"){
        height = parseInt($("#height").val()*0.393701);
    }
    else if(heightparam == "Feets"){
        height = parseInt($("#height0").val()*12)+parseInt($("#height1").val());
    }
    else{
        height = parseInt($("#height").val());
    }
    if(weightparam == "Kilograms"){
        weight = parseInt(weight*2.20462);
    }
    else if(weightparam == "Stones"){
        weight = parseInt(weight*14);
    }
    var file_ext = user_profile.split(".");
    file_ext = file_ext[file_ext.length - 1];
    if (file_ext.toLowerCase() == "jpeg" || file_ext.toLowerCase() == "png" || file_ext.toLowerCase() == "jpg" || file_ext.toLowerCase() == "gif") {
        var _file = document.getElementById('user_profile');
        data.append('type', 'Register');
        data.append('user_profile', _file.files[0]);
        data.append('fname', fname);
        data.append('lname', lname);
        data.append('email', email);
        data.append('password', password);
        data.append('country', countryId);
        data.append('state', stateId);
        data.append('city', cityId);
        data.append('gender', gender);
        data.append('dob', inputdob);
        data.append('stylist', stylist);
        data.append('height', height);
        data.append('weight', weight);
        data.append('language', language);
    }else{
        $("#registererror").html("Invalid File Type");
        return false;
    }
    var request = new XMLHttpRequest();
    request.onreadystatechange = function(){
        if(request.readyState == 4){
            var response = $.parseJSON(request.response);
            var status = response.Status;
            if(status == "Success"){
                $("#registerModal").modal("hide");
                $("#loginModal").modal("show");
            }
            else{
                $("#registererror").html(response.Message);
                return false;
            }
        }
    };
    request.open('POST', 'api/registerUser.php');
    request.send(data);
}
function switchmodel(currentModel){
    if(currentModel == "login"){
        $("#loginModal").modal("hide");
        $("#registerModal").modal("show");
    }else{
        $("#registerModal").modal("hide");
        $("#loginModal").modal("show");
    }
}
function countrydate(){
    var country = $("#countryId").val();
    if(country == "United States"){
        $('#inputdob').datetimepicker({
            minView : 2,
            format:"mm/dd/yyyy",
            autoclose: true
        });
    }else{
        $('#inputdob').datetimepicker({
            minView : 2,
            format:"dd/mm/yyyy",
            autoclose: true
        });
    }
}
function heightFeild(){
    var heightparam = $("#heightparam").val();
    if(heightparam == "Feets"){
        $("#heightFeilds").html("<div class='col-md-6' style='padding:0'><input type='text' id='height0' " +
            "class='form-control' placeholder='Feets' /></div><div class='col-md-6' style='padding:0'><input" +
            " type='text' class='form-control' id='height1' placeholder='Inches' /></div>");
    }
    else{
        $("#heightFeilds").html("<input type='text' class='form-control' id='height' placeholder='Enter Height' />");
    }
}
function userProfile(){
    var user_id = $("#user_id").val();
    if(user_id != "") {
        var url = "api/registerUser.php";
        $.post(url, {"type": "getParticularUserData", "user_id": user_id}, function (data) {
            var Status = data.Status;
            if (Status == "Success") {
                var userData = data.userData;
                var username = userData.name;
                $("#userName").html(" " + username);
                if (userData.profile_pic != "") {
                    $("#profileimg").attr("src", userData.profile_pic);
                }
            } else {
                showMessage(data.Message, "red");
            }
        }).fail(function () {
            // showMessage("Server Error !!! Please Try After Some Time", 'red');
        });
    }
}
function logout(){
    var url = "api/registerUser.php";
    $.post(url,{"type":"Logout"},function(data) {
        var Status = data.Status;
        if (Status == "Success") {
            showMessage(data.Message);
            window.location='index.php';
        }else{
            showMessage(data.Message);
            window.location='index.php';
        }
    });
}
userProfile();




function addNewCategory(){
    $(".modal-header").css({"display":"block"});
    $(".modal-title").html("Add New Category");
    $(".modal-body").html("<p id='message' style='text-align: center;font-size: 14px;color: red'></p>" +
    "<div class='row'><label class='label label-danger'>Please Fill the Required Data</label><label class='error'>" +
    "</label></div><br><div class='row'><div class='col-md-6 form-group'><label>Category Name (Max 50 Characters)</label>" +
    "<input type='text' maxlength='50' id='cat_name' value='' placeholder='Category Name' required class='form-control' />" +
    "</div><div class='col-md-6 form-group'><label>Select Visibilty Status</label><div><label><input type='radio' " +
    "name='cat_status' value='1' checked /> Show </label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"+
    "<label><input type='radio' name='cat_status' value='0' /> Hide</label></div></div></div><div class='row'>" +
    "<div class='col-md-6 form-group' ><label>Category Display Picture</label><input type='hidden' name='type' " +
    "value='addCategory' /><input type='file'" +
    "id='cat_image' onchange=setPhoto(this,'livepic') style='position: absolute;width:58px;height:60px;opacity:0;' " +
    "/><div class='photobox'><img src='images/img.png' class='livepic img-responsive' /></div>" +
    "</div><input type='hidden' value='addCategory' id='type' /><div class='form-group col-md-6' style='text-align:right;margin-top:40px'><input type='button' " +
    "value='Cancel' data-dismiss='modal' class='btn btn-default'/>&nbsp; <input type='Submit' value='Add Category' " +
    "onclick=addContent('') class='formbtn btn btn-info' /></div> </div><img src='images/default.gif' class='loadNow' />");
    $(".modal-footer").css({"display":"none"});
    $("#myModal").modal("show");
}
function editCategoryData(cat_id){
    var url = "api/categoryProcess.php";
    $.post(url,{"type":"getCategory","cat_id":cat_id} ,function (data) {
        var status = data.Status;
        if (status == "Success"){
            var catData = data.catData;
            $(".modal-header").css({"display":"block"});
            $(".modal-title").html("Edit Category");
            $(".modal-body").html("<p id='message' style='text-align: center;font-size: 14px;color: red'></p>" +
            "<div class='row'><label class='label label-danger'>Please Fill the Required Data</label><label class='error'>" +
            "</label></div><br><div class='row'><div class='col-md-6 form-group'><label>Category Name (Max 50 Characters)</label>" +
            "<input type='text' id='cat_name' maxlength='50' value='"+catData.cat_name+"' placeholder='Category Name' required class='form-control' />" +
            "</div><div class='col-md-6 form-group'><label>Select Visibilty Status</label><div><label><input type='radio' " +
            "name='cat_status' value='1' id='on' checked /> Show </label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"+
            "<label><input type='radio' id='off' name='cat_status' value='0' /> Hide</label></div></div></div><div class='row'>" +
            "<div class='col-md-6 form-group' ><label>Category Display Picture</label><input type='file' " +
            "id='cat_image' onchange=setPhoto(this,'livepic') style='position: absolute;width:58px;height:60px;opacity:0;' " +
            "/><div class='photobox'><img src='api/Files/images/"+catData.cat_image+"' class='livepic img-responsive' /></div>" +
            "</div><div class='form-group col-md-6' style='text-align:right;" +
            "margin-top:40px'><input type='hidden' value='editCategory' id='type' /><input type='button' value='Cancel' data-dismiss='modal' " +
            "class='btn btn-default'/>&nbsp;<input type='button' value='Update Category' onclick=addContent('"+cat_id+"') class='formbtn btn btn-info' />" +
            "</div></div><img src='images/default.gif' class='loadNow' />");
            $(".modal-footer").css({"display":"none"});
            if(catData.cat_status == "1"){
                $("#on").attr("checked",true);
            }else{
                $("#off").attr("checked",true);
            }
            $("#myModal").modal("show");
        }
        else{
            showMessage("Server Error!!! Please Try After Some Time","red");
        }
    }).fail(function(){
        showMessage("Server Error!!! Please Try After Some Time","red");
    });
}
function addContent(cat_id)
{
    var cat_name = $("#cat_name").val();
    var cat_status = $('input[name=cat_status]:checked').val();
    var cat_image = $('#cat_image').val();
    var type = $('#type').val();
    var data = new FormData();
    if(type == "addCategory") {
        if (cat_name == "" || cat_image == "") {
            $("#message").html("Please Fill All the Required Feilds");
            return false;
        }
        else {
            $("#message").html("");
            $(".loadNow").css({"display": "block"});
            $(".formbtn").attr("disabled", true);
            var file_ext = cat_image.split(".");
            file_ext = file_ext[file_ext.length - 1];
            if (file_ext == "jpeg" || file_ext == "png" || file_ext == "jpg" || file_ext == "gif") {
                var _file = document.getElementById('cat_image');
                data.append('type', type);
                data.append('cat_image', _file.files[0]);
                data.append('cat_name', cat_name);
                data.append('cat_status', cat_status);
            }
            else {
                $("#message").html("File Should Be JPG,PNG,JPEG,GIF Formats Only");
                $(".loadNow").css({"display": "none"});
                $(".formbtn").attr("disabled", false);
                return false;
            }
        }
    }
    else{
        if(cat_name == ""){
            $("#message").html("Please Fill All the Required Feilds");
            return false;
        }
        else{
            $("#message").html("");
            $(".loadNow").css({"display": "block"});
            $(".formbtn").attr("disabled", true);
            var imageChanged = "no";
            if(cat_image != ""){
                var file_ext = cat_image.split(".");
                file_ext = file_ext[file_ext.length - 1];
                if (file_ext == "jpeg" || file_ext == "png" || file_ext == "jpg" || file_ext == "gif") {
                    var _file = document.getElementById('cat_image');
                    imageChanged = "yes";
                    data.append('cat_image', _file.files[0]);
                }
                else {
                    $("#message").html("File Should Be JPG,PNG,JPEG,GIF Formats Only");
                    $(".loadNow").css({"display": "none"});
                    $(".formbtn").attr("disabled", false);
                    return false;
                }
            }
            data.append('type', type);
            data.append('cat_name', cat_name);
            data.append('cat_status', cat_status);
            data.append('cat_id', cat_id);
            data.append('imageChanged',imageChanged);
        }
    }
    var request = new XMLHttpRequest();
    request.onreadystatechange = function(){
        if(request.readyState == 4){
            var response = $.parseJSON(request.response);
            var status = response.Status;
            if(status == "Success")
            {
                $("#myModal").modal("hide");
                location.reload(true);
            }
            else
            {
                $("#modal").modal("hide");
                $("#message").html(response.Message);
            }
        }
    };
    request.open('POST', 'api/categoryProcess.php');
    request.send(data);
}
function deleteCategory(cat_id,booksCount) {
    $(".modal-header").css({"display":"block"});
    $(".modal-title").html("Delete Permission");
    $(".modal-body").css({"display":"block"});
    if(booksCount>0){
        $(".modal-body").html("<span style='color:red'>This Category Contains "+booksCount+" Books. Are You Sure you want to Delete this Category</span>");
    }
    else{
        $(".modal-body").html("<span style='color:red'>Are You Sure you want to Delete this Category</span>");
    }
    $(".modal-footer").css({"display":"block"});
    $(".modal-footer").html("<input type='button' value='Cancel' data-dismiss='modal' class='btn btn-sm btn-default' />" +
    "<input type='button' value='Delete' data-dismiss='modal' onclick=confirmDelete('"+cat_id+"') class='btn btn-sm btn-danger' />");
    $("#myModal").modal("show");
}
function confirmDelete(cat_id){
    var url = "api/categoryProcess.php";
    $.post(url,{"type":"deleteCategory","cat_id":cat_id} ,function (data) {
        var status = data.Status;
        setTimeout(function(){
            if (status == "Success"){
                showMessage(data.Message,"green");
                location.reload(true);
            }
            else{
                showMessage(data.Message,"red");
            }
        },500);
    }).fail(function(){
        showMessage("Server Error!!! Please Try After Some Time","red")
    });
}
//////////////////////////////////////////////////////////////////////////////
///category end
//////////////////////////////////////////////////////////////////////////////
function addNewBook(){
    var url = "api/categoryProcess.php";
    $.post(url,{"type":"getCategories"} ,function(data) {
        var status = data.Status;
        var catShow = "<option value='' selected='selected'>Please Select Category</option>";
        if (status == "Success") {
            var categoryData = data.data;
            for (var i = 0; i < categoryData.length; i++) {
                if (categoryData[i].cat_status == "1") {
                    catShow += "<option value='" + categoryData[i].cat_id + "' >" + categoryData[i].cat_name + "</option>";
                }
            }
            var couponURL = "api/discountProcess.php";
            $.post(couponURL, {"type": "getCoupons"}, function (data) {
                var status = data.Status;
                var couponShow = "<option value=''>Select Discount Coupons</option>";
                if (status == "Success") {
                    var couponData = data.data;
                    for (var i = 0; i < couponData.length; i++) {
                        if (couponData[i].coupon_status == "1") {
                            couponShow += "<option value='"+couponData[i].coupon_id+";"+couponData[i].coupon_value+"'>" + couponData[i].coupon_code + "</option>";
                        }
                    }
                    $(".modal-header").css({"display": "block"});
                    $(".modal-title").html("Add New Audio Book");
                    $(".modal-body").html("<p id='message' style='text-align: center;font-size: 14px;color: red'></p>" +
                        "<div class='row'><label class='label label-danger'>Please Fill the Required Data</label><label class='error'>" +
                        "</label></div><br><div class='row'><div class='col-md-6 form-group'><label>Book Title</label>" +
                        "<input type='text' id='book_title' value='' placeholder='Enter Book Title' required class='form-control' />" +
                        "</div><div class='col-md-6 form-group'><label>Author Name</label><input type='text' class='form-control' " +
                        "id='book_author' value='' placeholder='Enter Book Author Name'/></div></div><div class='row'><div class='col-md-6 form-group' >" +
                        "<label>Book Narrator</label><input type='text' class='form-control' value='' " +
                        "placeholder='Enter Name of Narrator' id='book_narrator' /></div><div class='col-md-6 form-group' ><label>" +
                        "Book Price</label><input type='text' value='' class='form-control' placeholder='Enter Price of Book' " +
                        "id='book_price' /></div></div><div class='row'><div class='col-md-6 form-group'><label>Book Description</label>" +
                        "<textarea id='book_desc' placeholder='Enter Book Description' class='form-control' ></textarea>" +
                        "</div><div class='col-md-6 form-group'><label>Short Audio Preview</label>" +
                        "<input type='file' id='short_audio_file' style='margin-top:20px;width:100%' />" +
                        "</div></div><div class='row'><div class='col-md-4'><label>Discount Coupon (Optional)</label>" +
                        "<select class='form-control' id='discount_on_book'>" + couponShow + "</select></div><div class='form-group col-md-4'>" +
                        "<label>Select Book Category</label><select id='book_category'" +
                        " class='form-control'>" + catShow + "</select></div>" +
                        "<div class='form-group col-md-4'><label>Book Audio File</label><input type='file' id='audio_file' " +
                        "style='width:100%' /></div></div>" +
                        "<div class='row'><div class='form-group col-md-3'><label>Book Front Look</label><input type='file' id='image_file'" +
                        " onchange=setPhoto(this,'livepic') style='position: absolute;width:58px;height:60px;opacity:0' /><div " +
                        "class='photobox'><img src='images/img.png' class='livepic img-responsive' /></div></div>" +
                        "<div class='col-md-4 form-group'><label>Select Visibilty Status</label><div><br><label><input type='radio' " +
                        "name='book_status' value='1' checked /> Show </label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" +
                        "<label><input type='hidden' value='addBook' id='type' /><input type='radio' name='book_status' value='0' /> Hide" +
                        "</label></div></div><div class='form-group " +
                        "col-md-5' style='text-align:right;margin-top:40px'><input type='button' value='Cancel' " +
                        "data-dismiss='modal' class='btn btn-default'/>&nbsp;<input type='button' value='Add New Book' " +
                        "class='btn btn-info formbtn' onclick=bookProcess('') /></div></div><img src='images/default.gif' class='loadNow' />");
                    $(".modal-footer").css({"display": "none"});
                    $("#myModal").modal("show");
                }
            });
        }
    });
}
function editBookData(book_id) {
    var url = "api/booksProcess.php";
    $.post(url, {"type": "getBook", "bookId": book_id}, function (data) {
        var status = data.Status;
        if (status == "Success") {
            var bookData = data.bookData;
            var url = "api/categoryProcess.php";
            $.post(url, {"type": "getCategories"}, function (data) {
                var status = data.Status;
                var catShow = "<option value=''>Please Select Category</option>";
                if (status == "Success") {
                    var categoryData = data.data;
                    for (var i = 0; i < categoryData.length; i++) {
                        if (bookData.cat_id == categoryData[i].cat_id) {
                            catShow += "<option value='" + categoryData[i].cat_id + "' selected='selected' >" + categoryData[i].cat_name + "</option>";
                        }
                        else {
                            catShow += "<option value='" + categoryData[i].cat_id + "' >" + categoryData[i].cat_name + "</option>";
                        }
                    }
                    var couponURL = "api/discountProcess.php";
                    $.post(couponURL, {"type": "getCoupons"}, function (data) {
                        var status = data.Status;
                        var couponShow = "<option value=''>Select Discount Coupons</option>";
                        if (status == "Success") {
                            var couponData = data.data;
                            for (var i = 0; i < couponData.length; i++) {
                                if(couponData[i].coupon_status == "1") {
                                    if (bookData.discount_id == couponData[i].coupon_id) {
                                        couponShow += "<option value='" + couponData[i].coupon_id + ";" + couponData[i].coupon_value + "' selected='selected' >"
                                            + couponData[i].coupon_code + "</option>";
                                    }
                                    else {
                                        couponShow += "<option value='" + couponData[i].coupon_id + ";" + couponData[i].coupon_value + "'>"
                                            + couponData[i].coupon_code + "</option>";
                                    }
                                }
                            }
                            $(".modal-header").css({"display": "block"});
                            $(".modal-title").html("Edit Audio Book");
                            $(".modal-body").html("<p id='message' style='text-align: center;font-size: 14px;color: red'>" +
                            "</p><div class='row'><label class='label label-danger'>Please Fill the Required Data</label>" +
                            "<label class='error'></label></div><br><div class='row'><div class='col-md-6 form-group'>" +
                            "<label>Book Title</label><input type='text' id='book_title' value='"+bookData.book_name+"'" +
                            " placeholder='Enter Book Title' required class='form-control' /></div><div class='col-md-6 " +
                            "form-group'><label>Author Name</label><input type='text' class='form-control' " +
                            "id='book_author' value='" + bookData.book_author + "' placeholder='Enter Book Author Name'/>" +
                            "</div></div><div class='row'><div class='col-md-6 form-group' ><label>Book Narrator</label>" +
                            "<input type='text' class='form-control' value='" + bookData.book_narrator + "' " +
                            "placeholder='Enter Name of Narrator' id='book_narrator' /></div><div class='col-md-6 " +
                            "form-group' ><label> Book Price</label><input type='text' value='"+bookData.list_price+"'" +
                            " class='form-control' placeholder='Enter Price of Book' id='book_price' /></div></div>" +
                            "<div class='row'><div class='col-md-6 form-group'><label>Book Description</label>" +
                            "<textarea id='book_desc' placeholder='Enter Book Description' class='form-control' >"
                            + bookData.book_desc + "</textarea></div><div class='col-md-6 form-group'><label>Short Audio" +
                            " Preview</label>" + "<input type='file' id='short_audio_file' " +
                            "style='margin-top:20px;width:100%;display:none' /><p style='line-height:59px' " +
                            "id='oldShortBook'>"+bookData.short_audio_file+"<i class='fa fa-edit' onclick=editbookaudio2()" +
                            " style='cursor:pointer'>" +
                            "</i></p></div></div><div class='row'><div class='col-md-4'>" +
                            "<label>Discount Coupon (Optional)</label><select class='form-control' id='discount_on_book'>"+
                            couponShow+"</select></div><div class='form-group col-md-4'><label>Select Book Category</label>" +
                            "<select id='book_category' class='form-control'>" + catShow + "</select></div>" +
                            "<div class='form-group col-md-4'><label>Book Audio File</label><input type='file' " +
                            "id='audio_file' style='display:none;width:100%' /><p style='line-height:23px' id='oldBook'>"+
                            bookData.audio_file+" <i class='fa fa-edit' onclick ='editbookaudio()' style='cursor:pointer'>" +
                            "</i></p></div></div><div class='row'><div class='form-group col-md-3'>" +
                            "<label>Book Front Look</label><input type='file' id='image_file' " +
                            "onchange=setPhoto(this,'livepic') style='position: absolute;width:58px;height:60px;opacity:0'" +
                            " /><div class='photobox'><img src='api/Files/images/"+bookData.front_look+"' " +
                            "class='livepic img-responsive' /></div></div><div class='col-md-4 form-group'>" +
                            "<label>Select Visibilty Status</label><div><br><label><input type='radio' " +
                            "name='book_status' value='1' id='on' /> Show </label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" +
                            "&nbsp;&nbsp;&nbsp;<label><input type='hidden' value='editBook' id='type' /><input type='radio'" +
                            " name='book_status' id='off' value='0' /> Hide</label></div></div><div class='form-group " +
                            "col-md-5' style='text-align:right;margin-top:40px'><input type='button' value='Cancel' " +
                            "data-dismiss='modal' class='btn btn-default'/>&nbsp;<input type='button' value='Update Book'" +
                            "class='btn btn-info formbtn' onclick=bookProcess('"+book_id+"') /></div></div>" +
                            "<img src='images/default.gif' class='loadNow' />");
                            $(".modal-footer").css({"display": "none"});
                            if(bookData.book_status == "1"){
                                $("#on").attr("checked",true);
                            }else{
                                $("#off").attr("checked",true);
                            }
                            $("#myModal").modal("show");
                        }
                    });
                }
            });
        }
    });
}
function editbookaudio(){
    $("#oldBook").hide();
    $("#audio_file").show();
}
function editbookaudio2(){
    $("#oldShortBook").hide();
    $("#short_audio_file").show();
}
function bookProcess(book_id)
{
    var book_title = $("#book_title").val();
    var book_desc = $("#book_desc").val();
    var book_author = $("#book_author").val();
    var book_narrator = $("#book_narrator").val();
    var book_price = $("#book_price").val();
    book_price = parseFloat(book_price);
    if(book_price<0){
        $("#message").html("Book Price Should Not Less than 0 (Zero)");
        return false;
    }
    var discount_on_book = $("#discount_on_book").val();
    var cat_id = $("#book_category").val();
    var book_status = $('input[name=book_status]:checked').val();
    var book_image = $('#image_file').val();
    var book_audio = $('#audio_file').val();
    var book_short_audio = $('#short_audio_file').val();
    var type = $('#type').val();
    var data = new FormData();
    if(type == "addBook") {
        if (book_title == "" || book_author == "" || book_narrator == "" || cat_id == "" ||
            book_image == "" || book_audio == "" || book_short_audio == "") {
            $("#message").html("Please Fill All the Required Feilds");
            return false;
        }
        else {
            if(discount_on_book != ""){
                var temp = discount_on_book.split(";");
                if(parseFloat(temp[1])>parseFloat(book_price)){
                    $("#message").html("Coupon Code Amount Should not be Greater than Book Price");
                    return false;
                }
                discount_on_book = temp[0];
            }
            $("#message").html("");
            $(".loadNow").css({"display": "block"});
            $(".formbtn").attr("disabled", true);
            var audio_ext = book_audio.split(".");
            audio_ext = audio_ext[audio_ext.length - 1];
            if(audio_ext == "mp3" || audio_ext == "wav" || audio_ext == "amr") {
                var audio_file = document.getElementById('audio_file');
                var image_ext = book_image.split(".");
                image_ext = image_ext[image_ext.length - 1];
                if (image_ext == "jpg" || image_ext == "png" || image_ext == "gif") {
                    var image_file = document.getElementById('image_file');
                    var short_audio = book_short_audio.split(".");
                    short_audio = short_audio[short_audio.length - 1];
                    if (short_audio == "mp3" || short_audio == "wav" || short_audio == "amr") {
                        var short_audio_file = document.getElementById('short_audio_file');
                        data.append('type', type);
                        data.append('book_title', book_title);
                        data.append('book_desc', book_desc);
                        data.append('book_author', book_author);
                        data.append('book_narrator', book_narrator);
                        data.append('book_price', book_price);
                        data.append('discount_on_book', discount_on_book);
                        data.append('cat_id', cat_id);
                        data.append('book_status', book_status);
                        data.append('book_image', image_file.files[0]);
                        data.append('book_audio', audio_file.files[0]);
                        data.append('book_short_audio', short_audio_file.files[0]);
                    }
                    else{
                        $("#message").html("Audio Preview File Should Be MP3, WAV, AMR Formats Only");
                        $(".loadNow").css({"display": "none"});
                        $(".formbtn").attr("disabled", false);
                        return false;
                    }
                }
                else{
                    $("#message").html("Image File Should Be JPG, PNG, GIF Formats Only");
                    $(".loadNow").css({"display": "none"});
                    $(".formbtn").attr("disabled", false);
                    return false;
                }
            }
            else {
                $("#message").html("Audio File Should Be MP3, WAV, AMR Formats Only");
                $(".loadNow").css({"display": "none"});
                $(".formbtn").attr("disabled", false);
                return false;
            }
        }
    }
    else{
        if(book_title == "" || book_author == "" || book_narrator == "" || cat_id == "" ){
            $("#message").html("Please Fill All the Required Feilds");
            return false;
        }
        else{
            if(discount_on_book != ""){
                var temp = discount_on_book.split(";");
                if(parseFloat(temp[1])>parseFloat(book_price)){
                    $("#message").html("Coupon Code Amount Should not be Greater than Book Price");
                    return false;
                }
                discount_on_book = temp[0];
            }
            $("#message").html("");
            $(".loadNow").css({"display": "block"});
            $(".formbtn").attr("disabled", true);
            var imageChanged = "no";
            var audioChanged = "no";
            var shortaudioChanged = "no";
            if(book_audio != "") {
                var audio_ext = book_audio.split(".");
                audio_ext = audio_ext[audio_ext.length - 1];
                if (audio_ext == "mp3" || audio_ext == "wav" || audio_ext == "amr") {
                    var audio_file = document.getElementById('audio_file');
                    audioChanged = "yes";
                    data.append('book_audio', audio_file.files[0]);
                }
                else {
                    $("#message").html("Audio File Should Be MP3,WAV,AMR Formats Only");
                    $(".loadNow").css({"display": "none"});
                    $(".formbtn").attr("disabled", false);
                    return false;
                }
            }
            if(book_short_audio != "") {
                var short_audio = book_short_audio.split(".");
                short_audio = short_audio[short_audio.length - 1];
                if (short_audio == "mp3" || short_audio == "wav" || short_audio == "amr") {
                    var short_audio_file = document.getElementById('short_audio_file');
                    shortaudioChanged = "yes";
                    data.append('book_short_audio', short_audio_file.files[0]);
                }
                else {
                    $("#message").html("Audio File Should Be MP3,WAV,AMR Formats Only");
                    $(".loadNow").css({"display": "none"});
                    $(".formbtn").attr("disabled", false);
                    return false;
                }
            }
            if(book_image != "") {
                var image_ext = book_image.split(".");
                image_ext = image_ext[image_ext.length - 1];
                if (image_ext == "jpg" || image_ext == "png" || image_ext == "gif") {
                    var image_file = document.getElementById('image_file');
                    imageChanged = "yes";
                    data.append('book_image', image_file.files[0]);
                }
                else {
                    $("#message").html("Image File Should Be JPG, PNG, GIF Formats Only");
                    $(".loadNow").css({"display": "none"});
                    $(".formbtn").attr("disabled", false);
                    return false;
                }
            }
            data.append('type', type);
            data.append('book_title', book_title);
            data.append('book_desc', book_desc);
            data.append('book_author', book_author);
            data.append('book_narrator', book_narrator);
            data.append('book_price', book_price);
            data.append('discount_on_book', discount_on_book);
            data.append('cat_id', cat_id);
            data.append('book_status', book_status);
            data.append('book_id', book_id);
            data.append('imageChanged',imageChanged);
            data.append('audioChanged',audioChanged);
            data.append('shortaudioChanged',shortaudioChanged);
        }
    }
    var request = new XMLHttpRequest();
    request.onreadystatechange = function(){
        if(request.readyState == 4){
            var response = $.parseJSON(request.response);
            var status = response.Status;
            if(status == "Success")
            {
                $("#myModal").modal("hide");
                location.reload(true);
            }
            else
            {
                $("#message").html(response.Message);
                $(".loadNow").css({"display": "none"});
                $(".formbtn").attr("disabled", false);
                return false;
            }
        }
    };
    request.open('POST', 'api/booksProcess.php');
    request.send(data);
}
function deleteBook(book_id) {
    $(".modal-content").css({"display":"block"});
    $(".modal-dialog").css({"display":"block"});
    $(".modal-header").css({"display":"block"});
    $(".modal-title").html("Delete Permission");
    $(".modal-body").css({"display":"block"});
    $(".modal-body").html("<span style='color:red'>Are You Sure you want to Delete this Book</span>");
    $(".modal-footer").css({"display":"block"});
    $(".modal-footer").html("<input type='button' value='Cancel' data-dismiss='modal' class='btn btn-sm btn-default' />" +
    "<input type='button' value='Delete' data-dismiss='modal' onclick=confirmBookDelete('"+book_id+"') class='btn btn-sm btn-danger' />");
    $("#myModal").modal("show");
}
function confirmBookDelete(book_id){
    var url = "api/booksProcess.php";
    $.post(url,{"type":"deleteBook","book_id":book_id} ,function (data) {
        var status = data.Status;
        setTimeout(function(){
            if (status == "Success"){
                showMessage(data.Message,"green");
                location.reload(true);
            }
            else{
                showMessage(data.Message,"red");
            }
        },500);
    }).fail(function(){
        showMessage("Server Error!!! Please Try After Some Time","red")
    });
}
function playSong(songfile) {
    $(".modal-header").css({"display":"none"});
    $(".modal-body").css({"display":"none"});
    $(".modal-footer").css({"display":"none"});
    $(".modal-dialog").html("<video class='playsong' style='margin:20% 0 0 40%' controls autoplay><source src='api/Files/audio/"+songfile+"' /></video>" +
    "<i class='fa fa-close fa-2x' onclick='stopMusic()' style='color:white;cursor:pointer;margin-top:-60px'></i>");
    $("#myModal").modal("show");
}
function stopMusic(){
    $(".playsong").remove();
    $("#myModal").modal("hide");
    setTimeout(function () {
        $(".modal-dialog").html("<div class='modal-content'><div class='modal-header'><button type='button' class='close' " +
        "data-dismiss='modal'>&times;</button><h4 class='modal-title'></h4></div><div class='modal-body'>" +
        "</div><div class='modal-footer'></div></div>");
    },300);
}
///////////////////////////////////////////////////////////////////////////////////
/////////////////////////////book portion End//////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////
function deleteUser(user_id) {
    $(".modal-content").css({"display":"block"});
    $(".modal-dialog").css({"display":"block"});
    $(".modal-header").css({"display":"block"});
    $(".modal-title").html("Delete Permission");
    $(".modal-body").css({"display":"block"});
    $(".modal-body").html("<span style='color:red'>Are You Sure you want to Delete this User</span>");
    $(".modal-footer").css({"display":"block"});
    $(".modal-footer").html("<input type='button' value='Cancel' data-dismiss='modal' class='btn btn-sm btn-default' />" +
    "<input type='button' value='Delete' data-dismiss='modal' onclick=confirmUserDelete('"+user_id+"') class='btn btn-sm btn-danger' />");
    $("#myModal").modal("show");
}
function confirmUserDelete(user_id){
    var url = "api/userProcess.php";
    $.post(url,{"type":"deleteUser","user_id":user_id} ,function (data) {
        var status = data.Status;
        setTimeout(function(){
            if (status == "Success"){
                showMessage(data.Message,"green");
                location.reload(true);
            }
            else{
                showMessage(data.Message,"red");
            }
        },500);
    }).fail(function(){
        showMessage("Server Error!!! Please Try After Some Time","red")
    });
}
$(document).ready(function () {
    var months = ["Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"];
    $('#datetimepicker6').datetimepicker({
        format:"D MMM YYYY"
    });

});
function setPhoto(input,showClass) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            $('.'+showClass).attr('src', e.target.result);
        };
        reader.readAsDataURL(input.files[0]);
    }
}
function showMessage(message,color){
    $(".modal-header").css({"display":"none"});
    $(".modal-body").css({"display":"block"});
    $(".modal-body").html("<span style='color:"+color+"'>"+message+"</span>");
    $(".modal-footer").css({"display":"none"});
    $("#myModal").modal("show");
    setTimeout(stopMusic,2000);
}
function changeAdminPassword(admin_id){
    $(".modal-header").css({"display":"block"});
    $(".modal-title").html('Change Admin Password');
    $(".modal-body").html("<div class='row'><div " +
    "class='col-md-3'></div><div class='col-md-6'><div class='form-group'>" +
    "<label>Old Password</label><input type='password' class='form-control' id='oldPassword' /></div><div class='form-group'><label>" +
    "New Password</label><input type='password' class='form-control' id='newPassword' /></div><div class='form-group'><label>" +
    "Confirm Password</label><input type='password' class='form-control' id='confirmNewPassword' /></div><input type='submit' " +
    "class='btn btn-info pull-right' data-dismiss='modal' onclick=changePassword('"+admin_id+"') value='Change Password'/>" +
    "</div></div><div class='col-md-3'></div>");
    $(".modal-footer").css({"display":"none"});
    $("#myModal").modal("show");
}
function changePassword(admin_id){
    var url = "api/admin_login.php";
    var oldPassword = $("#oldPassword").val();
    var newPassword = $("#newPassword").val();
    var confirmNewPassword = $("#confirmNewPassword").val();
    if(newPassword == confirmNewPassword) {
        setTimeout(function(){
            $.post(url, {
                "type": "changeAdminPassword",
                "old_password": oldPassword,
                "new_password": newPassword,
                "admin_id": admin_id
            }, function (data) {
                var status = data.Status;
                if (status == "Success") {
                    showMessage(data.Message, "green");
                }
                else {
                    showMessage(data.Message, "red");
                }
            }).fail(function () {
                showMessage("Server Error!!! Please Try After Some Time", "red")
            });
        },1000);
    }
    else{
        showMessage("New Password and Confirm Password Should Be Same", "red");
    }
}
function back(){
    window.history.back();
}
function cancelOrder(order_id,generated_date){
    var last30 = moment().subtract(30, 'days');
    var last30ts = Math.floor(new Date(last30).getTime()/1000);
    if(generated_date<last30ts){
        showMessage("Sorry Your Order is More Than 30 Days Old so we are not able to cancel this order","red");
    }
    else{
        var url = "api/orderProcess.php";
        $.post(url,{"type":"cancelOrder","order_id":order_id} ,function (data) {
            var status = data.Status;
            if (status == "Success"){
                showMessage("Your Order has Been Cancelled SuccessFully","green");
                location.reload();
            }
            else{
                showMessage(data.Message,"red");
            }
        }).fail(function(){
            showMessage("Server Error!!! Please Try After Some Time","red")
        });
    }
}