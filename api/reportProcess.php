<?php
/**
 * Created by PhpStorm.
 * User: Sourav
 * Date: 7/11/2017
 * Time: 6:35 PM
 */
session_start();
ini_set('memory_limit', '512M');
include("Constants/configuration.php");
include("Constants/dbConfig.php");
include("Constants/functions.php");
require_once "Classes/USERS.php";
require_once "Classes/REPORT.php";

$reportClass = new \Modals\REPORT();
$userClass = new \Modals\USERS();
$requiredFeilds = array("type");
$response = RequiredFields($_REQUEST, $requiredFeilds);
if ($response[Status] != Success) {
    $userClass->apiResponse($response);
    return false;
}

$type = $_POST['type'];

if($type == 'addReport') {

    $requiredFeilds = array("gender","income","martial","age","education","activity","ethnicity","user_id");
    $response = RequiredFields($_REQUEST, $requiredFeilds);
    if ($response[Status] != Success) {
        $userClass->apiResponse($response);
        return false;
    }
    $gender = $_POST['gender'];
    $income = $_POST['income'];
    $martial = $_POST['martial'];
    $education = $_POST['education'];
    $activity = $_POST['activity'];
    $ethnicity = $_POST['ethnicity'];
    $age = $_POST['age'];
    $user_id = $_POST["user_id"];
    $data = array();
    if($gender!='') {
        $data["Gender"]=$gender;
    }
    if($income!='') {
        $data['Income'] = $income;
    }
    if($martial!='') {
//        $data[] = array("report_martial_status"=>$martial);

        $data['MaritalStatus'] = $martial;
    }
    if($activity!='') {
        $data['Activity'] = $activity;
//        $data[] = array("report_activity"=>$activity);
    }
    if($ethnicity!='') {
        $data['EthnicGroup'] = $ethnicity;
//        $data[] = array("report_ethnicity"=>$ethnicity);
    }
    if($education!='') {
        $data['School'] = $education;
//        $data[] = array("report_education"=>$education);
    }
    if($age!='') {
        $data['AgeRange'] = $age;
//        $data[] = array("report_age"=>$age);
    }
    if(isset($_REQUEST['height'])) {
        $data['HeightIn'] = $_REQUEST['height'];
    }
    if(isset($_REQUEST['weight'])) {
        $data['Weight'] = $_REQUEST['weight'];
    }
    $data['report_user_id'] = $user_id;


//    print_r($data);

    $response = $reportClass->insertReportData('sizeusa_report',$data,$user_id);
    $userClass->apiResponse($response);

}

else if($type == 'getReportFields') {
    $requiredFeilds = array("table");
    $response = RequiredFields($_REQUEST, $requiredFeilds);
    if ($response[Status] != Success) {
        $userClass->apiResponse($response);
        return false;
    }
    $table_name = $_POST['table'];
    $response = $reportClass->getReportFields($table_name);
    $userClass->apiResponse($response);
}

else if($type == 'filterReportData') {
    $requiredFeilds = array("data","tableName","userId","selection");
    $response = RequiredFields($_REQUEST, $requiredFeilds);
    if ($response[Status] != Success) {
        $userClass->apiResponse($response);
        return false;
    }
    $data = $_POST['data'];
    $userId = $_POST['userId'];
    $tableName = $_POST['tableName'];
    $selection = $_POST['selection'];
    $page = '';
    if(isset($_POST['page']))
      $page = $_POST['page'];

    $response = $reportClass->getFiterReportData($data,$userId,$tableName,$selection,$page);
    $userClass->apiResponse($response);
}

else if($type == 'updateReportData') {
    $response = array();
    $requiredFeilds = array("userId","tableName","data");
    $response = RequiredFields($_REQUEST, $requiredFeilds);
    if ($response[Status] != Success) {
        $userClass->apiResponse($response);
        return false;
    }

    $userId = $_POST['userId'];
    $tableName = $_POST['tableName'];
    $data = $_POST['data'];
    $selection = $_POST['selection'];

    $_SESSION['userId'] = $userId;
    $_SESSION['tableName'] = $tableName;
    $_SESSION['data'] = $data;
    $_SESSION['selection'] = $selection;

    $response[Status] = "Success";
    $response[Message] = "Data saved successfully";
    $userClass->apiResponse($response);

}
?>