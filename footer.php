<!--<script src="js/jquery.min.js"></script>-->
<script src="http://code.jquery.com/jquery-1.9.1.min.js"></script>
<script src="js/jquery.bootpag.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script type="text/javascript" src="js/bootstrap-datetimepicker.js"></script>
<script src="js/location.js"></script>
<script src="js/jquery.dataTables.min.js"></script>
<script src="js/myscript.js"></script>
<script src="http://code.highcharts.com/js/highcharts.js"></script>
<script src="js/grouped-categories.js"></script>


<footer>
            <div class=" col-md-12 upperfooter">
                <div class="col-md-3 footerblock">
                    <p class="blockheadtext">Telephone</p>
                    <p class="blockvalue"><i class="fa fa-phone"></i> +1 919 380.21.71</p>
                    <p class="blockvalue"><i class="fa fa-fax"></i> +1 919 380.21.81</p>
                </div>
                <div class="col-md-3 footerblock">
                    <p class="blockheadtext">E-Mail</p>
                    <p class="blockvalue"><i class="fa fa-envelope"></i> info@tc2.com</p>
                    <p class="blockvalue"><a href="http://tc2.com"><i class="fa fa-globe"></i> www.tc2.com</a></p>
                </div>
                <div class="col-md-3 footerblock">
                    <p class="blockheadtext">Social</p>
                    <p class="blockvalue">
                        <i style="margin-left:0" class="fa fa-facebook socialicons"></i>
                        <i class="fa fa-google-plus socialicons"></i>
                        <i class="fa fa-twitter socialicons"></i>`
                        <i class="fa fa-youtube socialicons"></i>
                        <i class="fa fa-rss socialicons"></i>
                    </p>
                </div>
                <div class="col-md-3 footerblock" style="border: 0">
                    <p class="blockheadtext">Address</p>
                    <p class="blockvalue"><i class="fa fa-map-marker"></i> TC2 Labs LLC<br>2500 Reliance Avenue<br>
                        Apex NC 27539<br>USA</p>
                </div>
            </div>
            <div class=" col-md-12 lowerfooter">
                Copyright <i class="fa fa-copyright"></i> 2017 Privacy policy
            </div>
        </footer>
    </body>
</html>
<div id ="myModal" class="modal fade" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="gridSystemModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="gridSystemModalLabel">Modal title</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-4">.col-md-4</div>
                    <div class="col-md-4 col-md-offset-4">.col-md-4 .col-md-offset-4</div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div id ="loginModal" class="modal fade" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="gridSystemModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content" style="border-radius:0;">
            <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <div class="row">
                    <div class="col-md-12">
                        <p class="loginheader">Login to SizeUSA</p>
                        <div class="line"></div >
                    </div>
                    <div class="col-md-1"></div>
                    <div class="col-md-10">
                        <div class="col-md-12" style="margin-bottom:30px">
                            <label class="label label-danger">Please Fill all the Required Fields</label>
                            <p id="loginerror" style="text-align:left;color:red;margin-top: 10px"></p>
                        </div>
                        <div class="col-md-12 form-group">
                            <label>UserName / E-Mail</label>
                            <input type="text" placeholder="Enter UserName or E-Mail Address" id='login_uname' class="form-control" />
                        </div>
                        <div class="col-md-12 form-group">
                            <label>Password</label>
                            <input type="password" placeholder="Enter Password" class="form-control" id="login_password" />
                        </div>
                        <div class="col-md-6 form-group">
                            <label>Forgot Password ?</label>
                        </div>
                        <div class="col-md-6 form-group">
                            <input type="button" class="btn btn-danger pull-right" value="Login Now" onclick="loginNow()"/>
                        </div>
                    </div>
                    <div class="col-md-1"></div>
                    <div class="clear"></div>
                    <hr>
                </div>
                <p class="signuplink">New User ? Sign Up <a style="cursor:pointer" onclick="switchmodel('login')">Here</a></p>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div id ="registerModal" class="modal fade" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="gridSystemModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content" style="border-radius:0;">
            <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <div class="row">
                    <div class="col-md-12">
                        <p class="loginheader">Register With SizeUSA</p>
                        <div class="line"></div >
                    </div>
                    <div class="col-md-12" style="margin-bottom:30px">
                        <label class="label label-danger">Please Fill all the Required Fields</label>
                    </div>
                    <p id="registererror" style="text-align: center;color:red"></p>
                    <div class="col-md-12">
                        <div class="col-md-6 form-group">
                            <input type="text" id="fname" placeholder="Enter First Name" class="form-control" />
                        </div>
                        <div class="col-md-6 form-group">
                            <input type="text" id="lname" placeholder="Enter Last Name" class="form-control" />
                        </div>
                        <div class="col-md-6 form-group">
                            <input type="text" id="email" placeholder="Enter E-Mail Address" class="form-control" />
                        </div>
                        <div class="col-md-6 form-group">
                            <input type="password" id="password" placeholder="Enter Password" class="form-control" />
                        </div>
                        <div class="col-md-6 form-group">
                            <select class="form-control countries" id="countryId" onchange="countrydate()" >
                                <option value="" selected="selected">Select Country</option>
                            </select>
                        </div>
                        <div class="col-md-6 form-group">
                            <select class="form-control states" id="stateId">
                                <option value="" selected="selected">Select State</option>
                            </select>
                        </div>
                        <div class="col-md-6 form-group" id="cityfeild">
                            <select class="form-control cities" id="cityId">
                                <option value="" selected="selected">Select City</option>
                            </select>
                        </div>
                        <div class="col-md-6 form-group">
                            <select class="form-control" id="gender">
                                <option>Select Gender</option>
                                <option selected="selected" value="Male">Male</option>
                                <option value="Female">Female</option>
                            </select>
                        </div>
                        <div class="col-md-6 form-group">
                            <input type="text" placeholder="Pick Date of Birth" id="inputdob" class="form-control" />
                        </div>
                        <div class="col-md-6 form-group">
                            <input type="text" id="stylist" placeholder="Enter Stylist Name" class="form-control" />
                        </div>
                        <div class="col-md-6 form-group" style="padding: 0">
                            <div class="col-md-4" style="padding-right:0">
                                <select class="form-control" style="padding:0" id="heightparam" onchange="heightFeild()" >
                                    <option selected="selected" value="Inches" >Inches</option>
                                    <option value="Centimeters" >Centimeters</option>
                                    <option value="Feets" >Feets</option>
                                </select>
                            </div>
                            <div class="col-md-8" id="heightFeilds">
                                <input type="text" id='height' class="form-control" placeholder="Enter Height"/>
                            </div>
                        </div>
                        <div class="col-md-6 form-group" style="padding: 0">
                            <div class="col-md-4" style="padding-right:0">
                                <select class="form-control" style="padding: 0" id="weightparam">
                                    <option selected="selected" value="Pounds">Pounds</option>
                                    <option value="Kilograms">Kilograms</option>
                                    <option value="Stones">Stones</option>
                                </select>
                            </div>
                            <div class="col-md-8">
                                <input type="text" id="weight" class="form-control" placeholder="Enter Weight"/>
                            </div>
                        </div>
                        <div class="col-md-6 form-group">
                            <select class="form-control" id="language" >
                                <option value="English (US)" selected="selected">English (US)</option>
                                <option value="English (US)">English (UK)</option>
                                <option value="French">French</option>
                            </select>
                        </div>
                        <div class="col-md-6 form-group">
                            <input type="file" id="user_profile" class="form-control" />
                        </div>
                        <div class="col-md-6 form-group">
                            <p class="signuplink">Already Register ? Login <a style="cursor:pointer"
                                                                              onclick="switchmodel('register')">Here</a></p>
                        </div>
                        <div class="col-md-6 form-group">
                            <input type="button" onclick="registerNow()" class="btn btn-danger pull-right" value="Register Now" />
                        </div>
                    </div>
                    <div class="col-md-1"></div>
                </div>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div id="customModal" class="modal1">

    <!-- Modal content -->
    <div class="modal1-content">
        <div class="modal1-header">
            <span class="close1" id="close">&times;</span>
            <h3>Subscription</h3>
        </div>
        <div class="modal1-body">
            <div class="row" style="margin-top: 4%;text-align: center;">
            <div class="col-md-12"><label>You have not subscribed for our plan to get subscription please purchase plan </label></div>
                <div class="col-md-12"><label style="color: green"><i>Charges $1000 per year</i></label></div>
            </div>
        </div>
        <div class="modal1-footer">
            <div class="col-md-12" style="margin-top: 5px;text-align: right">
                <button type="button" class="btn btn-default" onclick="onBuyClicked()"><i class="fa fa-credit-card" aria-hidden="true"></i> Buy</button>
                <button type="button" class="btn btn-danger" onclick="onPopUpClosed()"><i class="fa fa-close"></i> Close</button>
            </div>
        </div>
    </div>

</div>

<style>
    #chart-container-license-text{
        opacity: 0;
    }
</style>

