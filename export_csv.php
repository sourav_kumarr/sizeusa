<?php
/* vars for export */
// database record to be exported
session_start();
ini_set('memory_limit','128M');
header("Content-type: text/csv");
header("Content-Disposition: attachment; filename=data.csv");
header("Pragma: no-cache");
header("Expires: 0");

include("api/Constants/configuration.php");
include("api/Constants/dbConfig.php");
include("api/Constants/functions.php");
require_once "api/Classes/USERS.php";
require_once "api/Classes/REPORT.php";

$connect = new \Modals\CONNECT();
$reportClass = new \Modals\REPORT();
$SQL = $_SESSION['sql'];
//echo 'before '.$SQL;

$sql = substr($SQL,0,strpos($SQL,"limit")-1);
//echo 'after '.$sql;

$link = $connect->Connect();
$exportData = mysqli_query ($link,$sql ) or die ( "Sql error : " . $connect->sqlError() );

$fields = mysqli_num_fields($exportData);
$fields_data = array();
for ( $i = 0; $i < $fields; $i++ )
{
    $fields_data[] = mysqli_fetch_field_direct($exportData,$i)->name;
}
$fields_data = implode(',',$fields_data);
$lists = array();

while( $row = mysqli_fetch_array( $exportData ) )
{
    $row_item = array();
    for ( $i = 0; $i < $fields; $i++ )
    {
        $item = $row[mysqli_fetch_field_direct($exportData,$i)->name];

        $row_item[] = $item;
    }
    $lists[] = $row_item;

}
//header
echo  $fields_data."\n";

foreach ($lists as $list) {
    echo implode(',', $list);
    echo PHP_EOL;
}
?>