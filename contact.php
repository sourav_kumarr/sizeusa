<?php
include_once 'header.php';
?>
<style>
    .addressheading {
        color: white;
        font-family: calibri;
        font-size: 45px;
        font-weight: normal;
    }
    .tr {
        color: #666;
        font-family: Trebuchet MS;
        letter-spacing: 1px;
        line-height: 40px;
    }
    .comtactpageupperpart{
        border-bottom: 2px solid #2095f2;
        padding: 50px 0;
    }
    td, th{
        color:white;
        font-family: Calibri;
    }
</style>
<div class="col-md-12 heritageheader" style="margin-bottom:0px;height:645px;">
    <video autoplay loop style="width: 100%">
        <Source src="images/contactmovie.mp4" />
    </video>
    <div class="videotopdiv">
           <div class="container">
	
        <div class="col-md-10" style="position: absolute; margin-top: 100px;">
            <div class="col-md-4">
                <p class="addressheading">Contact Us</p>
                <label class="headingafternumber">TC2 Labs LLC<br>
                    2500 Reliance Avenue<br>
                    Apex NC 27539<br>
                    USA</label>
                <table>
                    <tr class="tr">
                        <th>Phone : </th><td>  +1 919 380 2171</td>
                    </tr>
                    <tr class="tr">
                        <th>Fax : </th><td>  +1 919 380 2171</td>
                    </tr>
                    <tr class="tr">
                        <th>E-Mail : </th><td>  info@tc2.com</td>
                    </tr>
                    <tr class="tr">
                        <th>Website :- </th>
                        <td>
                            <a href="http://www.tc2.com">www.tc2.com</a> <a href="http://www.sizeusa.com">www.sizeusa.com</a>
                        </td>
                    </tr>
                </table>
            </div>
            <div class="col-md-8" style="background: rgba(255, 255, 255, 0.3) none repeat scroll 0 0;border-radius: 10px;
                padding: 20px;">
                <div class="col-md-12" style="padding: 0">
                    <div class="form-group col-md-4">
                        <label>Name</label>
                        <input type="text" class="form-control" value="" placeholder="Enter Your Name" />
                    </div>
                    <div class="form-group col-md-4">
                        <label>E-Mail</label>
                        <input type="text" class="form-control" value="" placeholder="Enter Your E-Mail Address" />
                    </div>
                    <div class="form-group col-md-4">
                        <label>Phone</label>
                        <input type="text" class="form-control" value="" placeholder="Enter Your Phone" />
                    </div>
                    <div class="form-group col-md-12">
                        <label>Phone</label>
                        <textarea class="form-control" rows="8" placeholder="Enter Your Message" ></textarea>
                    </div>
                    <div class="col-md-12">
                        <button class="btn btn-info pull-right" style="background:#2095F2;width:200px;border:0" > Contact</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
</div>

<div class="centercontent">
 
    <div class="clear"></div>
    <div class="col-md-12 map">
        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d18357.066337464847!2d-78.82964216491493!3d35.72039243365781!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x89ac8d693ecb65ff%3A0x3389483c9fac0044!2s2500+Reliance+Ave%2C+Apex%2C+NC+27539%2C+USA!5e0!3m2!1sen!2sin!4v1497514280599" width="100%" height="350" frameborder="0" style="border:0" allowfullscreen></iframe>
    </div>
</div>
<div class="clear"></div>
<hr>
<?php
include_once 'footer.php';
?>
<script>
    $(".menuitems").removeClass("activemenuitem");
    $("#contact").addClass("activemenuitem");
</script>