/**
 * Created by Sourav on 7/12/2017.
 */

var idd = '';
window.onload = function () {
  loadData('sizeusa_primary','1b');
};

function loadData(tableName,id) {
    idd = id;
    $('#select_all').prop('checked',false);
   var url = 'api/reportProcess.php';
   $.post(url,{type:'getReportFields','table':tableName},function (data){
      var status = data.Status;
      var message = data.Message;
      var display = '<div class="col-md-12 boxes"> <div class="col-md-3"><div class="checkbox"><label><input type="checkbox" value="0"  id="select_all_'+id+'" onchange=selectedItems(this,"'+id+'")>Select All</label>'+
       '</div></div></div><div class="col-md-12 boxes">';
      // alert(status);
      if(status == 'Success') {
         var columns = data.data;
         for(var i=2;i<columns.length;i++) {

              display = display+'<div class="col-md-3"><div class="checkbox "><label><input type="checkbox" name="check_items_'+id+'" class="checkbox_'+id+'" id='+i+'  onchange=selectedItems(this,"'+id+'") value='+columns[i]+'>'+columns[i]+'</label>'+
             '</div></div>';
         }
         $('#'+id).html(display+'</div>');
      }
      else if(status == 'Failure'){
          $("#"+id).html('<label style="color:red">'+message+'</label>');
      }
   });
}

 function selectedItems(ref,id) {
        console.log('id -- '+ref.id);
        if(ref.id =='select_all_'+id) {
            $(".checkbox_"+id).prop('checked', $('#select_all_'+id).prop('checked'));

        }
        else{
            $('#select_all').prop('checked',false);
        }
     $("#message").html("");
 }
 
 function onCreateReport() {

     var checked_items = [];
     var user_id = $('#user_id').val();
     console.log('create report is called -- ');

     $.each($("input[name='check_items_"+idd+"']:checked"), function(){
         checked_items.push($(this).val());
     });

     if(checked_items.length>0) {
         checked_items = checked_items.join(',');
         var tableName ='sizeusa_primary';
         if(idd == '1b') {
             tableName = 'sizeusa_primary';
         }
         else if(idd == '2b') {
             tableName = 'sizeusa_twins';
         }
         else if(idd == '3b') {
             tableName = 'sizeusa_secondry';
         }
         var url = 'api/reportProcess.php';

         var select_all =  $('#select_all_'+idd).prop('checked')?'selectAll':'na';

         if(checked_items.indexOf("Gender") < 0) {
             checked_items=checked_items+",Gender";
         }
         // alert(select_all);
         $.post(url,{'type':'updateReportData','userId':user_id,'tableName':tableName,'data':checked_items,'selection':select_all},function(data){
           var status = data.Status;
           if(status == 'Success') {
               window.location = 'results.php';
           }
           else if(status == 'Failure') {
               $('#message').html(status.Message);
               $('#message').css('color','red');
           }
         })

     }
     else{
         $("#message").css("color","red");
         $("#message").html("No items got selected ...");
     }

     console.log(checked_items);
 }