<?php
/**
 * Created by PhpStorm.
 * User: Sourav
 * Date: 7/11/2017
 * Time: 6:36 PM
 */

namespace Modals;


class REPORT
{
    private $link = null;
    private $link2 = null;
    public $CurrentDateTime = null;
    public $item_per_page = 10;
    function __construct() {

        $this->link = new CONNECT();
        $this->link2 = new CONNECT();
        $this->CurrentDateTime = date("Y-m-d h:i:s");
    }

    function insertReportData($table,$data,$user_id) {
       $link = $this->link->Connect();
       $userClass = new USERS();
       if($userClass->isSubscribed($user_id)) {
           $query = $this->link->insertQuery($table, $data);
//      echo $query;
           if ($link) {
               $result = mysqli_query($link, $query);
               if ($result) {
                   $id_ = $this->link->lastId();
                   if ($id_ > 0) {
                       $this->link->response[Status] = Success;
                       $this->link->response[Code] = 200;
                       $this->link->response[Message] = "Report generated successfully";
                       $this->link->response["data"] = $id_;
                       $_SESSION['is_subscribed'] = true;
                   }
               } else {
                   $this->link->response[Status] = Error;
                   $this->link->response[Code] = 501;
                   $this->link->response[Message] = $this->link->sqlError();
               }
           } else {
               $this->link->response[Status] = Error;
               $this->link->response[Code] = 501;
               $this->link->response[Message] = $this->link->sqlError();
           }
       }
       else{
           $this->link->response[Status] = Error;
           $this->link->response[Code] = 500;
           $this->link->response[Message] = "User is not subscribed for plan";
           $_SESSION['is_subscribed'] = false;

       }
      return $this->link->response;
    }

    function getReportFields($tableName) {
        $link = $this->link->Connect();
        if($link) {
            $sql = 'SHOW COLUMNS FROM '.$tableName;
            $res = mysqli_query($link,$sql);
            if($res) {
                $columns = array();
                while ($row = $res->fetch_assoc()) {
                    $columns[] = $row['Field'];
                }
                if(count($columns)>0) {
                    $this->link->response[Status] = Success;
                    $this->link->response["data"] = $columns;
                    $this->link->response[Message] = "Got data successfully";

                }
            }
            else{
                $this->link->response[Status] = Error;
                $this->link->response[Message] = $this->link->sqlError();
            }
        }
        return $this->link->response;
    }

    function getFiterReportData($data,$userId,$tableName,$selection,$page) {
        $tot = 0;
        $male_count = 0;
        $female_count = 0;

        $link = $this->link->Connect();
        if($link) {
          $response = $this->getReportFields('sizeusa_primary');
          $fields = array();
          if($response[Status] == Success) {
            $arr = $response["data"];
//            $data = explode(',',$data);
            $temp = '';
            $temp1='';
              $sql = "select * from sizeusa_report where report_user_id='$userId' order by report_id desc limit 0,1";
//                    echo $sql;
              $res = mysqli_query($link,$sql);

                    $num = mysqli_num_rows($res);
//                    $fields[] = $field_name;
                    if($num>0) {
                        $fieldss = mysqli_num_fields($res);
                        $rows = mysqli_fetch_array($res);

//                        echo $fieldss.' ';
                        for ( $j = 0; $j<$fieldss; $j++ ) {

                            $field_namee =  mysqli_fetch_field_direct($res,$j)->name;
//                            echo $field_namee;
                            $items = $rows[$field_namee];

                            if($field_namee!='report_id' && $field_namee!='report_updated_at' && $field_namee!='report_user_id' && $this->isFieldExists($arr,$field_namee)) {
                                if ($items != '0') {
                                    $temp1 = '(';
                                    $fields[] = $field_namee;
                                    $items = explode(',', $items);


                                    for ($k = 0; $k < count($items); $k++) {

                                            if ($k == count($items) - 1) {
                                                $temp1 = $temp1 . $field_namee . '=' . "'$items[$k]')";
                                            } else {
                                                $temp1 = $temp1 . $field_namee . '=' . "'$items[$k]' or ";
                                            }

                                    }
                                    if ($j == $fieldss - 1) {
                                        $temp = $temp . $temp1;
                                    } else if ($temp1 != '') {

                                        $temp = $temp . $temp1 . ' and ';
                                    }
                                } else {
                                    $temp1 = '';
                                }
                            }

//                            $temp = $temp . $temp1 . ' or ';

                        }

              }
              $up_query = '';
              $keys = array();
              if($tableName =='sizeusa_secondry' || $tableName =='sizeusa_twins' ) {

              $sql_ = 'select Survey_ID,Gender from sizeusa_primary where '.$temp;

              $temp = '';
              $sql_ = rtrim($sql_,' and ');
//               echo $sql_;
              $res_ = mysqli_query($link,$sql_);
              $counter = 0;
              while($rows = mysqli_fetch_array($res_)) {
                  if($tableName == 'sizeusa_twins') {
                      $temp = $temp."Survey_ID='".$rows['Survey_ID']."_KX16' or ";
                      /*if($counter>10000 && $counter<=10610){
                          $up_query = $up_query."WHEN `Survey_ID` = '".$rows['Survey_ID']."_KX16' THEN '".$rows['Gender']."' ";
                          $survey_id = $rows['Survey_ID'].'_KX16';
                          $keys []= "'$survey_id'";

                      }*/

                  }
                  else{
                      $temp = $temp."Survey_ID='".$rows['Survey_ID']."' or ";
                  }
                  rtrim($temp,' or ');
                 }

              }
              $up_ = 'UPDATE sizeusa_twins SET `Gender` = CASE '.$up_query.'ELSE Survey_ID END WHERE `Survey_ID` IN ('.implode(',',$keys).')';
//              echo $up_;
              $sql = 'select Survey_ID,'.$data.' from '.$tableName.' where '.$temp;
              if($tableName !='sizeusa_primary'){
                  $sql = rtrim($sql,' or ');
              }
              else{
                  $sql = rtrim($sql,' and ');
              }
//              $sql = $sql.' limit 0,1500';

//              echo $up_query;

//              $_SESSION['sql'] = $sql;

              $tot_query =  'select Survey_ID,'.$data.' from '.$tableName.' where '.$temp;

              $tot_res = mysqli_query($link,$tot_query);
              if($tot_res)
                $tot = mysqli_num_rows($tot_res);

              if($page!='') {
                  $page = intval($page);
                  $page_number = filter_var($page, FILTER_SANITIZE_NUMBER_INT, FILTER_FLAG_STRIP_HIGH);

                  $position = (($page_number-1) * $this->item_per_page);
                  $sql = $sql.' limit '. $position.','. $this->item_per_page;
              }
              else{
                  $sql = $sql.' limit 0,2000';
              }
//               echo $sql;

              $result = mysqli_query($link,$sql);
              if($result) {
                $num = mysqli_num_rows($result);
                if($num>0) {
                    $temp = rtrim($temp,' and ');
                    $temp = rtrim($temp,' or ');
                    $tot_query =  "select count(*) as total,(select count(*) from ".$tableName." where ".$temp." and Gender='Male') as maleCount,(select count(*) from ".$tableName." where ".$temp." and Gender='Female') as femaleCount, Gender from ".$tableName." where ".$temp;
//                        echo $tot_query;
                    $tot_res = mysqli_query($link,$tot_query);
                    if($tot_res) {
                        $tot = mysqli_fetch_array($tot_res)['total'];

                    }

                    $_SESSION['sql'] = $sql;
//                    echo $sql;
                    $response1 = array();
                    $counter = 0;
                    $tot_query =  'select Survey_ID,'.$data.' from '.$tableName.' where '.$temp;
                    $tot_res = mysqli_query($link,$tot_query);
                    if($tot_res){
                        $tot_rows = mysqli_fetch_array($tot_res);
                        $tot = $tot_rows['total'];
                        $male_count = $tot_rows['maleCount'];
                        $female_count = $tot_rows['femaleCount'];
                    }

                    while($rows = mysqli_fetch_array($result)) {
//                        if(!key_exists('_id',$rows))
                           $fieldss = mysqli_num_fields($result);
                           $keys = array();
                           $values = array();
                           for ( $i = 0; $i < $fieldss; $i++ )
                           {
                            $field_name = mysqli_fetch_field_direct($result,$i)->name;
                            $item = $rows[$field_name];
                             $keys[] = $field_name;
                             $values []= $item;
                           }
                             $response1[] = array_combine($keys,$values);
                             $counter++;
                    }

//                    print_r($response1);

                    $this->link->response[Status] = Success;
                    $this->link->response[Message] = "Got data successfully";
                    $this->link->response["data"] = $response1;
                    $this->link->response["total"] = $tot;
                    $this->link->response["maleCount"] = $male_count;
                    $this->link->response["femaleCount"] = $female_count;

                    return $this->link->response;


                }
                else{
                    return $this->rebuildQuery($data,$userId,$tableName,$fields,$page);
                }

            }
            else{
                return $this->rebuildQuery($data,$userId,$tableName,$fields,$page);
            }
          }
        }
        else{
            $this->link->response[Status] = Error;
            $this->link->response[Message] = $this->link->sqlError();
            return $this->link->response;
        }

    }


    function rebuildQuery($data,$userId,$tableName,$fieldss,$page) {
        $tot = 0;
        $male_count = 0;
        $female_count = 0;

        $link = $this->link->Connect();
        if($link) {
            $response = $this->getReportFields('sizeusa_primary');
            $fields = array();
            if($response[Status] == Success) {
                $arr = $response["data"];
//            $data = explode(',',$data);
                $temp = '';
                $temp1='';
                $sql = "select * from sizeusa_report where report_user_id='$userId' order by report_id desc limit 0,1";
//                    echo $sql;
                $res = mysqli_query($link,$sql);

                $num = mysqli_num_rows($res);
//                    $fields[] = $field_name;
                if($num>0) {
//                    $fieldss = mysqli_num_fields($res);
                    $rows = mysqli_fetch_array($res);

//                        echo $fieldss.' ';
                    for ( $j = 0; $j<count($fieldss); $j++ ) {

                        $field_namee =  $fieldss[$j];
//                            echo $field_namee;
                        $items = $rows[$field_namee];

                        if($field_namee!='report_id' && $field_namee!='report_updated_at' && $field_namee!='report_user_id' && $this->isFieldExists($arr,$field_namee)) {
                            if ($items != '0') {
                                $temp1 = '(';

                                $items = explode(',', $items);


                                for ($k = 0; $k < count($items); $k++) {

                                    if ($k == count($items) - 1) {
                                        $temp1 = $temp1 . $field_namee . '=' . "'$items[$k]')";
                                    } else {
                                        $temp1 = $temp1 . $field_namee . '=' . "'$items[$k]' or ";
                                    }

                                }
                                if ($j == count($fieldss )- 1) {
                                    $temp = $temp . $temp1;
                                } else if ($temp1 != '') {
                                        $temp = $temp . $temp1 . ' and ';
                                }
                            } else {
                                $temp1 = '';
                            }
                        }

                    }

                }

                if($tableName =='sizeusa_secondry' || $tableName =='sizeusa_twins' ) {
//              echo $temp;
                    $sql_ = 'select Survey_ID from sizeusa_primary where '.$temp;
                    $temp = '';
                    $sql_ = rtrim($sql_,' and ');
//              echo $temp;

                    $res_ = mysqli_query($link,$sql_);


                    while($rows = mysqli_fetch_array($res_)) {
                        if($tableName == 'sizeusa_twins') {
                            $temp = $temp."Survey_ID='".$rows['Survey_ID']."_KX16' or ";
                        }
                        else{
                            $temp = $temp."Survey_ID='".$rows['Survey_ID']."' or ";
                        }
                        rtrim($temp,' or ');
                    }

                }
                $sql = 'select Survey_ID,'.$data.' from '.$tableName.' where '.$temp;
                $sql = rtrim($sql,' or ');
                $sql = rtrim($sql,' and ');
//                $sql = $sql.' limit 0,1500';
//                echo $sql;
//                echo $page;

                if($page!='') {
                    $page = intval($page);
                    $page_number = filter_var($page, FILTER_SANITIZE_NUMBER_INT, FILTER_FLAG_STRIP_HIGH);

                        $position = (($page_number-1) * $this->item_per_page);
                        $sql = $sql.' limit '. $position.','. $this->item_per_page;


                }
                else{
                    $sql = $sql.' limit 0,2000';
                }
//                echo $sql;
                $result = mysqli_query($link,$sql);

                if($result) {
                    $num = mysqli_num_rows($result);
                    if($num>0) {
                        $temp = rtrim($temp,' and ');
                        $temp = rtrim($temp,' or ');
                        $tot_query =  "select count(*) as total,(select count(*) from ".$tableName." where ".$temp." and Gender='Male') as maleCount,(select count(*) from ".$tableName." where ".$temp." and Gender='Female') as femaleCount, Gender from ".$tableName." where ".$temp;
//                        echo $tot_query;
                        $tot_res = mysqli_query($link,$tot_query);
                        if($tot_res){
                            $tot_rows = mysqli_fetch_array($tot_res);
                            $tot = $tot_rows['total'];
                            $male_count = $tot_rows['maleCount'];
                            $female_count = $tot_rows['femaleCount'];
                        }


                        $_SESSION['sql'] = $sql;
                        $response1 = array();
                        $counter = 0;
                        while($rows = mysqli_fetch_array($result)) {
//                        if(!key_exists('_id',$rows))
                            $fieldss = mysqli_num_fields($result);
                            $keys = array();
                            $values = array();
                            for ( $i = 0; $i < $fieldss; $i++ )
                            {
                                $field_name = mysqli_fetch_field_direct($result,$i)->name;
                                $item = $rows[$field_name];
                                $keys[] = $field_name;
                                $values []= $item;
                            }
                            $response1[] = array_combine($keys,$values);
                            $counter++;
                        }

                        $this->link->response[Status] = Success;
                        $this->link->response[Message] = "Got data successfully";
                        $this->link->response["data"] = $response1;
                        $this->link->response["total"] = $tot;
                        $this->link->response["maleCount"] = $male_count;
                        $this->link->response["femaleCount"] = $female_count;


                    }
                    else{
                        /*$this->link->response[Status] = Error;
                        $this->link->response[Message] = "No searched records found yet";*/
                        array_pop($fieldss);
                        return $this->rebuildQuery($data,$userId,$tableName,$fieldss,$page);
                    }

                }
                else{
                    if(count($fieldss) == 1) {
                        $this->link->response[Status] = Error;
                        $this->link->response[Message] = "Unable to get data";
                    }
                    else{
                        array_pop($fieldss);
                        return $this->rebuildQuery($data,$userId,$tableName,$fieldss,$page);
                    }
                }
            }
        }
        else{
            $this->link->response[Status] = Error;
            $this->link->response[Message] = $this->link->sqlError();
        }
        return $this->link->response;
    }


    function isFieldExists($arr,$item) {
        $isFound = false;

        for($i=0;$i<count($arr);$i++) {
            if($arr[$i] == $item) {
                $isFound = true;
                break;
            }
        }
        return $isFound;
    }
}