/**
 * Created by Sourav on 7/13/2017.
 */

var primary = ['id_','Survey_ID','ZipCode','Gender','AgeRange','Weight','ScanTop','ScanBottom','Location','EthnicCategory',
    'EthnicGroup','Income','MaritalStatus','WeightDescription','Activity','School','Employment','ClothingSize','Stores','ClothingTypes'];

var secondry = ['id_','Survey_ID','Gender','AgeRange'];
var image_twin = ['id_','Survey_ID','Gender'];
var items = [];
var total = 0;
var maleCount = 0;
var femaleCount = 0;

window.onload = function(){
    loadData();

};

function loadData() {
    var userId = $('#user_id').val();
    var table_name = $('#table_name').val();
    var data = $('#data').val();
    var selection = $('#selection').val();

    if(table_name == 'sizeusa_twins') {
        $("#length_type").val("cm");
    }
    var length_type = $("#length_type").val();

    if(userId!='') {
      var url = 'api/reportProcess.php';
      $.post(url,{'type':'filterReportData','userId':userId,'tableName':table_name,'data':data,'selection':selection},function(data){
          $('#overlay').css('display','none');
          var status = data.Status;
          items = data.data;
         total = data.total;
         maleCount = data.maleCount;
         femaleCount = data.femaleCount;

          console.log(status);
         if(status == 'Success') {
           var th_ ='<thead><tr>';
           var td_='<tbody>';
           for(var i=0;i<items.length;i++) {
               var obj = items[i];

               if(obj) {
                   td_ = td_ + '<tr>';
                   for (var j in obj) {
                       if (i == 0) {
                           th_ = th_ + '<th>' + j + '</th>';
                       }
                       // console.log('col val -- '+obj[j]+'index -- '+i);
                       var col_value = obj[j];

                       if(table_name == 'sizeusa_primary') { // first table data
                           if(!findItem(primary,j)){
                               if(length_type == 'cm') {
                                   col_value = parseFloat(col_value).toFixed(2)+' cm';
                                   // col_value.toFixed(2);

                               }
                               else{
                                   col_value = parseFloat(col_value).toFixed(2)+' inch';
                               }

                           }

                       }

                       else if(table_name == 'sizeusa_secondry') { // first table data
                           if(!findItem(secondry,j)){
                               if(length_type == 'cm') {
                                   // col_value = parseFloat(col_value)*2.54; // inch to cm formula
                                   col_value = parseFloat(col_value).toFixed(2)+' cm';
                               }
                               else{
                                   col_value = parseFloat(col_value).toFixed(2)+' inch';
                               }
                           }

                       }

                       else { // first table data
                           if(!findItem(image_twin,j)) {
                               if(length_type == 'inch') {
                                   col_value = parseFloat(col_value).toFixed(2)+' inch';
                               }
                               else{
                                   col_value = parseFloat(col_value).toFixed(2)+' cm';
                               }

                           }

                       }

                       td_ = td_ + '<td>' + col_value + '</td>';
                   }

                   td_ = td_ + '</tr>';
               }

           }


           th_ = th_+'</tr></thead>';
           td_ = td_+'</tbody>';
           $('#table_data').html(th_+td_);
           $('#table_data').dataTable({"order": [],"destroy":true});
           $("#total").html('Total : '+(parseInt(total)*32000).toLocaleString());
             /* $(".pagination").bootpag({
                 total: data.total,
                 page: 1,
                 maxVisible: 10
               }).on("page", function(e, num){
                 e.preventDefault();
                 // $("#results").prepend('<div class="loading-indication"><img src="ajax-loader.gif" /> Loading...</div>');
                 loadData(num);
             });*/
           // $('.dataTables_filter').css('display','none');

         }
         else if(status == 'Failure') {
             $('#table_data').html('<label style="color:red">Sorry data not found</label>');
         }
      });
    }
    else{
        $('#table_data').html('<label style="color:red">Sorry parameter not found</label>');
    }

}

function onPrintReport() {
    // var print_data = $('#print_data').val();
    window.location = 'export_csv.php';
}

function filterReport() {
 var table_name = $('#table_name').val();
 var length_type = $("#length_type").val();
 console.log('len type -- '+length_type);
 $("#overlay").css("display","block");
    var th_ ='<thead><tr>';
    var td_='<tbody>';
    for(var i=0;i<items.length;i++) {
        var obj = items[i];
        if(obj) {
            td_ = td_ + '<tr>';
            for (var j in obj) {
                if (i == 0) {
                    th_ = th_ + '<th>' + j + '</th>';
                }

                // console.log('j val -- '+j);
                var col_value = obj[j];

                if(table_name == 'sizeusa_primary') { // first table data
                    if(!findItem(primary,j)){
                        if(length_type == 'cm') {
                            // col_value = parseFloat(col_value);

                                // console.log('col val -- '+col_value+'index -- '+i);

                            col_value = parseInt(col_value)*2.54; // inch to cm formula
                            col_value = col_value.toFixed(2)+' cm';
                            // col_value.toFixed(2);

                        }
                        else{
                            col_value = col_value.toFixed(2)+' inch';
                        }

                    }

                }

               else if(table_name == 'sizeusa_secondry') { // first table data
                    if(!findItem(secondry,j)){
                        if(length_type == 'cm') {
                            col_value = parseFloat(col_value)*2.54; // inch to cm formula
                            col_value = col_value.toFixed(2)+' cm';
                        }
                        else{
                            col_value = parseFloat(col_value).toFixed(2)+' inch';
                        }
                    }

                }

                else { // first table data
                    if(!findItem(image_twin,j)) {
                        if(length_type == 'inch') {
                            col_value = parseFloat(col_value)*0.39; // cm to inch formula
                            col_value = col_value.toFixed(2)+' inch';
                        }
                        else{
                            col_value = parseFloat(col_value).toFixed(2)+' cm';
                        }

                    }

                }

                td_ = td_ + '<td>' + col_value + '</td>';
            }

            td_ = td_ + '</tr>';
        }

    }


    th_ = th_+'</tr></thead>';
    td_ = td_+'</tbody>';
    setTimeout(function () {
        $("#overlay").css("display","none");
        $('#table_data').html(th_+td_);
        $('#table_data').dataTable({"order": [],"destroy":true});
        $("#total").html('Total : '+(parseInt(total*32000)).toLocaleString());

    },2000);

}

function findItem(arr,item) {
    var isFound = false;
    for(var i=0;i<arr.length;i++) {
        if(arr[i] == item) {
            isFound = true;
            break;
        }
    }
    return isFound;
}

function onLevelLoaded(step) {
    $(".results").removeClass("active");
    $(".results.results-"+step).addClass("active");
}

function onDataLoaded() {

    // alert('onDataLoaded()');
    var table_name = $("#table_name").val();
    var th_ ='<thead><tr ><th>#</th><th>Name</th></tr></thead>';
    var td_='<tbody>';
    $("#total1").html("<label>Total : </label>"+(parseInt(total*32000).toLocaleString()));
    var counter = 0;
    var isItemFound = false;
    for(var i=0;i<items.length;i++) {
        var obj = items[i];
        if (obj) {

            counter = 1;
            for (var j in obj) {
                if (i == 0) {

                    var col_value = obj[j];
                    if(table_name == 'sizeusa_primary') { // first table data
                        if(!findItem(primary,j)) {

                                if(counter == 1)
                                td_ = td_ + '<tr class="active" style="cursor: pointer" onclick=printGraph("' + j + '") id="'+j+'">';
                                else
                                    td_ = td_ + '<tr style="cursor: pointer" onclick=printGraph("' + j + '") id="'+j+'">';
                                td_ = td_ + '<td>' + counter + '</td><td><span >' + j + '</span></td>';
                                if (counter == 1) {
                                    printGraph(j);
                                }
                                counter = counter+1;
                            isItemFound = true;

                        }
                    }
                    else if(table_name == 'sizeusa_secondry') { // first table data
                        if(!findItem(secondry,j)){

                                if(counter == 1)
                                td_ = td_ + '<tr class="active" style="cursor: pointer" onclick=printGraph("' + j + '") id="'+j+'">';
                               else
                                td_ = td_ + '<tr style="cursor: pointer" onclick=printGraph("' + j + '") id="'+j+'">';
                                td_ = td_ + '<td>' + counter + '</td><td style="cursor: pointer" onclick=printGraph("' + j + '")><span>' + j + '</span></td>';
                                if (counter == 1) {
                                    printGraph(j);
                                }
                            counter = counter+1;
                            isItemFound = true;
                        }
                    }
                    else { // first table data
                        if(!findItem(image_twin,j)) {

                                if(counter == 1)
                                td_ = td_ + '<tr class="active" style="cursor: pointer" onclick=printGraph("' + j + '") id="'+j+'">';
                                else
                                td_ = td_ + '<tr style="cursor: pointer" onclick=printGraph("' + j + '") id="'+j+'">';
                                td_ = td_ + '<td>' + counter + '</td><td style="cursor: pointer" onclick=printGraph("' + j + '")><span >' + j + '</span></td>';
                                if (counter == 1) {
                                    printGraph(j);
                                }
                            counter = counter+1;
                            isItemFound = true;
                        }
                    }

                }

                // console.log('j val -- '+j);

                td_ = td_ + '</tr>';
            }
        }
    }

    td_ = td_+'</tbody>';
    $('#result-data').html(th_+td_);
    if(!isItemFound) {
        alert("Appropirate data is not found graph can not be drawn");
        onLevelLoaded(1);
    }
    // alert(isItemFound);
    // $('#result-data').dataTable({"destroy":true});

}

function printGraph(key) {
       $('#measure-val').on('change',function () {
           printGraph(key);
       });

        var data = findData(key);
        if(data.series.Male.length == 0 && data.series.Female.length == 0 ) {
            $("#chart-container").html("<label style='color: red'>Sorry appropirate data is not found try again with Male or Female search</label>")
            $("#selected-item").html('');
            return false;
        }
       // data.category.push("");
       // data.series.Male.push(null);
       // data.series.Female.push(null);
        data = filterData(data);
        console.log('data--'+JSON.stringify(data));
       /* var myConfig = {

           "graphset":[
            {
                "type":"pop-pyramid",
                "legend":{
                    "layout":"1x1", //row x column
                    "x":"25%",
                    "y":"5%"
                },
                "plot":{
                    "stacked":true,
                    "bar-width": "25px",
                    marginBottomOffset: 2,
                    marginLeftOffset: 2,
                    marginRightOffset: 2,
                    marginTopOffset: 2
                },

                "scale-x": {
                    "item":{
                        "font-size":10
                    },
                    "values":data.category,
                    "zooming": true,
                    "zoom-to": [0, 20]
                },
                "scroll-x":{
                    "bar":{
                        "background-color":"#8CF226",
                        "alpha":0.3,
                        "width":10
                    },
                    "handle":{
                        "background-color":"#8CF226"
                    }
                },
                "series":[
                    {
                        "data-side":1,
                        "background-color":"#00A4DC",
                        "values":data.series.Male,
                        "text":"Male",
                        "font-size":17
                    },
                    {
                        "data-side":2,
                        "background-color":"#f47b5d",
                        "values":data.series.Female,
                        "text":"Female"

                    }
                ]
            }
        ]
    };

    zingchart.render({
        id : 'chart-container',
        data : myConfig,
        height: 530,
        width: "100%"
    });*/

    /*var categories = ['0-4', '5-9', '10-14', '15-19',
        '20-24', '25-29', '30-34', '35-39', '40-44',
        '45-49', '50-54', '55-59', '60-64', '65-69',
        '70-74', '75-79', '80-84', '85-89', '90-94',
        '95-99', '100 + '];*/

        var series = [];
        var category = [];
        if(data.series.Male.length>0) {
            series.push({'name':'Male',data:data.MaleCategory});
            category.push({
                categories: data.series.MaleGroup,
                reversed: false,
                labels: {
                    step: 1
                }
            });
        }
        if(data.series.Female.length>0) {
            series.push({'name':'Female',data:data.FemaleCategory});
            category.push({
                opposite: true,
                reversed: false,
                categories: data.series.FemaleGroup,
                linkedTo: 0,
                labels: {
                    step: 1
                }
            });
        }
        for(var j=0;j<category.length;j++)
        console.log('category --'+JSON.stringify(category[j]));

        Highcharts.chart('chart', {
            chart: {
                type: 'bar'
            },
            title: {
                text: 'Population pyramid for SizeUSA, 2017'
            },
            subtitle: {
                text: 'Source: <a href="http://scan2fit.com/sizeusa">SizeUSA</a>'
            },
            xAxis: category,
            yAxis: {
                title: {
                    text: null
                },
                labels: {
                    formatter: function () {
                        return Math.abs(this.value);
                    }
                }
            },

            plotOptions: {
                series: {
                    stacking: 'normal'
                }
            },

            tooltip: {
                formatter: function () {
                    return '<b>' + this.series.name + ' '+$("#selected-item").text()+' '+ this.point.category + '</b><br/>' +
                        $("#selected-item").text()+': ' + Highcharts.numberFormat(Math.abs(this.point.y), 0);
                }
            },
            credits: {
                enabled: false
            },
            series: series
        });



    $('#result-data>tbody>tr').removeClass('active');
   $('#'+key).addClass('active');
}

function findData(key) {
    var table_name = $('#table_name').val();
    var mesaurement_val = $('#measure-val').val();
    // var data = {"dataset":[],"category":[],"x_axis":"Counter","y_axis":""};
    var data = {"series":{"Male":[],"Female":[],"MaleGroup":[],"FemaleGroup":[]},"MaleCategory":[],"FemaleCategory":[]};
    var counter = 1;

    if(table_name == 'sizeusa_primary' || table_name =='sizeusa_secondry') {
        data.y_axis = "Units in (Inches)";
    }
    else {
        data.y_axis = "Units in (CM)";
    }
    for(var i=0;i<items.length;i++) {
        var obj = items[i];
        if (obj) {


            for (var j in obj) {


                    var col_value = obj[j];
                    if(table_name == 'sizeusa_primary') { // first table data
                        var gender = '';
                        var genderObj = {};
                        if(!findItem(primary,j) && j == key) {
                            $("#selected-item").html(key);
                            genderObj = findGender(obj);
                            // console.log('genderObj -- '+JSON.stringify(genderObj));
                            if(genderObj.isGender) {
                                if(genderObj.value == 'Male') {
                                    if(mesaurement_val == 'cm') {
                                        col_value = col_value/0.39;
                                    }
                                    data.series.Male.push(col_value);
                                }
                                else if(genderObj.value == 'Female') {
                                    if(mesaurement_val == 'cm') {
                                        col_value = col_value/0.39;
                                    }
                                    data.series.Female.push(col_value);
                                }
                                counter = counter+1;
                                // data.category.push(counter);

                            }

                        }

                    }
                    else if(table_name == 'sizeusa_secondry') { // first table data
                        if(!findItem(secondry,j) && j == key){
                            $("#selected-item").html(key);
                            genderObj = findGender(obj);
                            if(genderObj.isGender) {
                                if(genderObj.value == 'Male') {
                                    if(mesaurement_val == 'cm') {
                                        col_value = col_value/0.39;
                                    }
                                    data.series.Male.push(col_value);
                                }
                                else if(genderObj.value == 'Female') {
                                    if(mesaurement_val == 'cm') {
                                        col_value = col_value/0.39;
                                    }

                                    data.series.Female.push(col_value);
                                }
                                // data.category .push(counter);
                                counter++;
                            }
                        }
                    }
                    else { // first table data
                        if(!findItem(image_twin,j) && j == key){
                            genderObj = findGender(obj);
                            $("#selected-item").html(key);
                            if(genderObj.isGender) {
                                if(genderObj.value == 'Male') {
                                    if(mesaurement_val == 'cm') {
                                        col_value = col_value/0.39;
                                    }

                                    data.series.Male.push(col_value);
                                }
                                else if(genderObj.value == 'Female') {
                                    if(mesaurement_val == 'cm') {
                                        col_value = col_value/0.39;
                                    }

                                    data.series.Female.push(col_value);
                                }
                                // data.category.push(counter);
                                counter++;
                            }
                        }
                    }

            }
        }

    }

    return data;
}

function findGender(obj) {
    var response = {'isGender':false,value:''};

    for (var j in obj) {
       if(j == 'Gender') {
           response.isGender = true;
           response.value = obj[j];
           break;
       }
    }
    return response;
}
function filterData(data) {
    data.series.Male = data.series.Male.filter(function(e){if(!isNaN(e))return e});
    data.series.Female = data.series.Female.filter(function(e){if(!isNaN(e)) return e});

    if(data.series.Male.every(zeroTest) && data.series.Female.every(zeroTest)) {
        alert('Sorry!!! all data contains 0 so cannot draw chart');

    }
    var maleData = data.series.Male;
    var femaleData = data.series.Female;
    var max = 0;
    var min = 0;
    var interval = 0;
    var len = 0;
    var value = 0;
    var min_data = 0;
    var min_value = 0;

    $('#tot-male').html((maleCount*32000).toLocaleString());
    $('#tot-female').html((femaleCount*32000).toLocaleString());
    $('#tot-population').html((total*32000).toLocaleString());
    if(maleData.length>0 ) {
       len = maleData.length;
       if(maleData.length>=10) {
           len = 10;
       }
       max = maleData.max();
       min = maleData.min();
       $('#male-max').html(max.toFixed(2));
        $('#male-min').html(min.toFixed(2));
       interval = parseFloat((max-min)/10);
       console.log('max value '+max+' min value '+min);
       console.log('interval '+interval);

       data.series.Male = [];
       value = min;
       for(var i=0;i<len;i++) {

           // value = value.toFixed(2);
           var item =value.toFixed(2);

           data.series.Male.push(value.toFixed(2));
           value = value+interval;
           item = item+'-'+value.toFixed(2);
           data.series.MaleGroup.push(item);
       }
        var avg = (max+min)/2;
        $('#male-avg').html(avg.toFixed(2));
        /*min_data = parseInt(min/4);
        min_value = min;
        for(var i=0;i<4;i++) {
            min_value = min_value-min_data;
            // value = value+min_data;
            data.series.Male.push(min_value);
        }*/

        // console.log('male data -- '+data.series.Male);
     }

    if(femaleData.length>0) {
        max = femaleData.max();
        min = femaleData.min();
        $('#female-max').html(max.toFixed(2));
        $('#female-min').html(min.toFixed(2));

        interval = parseFloat((max-min)/10);
        len = femaleData.length;
        if(maleData.length>=10) {
            len = 10;
        }
        console.log('max value '+max+' min value '+min);
        console.log('interval '+interval);
        data.series.Female = [];
        value = min;

        for(var i=0;i<len;i++) {

            var item =value.toFixed(2);

            data.series.Female.push(value.toFixed(2));
            value = value+interval;
            item = item+'-'+value.toFixed(2);
            data.series.FemaleGroup.push(item);

        }
        var avg1 = (max+min)/2;
        $('#female-avg').html(avg1.toFixed(2));
    }

        /*min_data = parseInt(min/4);
        min_value = min;
        for(var i=0;i<4;i++) {
            min_value = min_value-min_data;
            // value = value+min_data;
            data.series.Female.push(min_value);
        }*/
        // console.log('female data -- '+data.series.Female);



    if(items.length>0) {
        var total_items = ((items.length)*32000)/10;
        // data.category = [];

        var length = items.length;
        if(items.length>=10) {
            length = 10;
        }
        var tot_value = 0;
        for(var i=0;i<length;i++) {
            tot_value = tot_value+total_items;

            data.MaleCategory.push(Math.ceil(tot_value));
        }
        tot_value = 0;
        for(var i=0;i<length;i++) {
            tot_value = tot_value+total_items;

            data.FemaleCategory.push(Math.ceil(tot_value));
        }
    }
    console.log('before sort male data -- '+data.series.Male);
    console.log('before sort female data -- '+data.series.Female);
    data.MaleCategory.sort(function (a,b) {
       return (b-a);
    });
    data.FemaleCategory.sort(function (a,b) {
        return (b-a);
    });
    console.log('male data -- '+data.series.Male);
    console.log('female data -- '+data.series.Female);
    for(var i=0;i<data.FemaleCategory.length;i++) {

        data.FemaleCategory[i] = -data.FemaleCategory[i];
    }
    $('#sel_val').html($('#selected-item').text());
    $('#sel_vall').html($('#selected-item').text());

    return data;
}


function zeroTest(element) {
    return element === 0;
}


Array.prototype.max = function() {
    return Math.max.apply(null, this);
};

Array.prototype.min = function() {
    return Math.min.apply(null, this);
};