<?php
/**
 * Created by PhpStorm.
 * User: Sourav
 * Date: 8/11/2017
 * Time: 3:57 PM
 */
?>

<script src="//cdn.jsdelivr.net/d3js/3.5.17/d3.min.js" charset="utf-8"></script>
<script src="//cdn.jsdelivr.net/underscorejs/latest/underscore-min.js" type="text/javascript"></script>
<script src="//cdn.jsdelivr.net/taucharts/latest/tauCharts.min.js" type="text/javascript"></script>


<link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/taucharts/latest/tauCharts.min.css">
<style>
    html, body, #scatter {
        width:100%;
        height:100%;
        margin:0;
        padding: 0;
        font-family:'Tahoma'
    }
    #scatter > svg {
        display:block;
    }
</style>
<div id="scatter" style="height:300px"></div>

<script>
    var defData = [
        {"team": "d", "cycleTime": 1, "effort": 1, "count": 1, "priority": "low"}, {
            "team": "d",
            "cycleTime": 2,
            "effort": 2,
            "count": 5,
            "priority": "low"
        }, {"team": "d", "cycleTime": 3, "effort": 3, "count": 8, "priority": "medium"}, {
            "team": "d",
            "cycleTime": 4,
            "effort": 4,
            "count": 3,
            "priority": "high"
        }, {"team": "l", "cycleTime": 2, "effort": 1, "count": 1, "priority": "low"}, {
            "team": "l",
            "cycleTime": 3,
            "effort": 2,
            "count": 5,
            "priority": "low"
        }, {"team": "l", "cycleTime": 4, "effort": 3, "count": 8, "priority": "medium"}, {
            "team": "l",
            "cycleTime": 5,
            "effort": 4,
            "count": 3,
            "priority": "high"
        }
        ];
    var chart = new tauCharts.Chart({
        data: defData,
        type: 'bar',
        x: 'team',
        y: 'effort',
        color:'priority'
    });
    chart.renderTo('#scatter');
</script>