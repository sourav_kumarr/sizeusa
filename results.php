<?php
/**
 * Created by PhpStorm.
 * User: Sourav
 * Date: 7/13/2017
 * Time: 2:00 PM
 */
//session_start();
//print_r($_SESSION);
include ('header.php');
//print_r($_SESSION);
if(!isset($_SESSION['sua_user_id'])) {
    ?>
    <script>
        window.location = "report_gene.php";
    </script>
    <?php
}
if(isset($_SESSION['tableName'])) {
    echo "<input type='hidden' id='user_id' value='".$_SESSION['userId']."'>";
    echo "<input type='hidden' id='table_name' value='".$_SESSION['tableName']."'>";
    echo "<input type='hidden' id='data' value='".$_SESSION['data']."'>";
    echo "<input type='hidden' id='selection' value='".$_SESSION['selection']."'>";


}
else{
    ?>
    <script>
        window.location = "report_gene.php";
    </script>
    <?php
}

if(isset($_SESSION['is_subscribed'])) {
  $is_subscribed = $_SESSION['is_subscribed'];
  if(!$is_subscribed) {
      ?>
      <script>
          window.location = "report_gene.php";
      </script>
      <?php
  }
}
else{
    ?>
    <script>
        window.location = "report_gene.php";
    </script>
    <?php
}
?>
<style>
    .dataTables_filter{
        display: none;
    }
    #table_data_info{
        display: none;
    }
    .results {
        padding: 0;
        display: none;
    }
    .results.active{
        display: block;
    }
    #result-data>tbody>tr.active>td {
        background: #123456;
        color: #fff;
    }
    /*.pagination {
        padding: 0px;
        margin: 0px;
        height: 30px;
        display: block;
        text-align: center;
    }
    .pagination li {
        display: inline-block;
        list-style: none;
        padding: 0px;
        margin-right: 1px;
        width: 30px;
        text-align: center;
        background: black;
        line-height: 25px;
    }
    .pagination .disabled {
        display: inline-block;
        list-style: none;
        padding: 0px;
        margin-right: 1px;
        width: 30px;
        text-align: center;
        line-height: 25px;
        background-color: #666666;
        cursor:inherit;
    }
    .pagination li a{
        color:#FFFFFF;
        text-decoration:none;
    }*/
</style>
<!--<script type="text/javascript" src="js/fusioncharts.js"></script>
<script type="text/javascript" src="js/fusionchart.theme.js"></script>
-->
<script src="https://cdn.zingchart.com/zingchart.min.js"></script>
<script>
    zingchart.MODULESDIR = "https://cdn.zingchart.com/modules/";
    ZC.LICENSE = ["569d52cefae586f634c54f86dc99e6a9", "ee6b7db5b51705a13dc2339db3edaf6d"];
</script>

<div id="overlay">
    <div id="progstat">....Please Wait....<br>Loading</div>
    <div id="progress"></div>
</div>
   <div class="results results-1 active">
    <div class="container-fluid">

        <div class="col-md-12">
            <hr/>
        </div>
        <div class="col-md-12 ">
            <ul id="exTab3" class="list-inline list-unstyled" style="text-align: center">
                <ul class="list-inline list-unstyled pull-left" >

                 <li ><h3 style="margin-top: 0px">Available data</h3></li>
                </ul>
                <ul class="list-inline list-unstyled pull-right">
                    <li><label>Results in : </label></li>
                    <li>
                        <select class="form-control" id="length_type" onchange="filterReport()">
                            <option value="inch">Inches</option>
                            <option value="cm">CM</option>
                        </select>
                    </li>
                    <li><label id="total">Total : 0</label></li>
                    <li>
                        <!--<select class="form-control" id="print_data">
                            <option value="excel">Excel</option>
                        </select>-->
                    </li>
                    <li><button class="btn btn-primary" onclick="onPrintReport()"><i class="fa  fa-file-excel-o"></i> Export</button></li>
                    <li><button class="btn btn-primary" onclick="onLevelLoaded(2);onDataLoaded();"><i class="fa  fa-line-chart"></i> Graph</button></li>

                </ul>




            </ul>


        </div>
        <div class="col-md-12">
            <hr/>
        </div>

        <div class="col-md-12" style="overflow: auto">
            <table id="table_data" class="table table-bordered" >

            </table>
        </div>
        <div class="col-md-12 " >
            <p id="pagination"></p>
        </div>

    </div>
   </div>

    <div class="results results-2">
        <div class="container-fluid">

            <div class="col-md-12">
                <hr/>
            </div>
            <div class="col-md-12 ">
                <ul id="exTab3" class="list-inline list-unstyled" style="text-align: center">
                    <ul class="list-inline list-unstyled pull-left" style="margin-top: -27px">
                        <li><h3 style="cursor:pointer" onclick="onLevelLoaded(1);"><i class="fa fa-arrow-circle-left"></i> </h3> </li>
                        <li ><h3 style="margin-top: 0px">Available data</h3></li>
                    </ul>
                    <ul class="list-inline list-unstyled pull-right">
                        <li><label>Results In : </label></li>
                        <li><select id="measure-val" class="form-control">
                                <option value="inch">Inch</option>
                                <option value="cm">CM</option>
                            </select></li>
                        <li><label id="total1">Total : 0</label></li>
                    </ul>
                </ul>


            </div>
            <div class="col-md-12">
                <hr/>
            </div>

            <div class="col-md-12" >
                <div class="col-md-3" style="margin-top: 2%;padding: 0px;">
                    <div class="col-md-12 results-header">
                        <label>Please click name below to see detail</label>
                    </div>

                    <div class="col-md-12" style="margin: 10px 0 0 0px;padding: 0px;height: 509px;overflow: auto">
                    <table id="result-data" class="table table-bordered table-hover">

                    </table>

                    </div>


                </div>
                <div class="col-md-6" style="margin: 0px;padding: 61px 0 0 0;">
                 <div class="chart-container" id="chart" style="margin-left: 3%;">

                 </div>

                    <div class="col-md-12" style="text-align: center">  <label id="selected-item"></label></div>

                </div>
                <div class="col-md-3" style="margin: 0px;padding: 51px 0 0 0;">

                    <div class="col-md-12" style="margin: 0px;padding: 0px">
                        <div  class="col-md-12">
                            <h4><label >Total Population </label></h4>
                        </div>
                       <div  class="col-md-12">
                            <ul class="list-unstyled">
                                <li style="display: flex; display:-webkit-flex"><p style="width: 70px" >Male  </p>&nbsp;<label id="tot-male">20</label></li>

                                <li style="display: flex; display:-webkit-flex"><p style="width: 70px">Female  </p>&nbsp;<label id="tot-female">20</label></li>
                                <li style="display: flex; display:-webkit-flex"><p style="width: 70px">Total  </p>&nbsp;<label id="tot-population">20</label></li>

                            </ul>
                        </div>
                    </div>
                    <div  class="col-md-12">
                        <hr/>
                    </div>

                    <div class="col-md-12" style="margin: 0px;padding: 0px">
                        <div  class="col-md-12">
                            <h4><label id="sel_val">Height </label>&nbsp;&nbsp;(Male) </h4>
                        </div>

                        <div  class="col-md-12">
                            <ul class="list-unstyled">
                                <li style="display: flex; display:-webkit-flex"><p style="width:70px">Max </p> <label id="male-max">20</label></li>
                                <li style="display: flex; display:-webkit-flex"><p style="width:70px">Min </p> <label id="male-min">20</label></li>
                                <li style="display: flex; display:-webkit-flex"><p style="width:70px">Avg </p> <label id="male-avg">20</label></li>


                            </ul>
                        </div>
                    </div>

                    <div  class="col-md-12">
                        <hr/>
                    </div>
                    <div class="col-md-12" style="margin: 0px;padding: 0px">
                        <div  class="col-md-12">
                            <h4><label id="sel_vall">Height </label>&nbsp;&nbsp;(Female) </h4>
                        </div>

                        <div  class="col-md-12">
                            <ul class="list-unstyled">
                                <li style="display: flex; display:-webkit-flex"><p style="width:70px">Max  </p> <label id="female-max">20</label></li>
                                <li style="display: flex; display:-webkit-flex"><p style="width:70px">Min  </p><label id="female-min">20</label></li>
                                <li style="display: flex; display:-webkit-flex"><p style="width:70px">Avg  </p><label id="female-avg">20</label></li>

                            </ul>
                        </div>
                    </div>
                    <!--<table id="maxmin-det" class="table table-bordered table-hover">
                        <thead>
                        <tr>
                            <th colspan="2" style="text-align: center">No.People</th>
                            <th colspan="5" id="sel_val" style="text-align: center">Height</th>

                        </tr>
                        <tr>
                            <th>Max</th>
                            <th>Min</th>
                            <th colspan="2" style="text-align: center">Male</th>
                            <th colspan="2" style="text-align: center">Female</th>
                            <th>Unit</th>
                        </tr>

                        <tr id="maxmin_row">
                            <td>0</td>
                            <td>1</td>
                            <td>10</td>
                            <td>15</td>
                            <td>10</td>
                            <td>15</td>
                            <td>Inches</td>
                        </tr>
                        </thead>
                    </table>-->

                </div>

        </div>
    </div>
<?php
include('footer.php');
?>

<script type="application/javascript" src="js/results.js"></script>
