/**
 * Created by Sourav on 8/14/2017.
 */

function onLevelLoaded(step) {
    $(".size-matching").removeClass("active");
    $(".size-matching.size-matching-"+step).addClass("active");
}

function onReportSubmit() {
    var heightt = $("#heightn").val();
    var weight = $("#weight").val();
    var age = $("#age").val();
    var waist = $("#waist").val();
    var race = $("#race").val();
    var gender = $("#gender").val();
    var user_id = $("#user_id").val();

    // console.log('height :'+heightt+' weight '+weight+' age '+age+' waist '+waist+' race '+race+' gender '+gender);

    if(user_id == '') {
        signIn();
        return false;
    }
    if(heightt == '' || weight =='' || age=='' || waist=='' || race=='' || gender=='') {
     $("#message").html("Please enter all fields");
      return false;
    }
    else if(!$.isNumeric(weight)) {
        $("#message").html("Please enter valid weight value");
        return false;
    }
    else if(!$.isNumeric(heightt)) {
        $("#message").html("Please enter valid height value");
        return false;
    }
    else if(!$.isNumeric(waist)) {
        $("#message").html("Please enter valid waist value");
        return false;
    }
    $('#overlay').css('display','block');
    $("#message").html("");
    var url='api/reportProcess.php';

    $.post(url,{'gender':gender,'age':age,'ethnicity':race,'income':'0',
            'education':'0','activity':'0','martial':'0','type':'addReport',
            'height':'0','weight':'0','user_id':user_id}, function(data) {

            var status = data.Status;
            var message = data.Message;
            var code = data.Code;
            $('#overlay').css('display','none');

            console.log('data -- '+JSON.stringify(data));
            if(status == 'Success') {
              onLevelLoaded(2);
              get_report_data();
            }
            else if(status == 'Failure') {
                $('#message').html(message);
            }
        });




    // onLevelLoaded(2);

}

function get_report_data() {
    var userId = $('#user_id').val();
    var table_name = 'sizeusa_primary';
    var data = 'Gender,EthnicGroup,HeightIn,Weight,AgeRange,Waist';
    var selection = data;

    var th_ = '<thead>';
    var td_ = '<tbody>';
    var url = 'api/reportProcess.php';
    $('#overlay').css('display', 'block');
    $('#result-data').html('');

    $.post(url, {
        'type': 'filterReportData',
        'userId': userId,
        'tableName': table_name,
        'data': data,
        'selection': selection
    }, function (data) {
        $('#overlay').css('display', 'none');
        // console.log('data -- '+JSON.stringify(data));

        var status = data.Status;
        var items = data.data;
        console.log(status);
        if (status == 'Success') {
            for (var i = 0; i < items.length; i++) {
                var obj = items[i];
                if (obj) {

                    td_ = td_ + '<tr>';
                    for (var j in obj) {
                        if (i == 0) {
                            th_ = th_ + '<th>' + j + '</th>';
                        }

                        // console.log('j val -- '+j);
                        var col_value = obj[j];
                        td_ = td_ + '<td>' + col_value + '</td>';
                    }
                }
            }

            td_ = td_ + '</tbody>';

            $('#results-dataa').html(th_ + td_);
            $('#results-dataa').DataTable({destroy:true});
        }
    })
}
